/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriadocumental.findAll", query = "SELECT c FROM Categoriadocumental c"),
    @NamedQuery(name = "Categoriadocumental.findByIdcategoriadocumental", query = "SELECT c FROM Categoriadocumental c WHERE c.idcategoriadocumental = :idcategoriadocumental"),
    @NamedQuery(name = "Categoriadocumental.findByCategoria", query = "SELECT c FROM Categoriadocumental c WHERE c.categoria = :categoria")})
public class Categoriadocumental implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idcategoriadocumental;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String categoria;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcategoriadocumental", fetch = FetchType.EAGER)
    private List<Categoriatipodocumento> categoriatipodocumentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcategoriadocumental", fetch = FetchType.EAGER)
    private List<Documento> documentoList;
    @JoinColumn(name = "idsecciondocumental", referencedColumnName = "idsecciondocumental", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Secciondocumental idsecciondocumental;

    public Categoriadocumental() {
    }

    public Categoriadocumental(Integer idcategoriadocumental) {
        this.idcategoriadocumental = idcategoriadocumental;
    }

    public Categoriadocumental(Integer idcategoriadocumental, String categoria) {
        this.idcategoriadocumental = idcategoriadocumental;
        this.categoria = categoria;
    }

    public Integer getIdcategoriadocumental() {
        return idcategoriadocumental;
    }

    public void setIdcategoriadocumental(Integer idcategoriadocumental) {
        this.idcategoriadocumental = idcategoriadocumental;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @XmlTransient
    public List<Categoriatipodocumento> getCategoriatipodocumentoList() {
        return categoriatipodocumentoList;
    }

    public void setCategoriatipodocumentoList(List<Categoriatipodocumento> categoriatipodocumentoList) {
        this.categoriatipodocumentoList = categoriatipodocumentoList;
    }

    @XmlTransient
    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    public Secciondocumental getIdsecciondocumental() {
        return idsecciondocumental;
    }

    public void setIdsecciondocumental(Secciondocumental idsecciondocumental) {
        this.idsecciondocumental = idsecciondocumental;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoriadocumental != null ? idcategoriadocumental.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriadocumental)) {
            return false;
        }
        Categoriadocumental other = (Categoriadocumental) object;
        if ((this.idcategoriadocumental == null && other.idcategoriadocumental != null) || (this.idcategoriadocumental != null && !this.idcategoriadocumental.equals(other.idcategoriadocumental))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Categoriadocumental[ idcategoriadocumental=" + idcategoriadocumental + " ]";
    }
    
}
