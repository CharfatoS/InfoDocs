/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encuesta.findAll", query = "SELECT e FROM Encuesta e"),
    @NamedQuery(name = "Encuesta.findByIdencuesta", query = "SELECT e FROM Encuesta e WHERE e.idencuesta = :idencuesta"),
    @NamedQuery(name = "Encuesta.findByEstado", query = "SELECT e FROM Encuesta e WHERE e.estado = :estado"),
    @NamedQuery(name = "Encuesta.findByNombre", query = "SELECT e FROM Encuesta e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Encuesta.findByDescripcion", query = "SELECT e FROM Encuesta e WHERE e.descripcion = :descripcion")})
public class Encuesta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idencuesta;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @Size(max = 255)
    @Column(length = 255)
    private String descripcion;
    @JoinColumn(name = "iddependencia", referencedColumnName = "iddependencia", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Dependencia iddependencia;
    @JoinColumn(name = "idtemaarea", referencedColumnName = "idtemaarea", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Temaarea idtemaarea;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idencuesta", fetch = FetchType.EAGER)
    private List<Pregunta> preguntaList;

    public Encuesta() {
    }

    public Encuesta(Integer idencuesta) {
        this.idencuesta = idencuesta;
    }

    public Encuesta(Integer idencuesta, short estado, String nombre) {
        this.idencuesta = idencuesta;
        this.estado = estado;
        this.nombre = nombre;
    }

    public Integer getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(Integer idencuesta) {
        this.idencuesta = idencuesta;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Dependencia getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(Dependencia iddependencia) {
        this.iddependencia = iddependencia;
    }

    public Temaarea getIdtemaarea() {
        return idtemaarea;
    }

    public void setIdtemaarea(Temaarea idtemaarea) {
        this.idtemaarea = idtemaarea;
    }

    @XmlTransient
    public List<Pregunta> getPreguntaList() {
        return preguntaList;
    }

    public void setPreguntaList(List<Pregunta> preguntaList) {
        this.preguntaList = preguntaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idencuesta != null ? idencuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encuesta)) {
            return false;
        }
        Encuesta other = (Encuesta) object;
        if ((this.idencuesta == null && other.idencuesta != null) || (this.idencuesta != null && !this.idencuesta.equals(other.idencuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Encuesta[ idencuesta=" + idencuesta + " ]";
    }
    
}
