/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Disposicionfinal.findAll", query = "SELECT d FROM Disposicionfinal d"),
    @NamedQuery(name = "Disposicionfinal.findByIddisposicionfinal", query = "SELECT d FROM Disposicionfinal d WHERE d.iddisposicionfinal = :iddisposicionfinal"),
    @NamedQuery(name = "Disposicionfinal.findByDisposicion", query = "SELECT d FROM Disposicionfinal d WHERE d.disposicion = :disposicion")})
public class Disposicionfinal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer iddisposicionfinal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String disposicion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddisposicionfinal", fetch = FetchType.EAGER)
    private List<Documento> documentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddisposicionfinal", fetch = FetchType.EAGER)
    private List<Archivo> archivoList;

    public Disposicionfinal() {
    }

    public Disposicionfinal(Integer iddisposicionfinal) {
        this.iddisposicionfinal = iddisposicionfinal;
    }

    public Disposicionfinal(Integer iddisposicionfinal, String disposicion) {
        this.iddisposicionfinal = iddisposicionfinal;
        this.disposicion = disposicion;
    }

    public Integer getIddisposicionfinal() {
        return iddisposicionfinal;
    }

    public void setIddisposicionfinal(Integer iddisposicionfinal) {
        this.iddisposicionfinal = iddisposicionfinal;
    }

    public String getDisposicion() {
        return disposicion;
    }

    public void setDisposicion(String disposicion) {
        this.disposicion = disposicion;
    }

    @XmlTransient
    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    @XmlTransient
    public List<Archivo> getArchivoList() {
        return archivoList;
    }

    public void setArchivoList(List<Archivo> archivoList) {
        this.archivoList = archivoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddisposicionfinal != null ? iddisposicionfinal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Disposicionfinal)) {
            return false;
        }
        Disposicionfinal other = (Disposicionfinal) object;
        if ((this.iddisposicionfinal == null && other.iddisposicionfinal != null) || (this.iddisposicionfinal != null && !this.iddisposicionfinal.equals(other.iddisposicionfinal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Disposicionfinal[ iddisposicionfinal=" + iddisposicionfinal + " ]";
    }
    
}
