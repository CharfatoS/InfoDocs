/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asignado.findAll", query = "SELECT a FROM Asignado a"),
    @NamedQuery(name = "Asignado.findByIdasignado", query = "SELECT a FROM Asignado a WHERE a.idasignado = :idasignado"),
    @NamedQuery(name = "Asignado.findByEstado", query = "SELECT a FROM Asignado a WHERE a.estado = :estado"),
    @NamedQuery(name = "Asignado.findByFechaasignado", query = "SELECT a FROM Asignado a WHERE a.fechaasignado = :fechaasignado"),
    @NamedQuery(name = "Asignado.findByHabilitado", query = "SELECT a FROM Asignado a WHERE a.habilitado = :habilitado")})
public class Asignado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idasignado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String estado;
    @Temporal(TemporalType.DATE)
    private Date fechaasignado;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short habilitado;
    @JoinColumn(name = "idcargoempleadooficina", referencedColumnName = "idcargoempleadooficina", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Cargoempleadooficina idcargoempleadooficina;
    @JoinColumn(name = "idrespuesta", referencedColumnName = "idrespuesta", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Respuesta idrespuesta;
    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Solicitud idsolicitud;

    public Asignado() {
    }

    public Asignado(Integer idasignado) {
        this.idasignado = idasignado;
    }

    public Asignado(Integer idasignado, String estado, short habilitado) {
        this.idasignado = idasignado;
        this.estado = estado;
        this.habilitado = habilitado;
    }

    public Integer getIdasignado() {
        return idasignado;
    }

    public void setIdasignado(Integer idasignado) {
        this.idasignado = idasignado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaasignado() {
        return fechaasignado;
    }

    public void setFechaasignado(Date fechaasignado) {
        this.fechaasignado = fechaasignado;
    }

    public short getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(short habilitado) {
        this.habilitado = habilitado;
    }

    public Cargoempleadooficina getIdcargoempleadooficina() {
        return idcargoempleadooficina;
    }

    public void setIdcargoempleadooficina(Cargoempleadooficina idcargoempleadooficina) {
        this.idcargoempleadooficina = idcargoempleadooficina;
    }

    public Respuesta getIdrespuesta() {
        return idrespuesta;
    }

    public void setIdrespuesta(Respuesta idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idasignado != null ? idasignado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignado)) {
            return false;
        }
        Asignado other = (Asignado) object;
        if ((this.idasignado == null && other.idasignado != null) || (this.idasignado != null && !this.idasignado.equals(other.idasignado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Asignado[ idasignado=" + idasignado + " ]";
    }
    
}
