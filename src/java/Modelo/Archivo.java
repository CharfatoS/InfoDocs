/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Archivo.findAll", query = "SELECT a FROM Archivo a"),
    @NamedQuery(name = "Archivo.findByIdarchivo", query = "SELECT a FROM Archivo a WHERE a.idarchivo = :idarchivo"),
    @NamedQuery(name = "Archivo.findByAdjunto", query = "SELECT a FROM Archivo a WHERE a.adjunto = :adjunto"),
    @NamedQuery(name = "Archivo.findByTipoarchivo", query = "SELECT a FROM Archivo a WHERE a.tipoarchivo = :tipoarchivo")})
public class Archivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idarchivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String adjunto;
    @Lob
    private byte[] archivo;
    @Size(max = 45)
    @Column(length = 45)
    private String tipoarchivo;
    @JoinColumn(name = "iddisposicionfinal", referencedColumnName = "iddisposicionfinal", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Disposicionfinal iddisposicionfinal;
    @JoinColumn(name = "idrespuesta", referencedColumnName = "idrespuesta", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Respuesta idrespuesta;
    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Solicitud idsolicitud;

    public Archivo() {
    }

    public Archivo(Integer idarchivo) {
        this.idarchivo = idarchivo;
    }

    public Archivo(Integer idarchivo, String adjunto) {
        this.idarchivo = idarchivo;
        this.adjunto = adjunto;
    }

    public Integer getIdarchivo() {
        return idarchivo;
    }

    public void setIdarchivo(Integer idarchivo) {
        this.idarchivo = idarchivo;
    }

    public String getAdjunto() {
        return adjunto;
    }

    public void setAdjunto(String adjunto) {
        this.adjunto = adjunto;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getTipoarchivo() {
        return tipoarchivo;
    }

    public void setTipoarchivo(String tipoarchivo) {
        this.tipoarchivo = tipoarchivo;
    }

    public Disposicionfinal getIddisposicionfinal() {
        return iddisposicionfinal;
    }

    public void setIddisposicionfinal(Disposicionfinal iddisposicionfinal) {
        this.iddisposicionfinal = iddisposicionfinal;
    }

    public Respuesta getIdrespuesta() {
        return idrespuesta;
    }

    public void setIdrespuesta(Respuesta idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idarchivo != null ? idarchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Archivo)) {
            return false;
        }
        Archivo other = (Archivo) object;
        if ((this.idarchivo == null && other.idarchivo != null) || (this.idarchivo != null && !this.idarchivo.equals(other.idarchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Archivo[ idarchivo=" + idarchivo + " ]";
    }
    
}
