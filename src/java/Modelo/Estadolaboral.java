/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadolaboral.findAll", query = "SELECT e FROM Estadolaboral e"),
    @NamedQuery(name = "Estadolaboral.findByIdestadolaboral", query = "SELECT e FROM Estadolaboral e WHERE e.idestadolaboral = :idestadolaboral"),
    @NamedQuery(name = "Estadolaboral.findByEstado", query = "SELECT e FROM Estadolaboral e WHERE e.estado = :estado"),
    @NamedQuery(name = "Estadolaboral.findByDescripcion", query = "SELECT e FROM Estadolaboral e WHERE e.descripcion = :descripcion")})
public class Estadolaboral implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idestadolaboral;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String estado;
    @Size(max = 255)
    @Column(length = 255)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idestadolaboral", fetch = FetchType.EAGER)
    private List<Empleado> empleadoList;

    public Estadolaboral() {
    }

    public Estadolaboral(Integer idestadolaboral) {
        this.idestadolaboral = idestadolaboral;
    }

    public Estadolaboral(Integer idestadolaboral, String estado) {
        this.idestadolaboral = idestadolaboral;
        this.estado = estado;
    }

    public Integer getIdestadolaboral() {
        return idestadolaboral;
    }

    public void setIdestadolaboral(Integer idestadolaboral) {
        this.idestadolaboral = idestadolaboral;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestadolaboral != null ? idestadolaboral.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadolaboral)) {
            return false;
        }
        Estadolaboral other = (Estadolaboral) object;
        if ((this.idestadolaboral == null && other.idestadolaboral != null) || (this.idestadolaboral != null && !this.idestadolaboral.equals(other.idestadolaboral))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Estadolaboral[ idestadolaboral=" + idestadolaboral + " ]";
    }
    
}
