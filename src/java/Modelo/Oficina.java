/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Oficina.findAll", query = "SELECT o FROM Oficina o"),
    @NamedQuery(name = "Oficina.findByIdoficina", query = "SELECT o FROM Oficina o WHERE o.idoficina = :idoficina"),
    @NamedQuery(name = "Oficina.findByMail", query = "SELECT o FROM Oficina o WHERE o.mail = :mail"),
    @NamedQuery(name = "Oficina.findByNombre", query = "SELECT o FROM Oficina o WHERE o.nombre = :nombre"),
    @NamedQuery(name = "Oficina.findByTel", query = "SELECT o FROM Oficina o WHERE o.tel = :tel"),
    @NamedQuery(name = "Oficina.findByExtension", query = "SELECT o FROM Oficina o WHERE o.extension = :extension")})
public class Oficina implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idoficina;
    @Size(max = 255)
    @Column(length = 255)
    private String mail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @Size(max = 255)
    @Column(length = 255)
    private String tel;
    private Integer extension;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idoficina", fetch = FetchType.EAGER)
    private List<Cargoempleadooficina> cargoempleadooficinaList;

    public Oficina() {
    }

    public Oficina(Integer idoficina) {
        this.idoficina = idoficina;
    }

    public Oficina(Integer idoficina, String nombre) {
        this.idoficina = idoficina;
        this.nombre = nombre;
    }

    public Integer getIdoficina() {
        return idoficina;
    }

    public void setIdoficina(Integer idoficina) {
        this.idoficina = idoficina;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getExtension() {
        return extension;
    }

    public void setExtension(Integer extension) {
        this.extension = extension;
    }

    @XmlTransient
    public List<Cargoempleadooficina> getCargoempleadooficinaList() {
        return cargoempleadooficinaList;
    }

    public void setCargoempleadooficinaList(List<Cargoempleadooficina> cargoempleadooficinaList) {
        this.cargoempleadooficinaList = cargoempleadooficinaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idoficina != null ? idoficina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Oficina)) {
            return false;
        }
        Oficina other = (Oficina) object;
        if ((this.idoficina == null && other.idoficina != null) || (this.idoficina != null && !this.idoficina.equals(other.idoficina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Oficina[ idoficina=" + idoficina + " ]";
    }
    
}
