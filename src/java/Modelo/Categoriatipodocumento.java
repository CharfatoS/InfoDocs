/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriatipodocumento.findAll", query = "SELECT c FROM Categoriatipodocumento c"),
    @NamedQuery(name = "Categoriatipodocumento.findByIdcategoriatipodocumento", query = "SELECT c FROM Categoriatipodocumento c WHERE c.idcategoriatipodocumento = :idcategoriatipodocumento")})
public class Categoriatipodocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idcategoriatipodocumento;
    @JoinColumn(name = "idcategoriadocumental", referencedColumnName = "idcategoriadocumental", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Categoriadocumental idcategoriadocumental;
    @JoinColumn(name = "idtipodocumento", referencedColumnName = "idtipodocumento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tipodocumento idtipodocumento;

    public Categoriatipodocumento() {
    }

    public Categoriatipodocumento(Integer idcategoriatipodocumento) {
        this.idcategoriatipodocumento = idcategoriatipodocumento;
    }

    public Integer getIdcategoriatipodocumento() {
        return idcategoriatipodocumento;
    }

    public void setIdcategoriatipodocumento(Integer idcategoriatipodocumento) {
        this.idcategoriatipodocumento = idcategoriatipodocumento;
    }

    public Categoriadocumental getIdcategoriadocumental() {
        return idcategoriadocumental;
    }

    public void setIdcategoriadocumental(Categoriadocumental idcategoriadocumental) {
        this.idcategoriadocumental = idcategoriadocumental;
    }

    public Tipodocumento getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(Tipodocumento idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcategoriatipodocumento != null ? idcategoriatipodocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriatipodocumento)) {
            return false;
        }
        Categoriatipodocumento other = (Categoriatipodocumento) object;
        if ((this.idcategoriatipodocumento == null && other.idcategoriatipodocumento != null) || (this.idcategoriatipodocumento != null && !this.idcategoriatipodocumento.equals(other.idcategoriatipodocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Categoriatipodocumento[ idcategoriatipodocumento=" + idcategoriatipodocumento + " ]";
    }
    
}
