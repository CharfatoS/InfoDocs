/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lugar.findAll", query = "SELECT l FROM Lugar l"),
    @NamedQuery(name = "Lugar.findByIdlugar", query = "SELECT l FROM Lugar l WHERE l.idlugar = :idlugar"),
    @NamedQuery(name = "Lugar.findByNombre", query = "SELECT l FROM Lugar l WHERE l.nombre = :nombre"),
    @NamedQuery(name = "Lugar.findByCodigo", query = "SELECT l FROM Lugar l WHERE l.codigo = :codigo")})
public class Lugar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idlugar;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String codigo;
    @JoinColumn(name = "idciudad", referencedColumnName = "idciudad", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ciudad idciudad;
    @JoinColumn(name = "idtipolugar", referencedColumnName = "idtipolugar", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tipolugar idtipolugar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idlugar", fetch = FetchType.EAGER)
    private List<Direccion> direccionList;

    public Lugar() {
    }

    public Lugar(Integer idlugar) {
        this.idlugar = idlugar;
    }

    public Lugar(Integer idlugar, String nombre, String codigo) {
        this.idlugar = idlugar;
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public Integer getIdlugar() {
        return idlugar;
    }

    public void setIdlugar(Integer idlugar) {
        this.idlugar = idlugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Ciudad getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Ciudad idciudad) {
        this.idciudad = idciudad;
    }

    public Tipolugar getIdtipolugar() {
        return idtipolugar;
    }

    public void setIdtipolugar(Tipolugar idtipolugar) {
        this.idtipolugar = idtipolugar;
    }

    @XmlTransient
    public List<Direccion> getDireccionList() {
        return direccionList;
    }

    public void setDireccionList(List<Direccion> direccionList) {
        this.direccionList = direccionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlugar != null ? idlugar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lugar)) {
            return false;
        }
        Lugar other = (Lugar) object;
        if ((this.idlugar == null && other.idlugar != null) || (this.idlugar != null && !this.idlugar.equals(other.idlugar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Lugar[ idlugar=" + idlugar + " ]";
    }
    
}
