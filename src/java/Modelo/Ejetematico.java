/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ejetematico.findAll", query = "SELECT e FROM Ejetematico e"),
    @NamedQuery(name = "Ejetematico.findByIdejetematico", query = "SELECT e FROM Ejetematico e WHERE e.idejetematico = :idejetematico"),
    @NamedQuery(name = "Ejetematico.findByDescripcion", query = "SELECT e FROM Ejetematico e WHERE e.descripcion = :descripcion"),
    @NamedQuery(name = "Ejetematico.findByTema", query = "SELECT e FROM Ejetematico e WHERE e.tema = :tema"),
    @NamedQuery(name = "Ejetematico.findByTiemporespuesta", query = "SELECT e FROM Ejetematico e WHERE e.tiemporespuesta = :tiemporespuesta")})
public class Ejetematico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idejetematico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(nullable = false, length = 2147483647)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String tema;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int tiemporespuesta;
    @JoinColumn(name = "idservicio", referencedColumnName = "idservicio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Servicio idservicio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idejetematico", fetch = FetchType.EAGER)
    private List<Solicitud> solicitudList;

    public Ejetematico() {
    }

    public Ejetematico(Integer idejetematico) {
        this.idejetematico = idejetematico;
    }

    public Ejetematico(Integer idejetematico, String descripcion, String tema, int tiemporespuesta) {
        this.idejetematico = idejetematico;
        this.descripcion = descripcion;
        this.tema = tema;
        this.tiemporespuesta = tiemporespuesta;
    }

    public Integer getIdejetematico() {
        return idejetematico;
    }

    public void setIdejetematico(Integer idejetematico) {
        this.idejetematico = idejetematico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public int getTiemporespuesta() {
        return tiemporespuesta;
    }

    public void setTiemporespuesta(int tiemporespuesta) {
        this.tiemporespuesta = tiemporespuesta;
    }

    public Servicio getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Servicio idservicio) {
        this.idservicio = idservicio;
    }

    @XmlTransient
    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idejetematico != null ? idejetematico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ejetematico)) {
            return false;
        }
        Ejetematico other = (Ejetematico) object;
        if ((this.idejetematico == null && other.idejetematico != null) || (this.idejetematico != null && !this.idejetematico.equals(other.idejetematico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Ejetematico[ idejetematico=" + idejetematico + " ]";
    }
    
}
