/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tiporequerimiento.findAll", query = "SELECT t FROM Tiporequerimiento t"),
    @NamedQuery(name = "Tiporequerimiento.findByIdtiporequerimiento", query = "SELECT t FROM Tiporequerimiento t WHERE t.idtiporequerimiento = :idtiporequerimiento"),
    @NamedQuery(name = "Tiporequerimiento.findByTipo", query = "SELECT t FROM Tiporequerimiento t WHERE t.tipo = :tipo")})
public class Tiporequerimiento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtiporequerimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtiporequerimiento", fetch = FetchType.EAGER)
    private List<Solicitud> solicitudList;

    public Tiporequerimiento() {
    }

    public Tiporequerimiento(Integer idtiporequerimiento) {
        this.idtiporequerimiento = idtiporequerimiento;
    }

    public Tiporequerimiento(Integer idtiporequerimiento, String tipo) {
        this.idtiporequerimiento = idtiporequerimiento;
        this.tipo = tipo;
    }

    public Integer getIdtiporequerimiento() {
        return idtiporequerimiento;
    }

    public void setIdtiporequerimiento(Integer idtiporequerimiento) {
        this.idtiporequerimiento = idtiporequerimiento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtiporequerimiento != null ? idtiporequerimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tiporequerimiento)) {
            return false;
        }
        Tiporequerimiento other = (Tiporequerimiento) object;
        if ((this.idtiporequerimiento == null && other.idtiporequerimiento != null) || (this.idtiporequerimiento != null && !this.idtiporequerimiento.equals(other.idtiporequerimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tiporequerimiento[ idtiporequerimiento=" + idtiporequerimiento + " ]";
    }
    
}
