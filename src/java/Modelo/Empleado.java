/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByIdempleado", query = "SELECT e FROM Empleado e WHERE e.idempleado = :idempleado"),
    @NamedQuery(name = "Empleado.findByCesantias", query = "SELECT e FROM Empleado e WHERE e.cesantias = :cesantias"),
    @NamedQuery(name = "Empleado.findByEps", query = "SELECT e FROM Empleado e WHERE e.eps = :eps"),
    @NamedQuery(name = "Empleado.findByFecharegistro", query = "SELECT e FROM Empleado e WHERE e.fecharegistro = :fecharegistro"),
    @NamedQuery(name = "Empleado.findByPension", query = "SELECT e FROM Empleado e WHERE e.pension = :pension")})
public class Empleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idempleado;
    @Size(max = 255)
    @Column(length = 255)
    private String cesantias;
    @Size(max = 255)
    @Column(length = 255)
    private String eps;
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;
    @Size(max = 255)
    @Column(length = 255)
    private String pension;
    @JoinColumn(name = "idestadolaboral", referencedColumnName = "idestadolaboral", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Estadolaboral idestadolaboral;
    @JoinColumn(name = "docidusuario", referencedColumnName = "docid", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario docidusuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempleado", fetch = FetchType.EAGER)
    private List<Cargoempleadooficina> cargoempleadooficinaList;

    public Empleado() {
    }

    public Empleado(Integer idempleado) {
        this.idempleado = idempleado;
    }

    public Integer getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(Integer idempleado) {
        this.idempleado = idempleado;
    }

    public String getCesantias() {
        return cesantias;
    }

    public void setCesantias(String cesantias) {
        this.cesantias = cesantias;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public String getPension() {
        return pension;
    }

    public void setPension(String pension) {
        this.pension = pension;
    }

    public Estadolaboral getIdestadolaboral() {
        return idestadolaboral;
    }

    public void setIdestadolaboral(Estadolaboral idestadolaboral) {
        this.idestadolaboral = idestadolaboral;
    }

    public Usuario getDocidusuario() {
        return docidusuario;
    }

    public void setDocidusuario(Usuario docidusuario) {
        this.docidusuario = docidusuario;
    }

    @XmlTransient
    public List<Cargoempleadooficina> getCargoempleadooficinaList() {
        return cargoempleadooficinaList;
    }

    public void setCargoempleadooficinaList(List<Cargoempleadooficina> cargoempleadooficinaList) {
        this.cargoempleadooficinaList = cargoempleadooficinaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idempleado != null ? idempleado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.idempleado == null && other.idempleado != null) || (this.idempleado != null && !this.idempleado.equals(other.idempleado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Empleado[ idempleado=" + idempleado + " ]";
    }
    
}
