/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estadorequerimiento.findAll", query = "SELECT e FROM Estadorequerimiento e"),
    @NamedQuery(name = "Estadorequerimiento.findByIdestadorequerimiento", query = "SELECT e FROM Estadorequerimiento e WHERE e.idestadorequerimiento = :idestadorequerimiento"),
    @NamedQuery(name = "Estadorequerimiento.findByEstado", query = "SELECT e FROM Estadorequerimiento e WHERE e.estado = :estado")})
public class Estadorequerimiento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idestadorequerimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idestadorequerimiento", fetch = FetchType.EAGER)
    private List<Solicitud> solicitudList;

    public Estadorequerimiento() {
    }

    public Estadorequerimiento(Integer idestadorequerimiento) {
        this.idestadorequerimiento = idestadorequerimiento;
    }

    public Estadorequerimiento(Integer idestadorequerimiento, String estado) {
        this.idestadorequerimiento = idestadorequerimiento;
        this.estado = estado;
    }

    public Integer getIdestadorequerimiento() {
        return idestadorequerimiento;
    }

    public void setIdestadorequerimiento(Integer idestadorequerimiento) {
        this.idestadorequerimiento = idestadorequerimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idestadorequerimiento != null ? idestadorequerimiento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estadorequerimiento)) {
            return false;
        }
        Estadorequerimiento other = (Estadorequerimiento) object;
        if ((this.idestadorequerimiento == null && other.idestadorequerimiento != null) || (this.idestadorequerimiento != null && !this.idestadorequerimiento.equals(other.idestadorequerimiento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Estadorequerimiento[ idestadorequerimiento=" + idestadorequerimiento + " ]";
    }
    
}
