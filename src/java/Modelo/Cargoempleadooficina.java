/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cargoempleadooficina.findAll", query = "SELECT c FROM Cargoempleadooficina c"),
    @NamedQuery(name = "Cargoempleadooficina.findByIdcargoempleadooficina", query = "SELECT c FROM Cargoempleadooficina c WHERE c.idcargoempleadooficina = :idcargoempleadooficina"),
    @NamedQuery(name = "Cargoempleadooficina.findByFecharegistro", query = "SELECT c FROM Cargoempleadooficina c WHERE c.fecharegistro = :fecharegistro"),
    @NamedQuery(name = "Cargoempleadooficina.findByVinculado", query = "SELECT c FROM Cargoempleadooficina c WHERE c.vinculado = :vinculado")})
public class Cargoempleadooficina implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idcargoempleadooficina;
    @Temporal(TemporalType.DATE)
    private Date fecharegistro;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private boolean vinculado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcargoempleadooficina", fetch = FetchType.EAGER)
    private List<Asignado> asignadoList;
    @JoinColumn(name = "idcargo", referencedColumnName = "idcargo", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Cargo idcargo;
    @JoinColumn(name = "idempleado", referencedColumnName = "idempleado", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Empleado idempleado;
    @JoinColumn(name = "idoficina", referencedColumnName = "idoficina", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Oficina idoficina;

    public Cargoempleadooficina() {
    }

    public Cargoempleadooficina(Integer idcargoempleadooficina) {
        this.idcargoempleadooficina = idcargoempleadooficina;
    }

    public Cargoempleadooficina(Integer idcargoempleadooficina, boolean vinculado) {
        this.idcargoempleadooficina = idcargoempleadooficina;
        this.vinculado = vinculado;
    }

    public Integer getIdcargoempleadooficina() {
        return idcargoempleadooficina;
    }

    public void setIdcargoempleadooficina(Integer idcargoempleadooficina) {
        this.idcargoempleadooficina = idcargoempleadooficina;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public boolean getVinculado() {
        return vinculado;
    }

    public void setVinculado(boolean vinculado) {
        this.vinculado = vinculado;
    }

    @XmlTransient
    public List<Asignado> getAsignadoList() {
        return asignadoList;
    }

    public void setAsignadoList(List<Asignado> asignadoList) {
        this.asignadoList = asignadoList;
    }

    public Cargo getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(Cargo idcargo) {
        this.idcargo = idcargo;
    }

    public Empleado getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(Empleado idempleado) {
        this.idempleado = idempleado;
    }

    public Oficina getIdoficina() {
        return idoficina;
    }

    public void setIdoficina(Oficina idoficina) {
        this.idoficina = idoficina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcargoempleadooficina != null ? idcargoempleadooficina.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cargoempleadooficina)) {
            return false;
        }
        Cargoempleadooficina other = (Cargoempleadooficina) object;
        if ((this.idcargoempleadooficina == null && other.idcargoempleadooficina != null) || (this.idcargoempleadooficina != null && !this.idcargoempleadooficina.equals(other.idcargoempleadooficina))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Cargoempleadooficina[ idcargoempleadooficina=" + idcargoempleadooficina + " ]";
    }
    
}
