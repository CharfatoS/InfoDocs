/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEntrega.findAll", query = "SELECT t FROM TipoEntrega t"),
    @NamedQuery(name = "TipoEntrega.findByIdtipoentrega", query = "SELECT t FROM TipoEntrega t WHERE t.idtipoentrega = :idtipoentrega"),
    @NamedQuery(name = "TipoEntrega.findByNombre", query = "SELECT t FROM TipoEntrega t WHERE t.nombre = :nombre")})
public class TipoEntrega implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtipoentrega;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipoentrega", fetch = FetchType.EAGER)
    private List<Respuesta> respuestaList;

    public TipoEntrega() {
    }

    public TipoEntrega(Integer idtipoentrega) {
        this.idtipoentrega = idtipoentrega;
    }

    public TipoEntrega(Integer idtipoentrega, String nombre) {
        this.idtipoentrega = idtipoentrega;
        this.nombre = nombre;
    }

    public Integer getIdtipoentrega() {
        return idtipoentrega;
    }

    public void setIdtipoentrega(Integer idtipoentrega) {
        this.idtipoentrega = idtipoentrega;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Respuesta> getRespuestaList() {
        return respuestaList;
    }

    public void setRespuestaList(List<Respuesta> respuestaList) {
        this.respuestaList = respuestaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoentrega != null ? idtipoentrega.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEntrega)) {
            return false;
        }
        TipoEntrega other = (TipoEntrega) object;
        if ((this.idtipoentrega == null && other.idtipoentrega != null) || (this.idtipoentrega != null && !this.idtipoentrega.equals(other.idtipoentrega))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.TipoEntrega[ idtipoentrega=" + idtipoentrega + " ]";
    }
    
}
