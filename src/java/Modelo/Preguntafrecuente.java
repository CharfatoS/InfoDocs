/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preguntafrecuente.findAll", query = "SELECT p FROM Preguntafrecuente p"),
    @NamedQuery(name = "Preguntafrecuente.findByIdpregunafrecuente", query = "SELECT p FROM Preguntafrecuente p WHERE p.idpregunafrecuente = :idpregunafrecuente"),
    @NamedQuery(name = "Preguntafrecuente.findByEstado", query = "SELECT p FROM Preguntafrecuente p WHERE p.estado = :estado"),
    @NamedQuery(name = "Preguntafrecuente.findByPregunta", query = "SELECT p FROM Preguntafrecuente p WHERE p.pregunta = :pregunta"),
    @NamedQuery(name = "Preguntafrecuente.findByDescripcion", query = "SELECT p FROM Preguntafrecuente p WHERE p.descripcion = :descripcion")})
public class Preguntafrecuente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idpregunafrecuente;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short estado;
    @Size(max = 255)
    @Column(length = 255)
    private String pregunta;
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String descripcion;
    @JoinColumn(name = "idrespuestafaq", referencedColumnName = "idrespuestafaq", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private RespuestaFAQ idrespuestafaq;
    @JoinColumn(name = "idservicio", referencedColumnName = "idservicio", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Servicio idservicio;
    @JoinColumn(name = "idtemaarea", referencedColumnName = "idtemaarea", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Temaarea idtemaarea;

    public Preguntafrecuente() {
    }

    public Preguntafrecuente(Integer idpregunafrecuente) {
        this.idpregunafrecuente = idpregunafrecuente;
    }

    public Preguntafrecuente(Integer idpregunafrecuente, short estado) {
        this.idpregunafrecuente = idpregunafrecuente;
        this.estado = estado;
    }

    public Integer getIdpregunafrecuente() {
        return idpregunafrecuente;
    }

    public void setIdpregunafrecuente(Integer idpregunafrecuente) {
        this.idpregunafrecuente = idpregunafrecuente;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public RespuestaFAQ getIdrespuestafaq() {
        return idrespuestafaq;
    }

    public void setIdrespuestafaq(RespuestaFAQ idrespuestafaq) {
        this.idrespuestafaq = idrespuestafaq;
    }

    public Servicio getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Servicio idservicio) {
        this.idservicio = idservicio;
    }

    public Temaarea getIdtemaarea() {
        return idtemaarea;
    }

    public void setIdtemaarea(Temaarea idtemaarea) {
        this.idtemaarea = idtemaarea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpregunafrecuente != null ? idpregunafrecuente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preguntafrecuente)) {
            return false;
        }
        Preguntafrecuente other = (Preguntafrecuente) object;
        if ((this.idpregunafrecuente == null && other.idpregunafrecuente != null) || (this.idpregunafrecuente != null && !this.idpregunafrecuente.equals(other.idpregunafrecuente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Preguntafrecuente[ idpregunafrecuente=" + idpregunafrecuente + " ]";
    }
    
}
