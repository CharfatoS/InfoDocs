/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empjuridico.findAll", query = "SELECT e FROM Empjuridico e"),
    @NamedQuery(name = "Empjuridico.findByNit", query = "SELECT e FROM Empjuridico e WHERE e.nit = :nit"),
    @NamedQuery(name = "Empjuridico.findByMail", query = "SELECT e FROM Empjuridico e WHERE e.mail = :mail"),
    @NamedQuery(name = "Empjuridico.findByTel", query = "SELECT e FROM Empjuridico e WHERE e.tel = :tel"),
    @NamedQuery(name = "Empjuridico.findByWeb", query = "SELECT e FROM Empjuridico e WHERE e.web = :web")})
public class Empjuridico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nit;
    @Size(max = 255)
    @Column(length = 255)
    private String mail;
    private Integer tel;
    @Size(max = 255)
    @Column(length = 255)
    private String web;
    @JoinColumn(name = "iddireccion", referencedColumnName = "iddireccion", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Direccion iddireccion;
    @JoinColumn(name = "idrazonsocial", referencedColumnName = "idrazonsocial", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Razonsocial idrazonsocial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idempjuridico", fetch = FetchType.EAGER)
    private List<Usuario> usuarioList;

    public Empjuridico() {
    }

    public Empjuridico(String nit) {
        this.nit = nit;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getTel() {
        return tel;
    }

    public void setTel(Integer tel) {
        this.tel = tel;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public Direccion getIddireccion() {
        return iddireccion;
    }

    public void setIddireccion(Direccion iddireccion) {
        this.iddireccion = iddireccion;
    }

    public Razonsocial getIdrazonsocial() {
        return idrazonsocial;
    }

    public void setIdrazonsocial(Razonsocial idrazonsocial) {
        this.idrazonsocial = idrazonsocial;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nit != null ? nit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empjuridico)) {
            return false;
        }
        Empjuridico other = (Empjuridico) object;
        if ((this.nit == null && other.nit != null) || (this.nit != null && !this.nit.equals(other.nit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Empjuridico[ nit=" + nit + " ]";
    }
    
}
