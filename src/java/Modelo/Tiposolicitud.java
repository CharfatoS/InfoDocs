/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tiposolicitud.findAll", query = "SELECT t FROM Tiposolicitud t"),
    @NamedQuery(name = "Tiposolicitud.findByIdtiposolicitud", query = "SELECT t FROM Tiposolicitud t WHERE t.idtiposolicitud = :idtiposolicitud"),
    @NamedQuery(name = "Tiposolicitud.findByTipo", query = "SELECT t FROM Tiposolicitud t WHERE t.tipo = :tipo")})
public class Tiposolicitud implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtiposolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtiposolicutd", fetch = FetchType.EAGER)
    private List<Solicitud> solicitudList;

    public Tiposolicitud() {
    }

    public Tiposolicitud(Integer idtiposolicitud) {
        this.idtiposolicitud = idtiposolicitud;
    }

    public Tiposolicitud(Integer idtiposolicitud, String tipo) {
        this.idtiposolicitud = idtiposolicitud;
        this.tipo = tipo;
    }

    public Integer getIdtiposolicitud() {
        return idtiposolicitud;
    }

    public void setIdtiposolicitud(Integer idtiposolicitud) {
        this.idtiposolicitud = idtiposolicitud;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtiposolicitud != null ? idtiposolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tiposolicitud)) {
            return false;
        }
        Tiposolicitud other = (Tiposolicitud) object;
        if ((this.idtiposolicitud == null && other.idtiposolicitud != null) || (this.idtiposolicitud != null && !this.idtiposolicitud.equals(other.idtiposolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tiposolicitud[ idtiposolicitud=" + idtiposolicitud + " ]";
    }
    
}
