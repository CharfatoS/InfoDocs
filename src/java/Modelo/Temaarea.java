/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Temaarea.findAll", query = "SELECT t FROM Temaarea t"),
    @NamedQuery(name = "Temaarea.findByIdtemaarea", query = "SELECT t FROM Temaarea t WHERE t.idtemaarea = :idtemaarea"),
    @NamedQuery(name = "Temaarea.findByEstado", query = "SELECT t FROM Temaarea t WHERE t.estado = :estado"),
    @NamedQuery(name = "Temaarea.findByNombre", query = "SELECT t FROM Temaarea t WHERE t.nombre = :nombre")})
public class Temaarea implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtemaarea;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtemaarea", fetch = FetchType.EAGER)
    private List<Preguntafrecuente> preguntafrecuenteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtemaarea", fetch = FetchType.EAGER)
    private List<Encuesta> encuestaList;

    public Temaarea() {
    }

    public Temaarea(Integer idtemaarea) {
        this.idtemaarea = idtemaarea;
    }

    public Temaarea(Integer idtemaarea, short estado, String nombre) {
        this.idtemaarea = idtemaarea;
        this.estado = estado;
        this.nombre = nombre;
    }

    public Integer getIdtemaarea() {
        return idtemaarea;
    }

    public void setIdtemaarea(Integer idtemaarea) {
        this.idtemaarea = idtemaarea;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Preguntafrecuente> getPreguntafrecuenteList() {
        return preguntafrecuenteList;
    }

    public void setPreguntafrecuenteList(List<Preguntafrecuente> preguntafrecuenteList) {
        this.preguntafrecuenteList = preguntafrecuenteList;
    }

    @XmlTransient
    public List<Encuesta> getEncuestaList() {
        return encuestaList;
    }

    public void setEncuestaList(List<Encuesta> encuestaList) {
        this.encuestaList = encuestaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtemaarea != null ? idtemaarea.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Temaarea)) {
            return false;
        }
        Temaarea other = (Temaarea) object;
        if ((this.idtemaarea == null && other.idtemaarea != null) || (this.idtemaarea != null && !this.idtemaarea.equals(other.idtemaarea))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Temaarea[ idtemaarea=" + idtemaarea + " ]";
    }
    
}
