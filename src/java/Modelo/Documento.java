/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d"),
    @NamedQuery(name = "Documento.findByIddocumento", query = "SELECT d FROM Documento d WHERE d.iddocumento = :iddocumento"),
    @NamedQuery(name = "Documento.findByAsunto", query = "SELECT d FROM Documento d WHERE d.asunto = :asunto"),
    @NamedQuery(name = "Documento.findByCodigo", query = "SELECT d FROM Documento d WHERE d.codigo = :codigo"),
    @NamedQuery(name = "Documento.findByFechaarchivo", query = "SELECT d FROM Documento d WHERE d.fechaarchivo = :fechaarchivo")})
public class Documento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer iddocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String asunto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String codigo;
    @Temporal(TemporalType.DATE)
    private Date fechaarchivo;
    @JoinColumn(name = "idcategoriadocumental", referencedColumnName = "idcategoriadocumental", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Categoriadocumental idcategoriadocumental;
    @JoinColumn(name = "iddisposicionfinal", referencedColumnName = "iddisposicionfinal", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Disposicionfinal iddisposicionfinal;
    @JoinColumn(name = "idretencion", referencedColumnName = "idretencion", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Retencion idretencion;
    @JoinColumn(name = "idtipodocumento", referencedColumnName = "idtipodocumento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tipodocumento idtipodocumento;

    public Documento() {
    }

    public Documento(Integer iddocumento) {
        this.iddocumento = iddocumento;
    }

    public Documento(Integer iddocumento, String asunto, String codigo) {
        this.iddocumento = iddocumento;
        this.asunto = asunto;
        this.codigo = codigo;
    }

    public Integer getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(Integer iddocumento) {
        this.iddocumento = iddocumento;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaarchivo() {
        return fechaarchivo;
    }

    public void setFechaarchivo(Date fechaarchivo) {
        this.fechaarchivo = fechaarchivo;
    }

    public Categoriadocumental getIdcategoriadocumental() {
        return idcategoriadocumental;
    }

    public void setIdcategoriadocumental(Categoriadocumental idcategoriadocumental) {
        this.idcategoriadocumental = idcategoriadocumental;
    }

    public Disposicionfinal getIddisposicionfinal() {
        return iddisposicionfinal;
    }

    public void setIddisposicionfinal(Disposicionfinal iddisposicionfinal) {
        this.iddisposicionfinal = iddisposicionfinal;
    }

    public Retencion getIdretencion() {
        return idretencion;
    }

    public void setIdretencion(Retencion idretencion) {
        this.idretencion = idretencion;
    }

    public Tipodocumento getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(Tipodocumento idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddocumento != null ? iddocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) object;
        if ((this.iddocumento == null && other.iddocumento != null) || (this.iddocumento != null && !this.iddocumento.equals(other.iddocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Documento[ iddocumento=" + iddocumento + " ]";
    }
    
}
