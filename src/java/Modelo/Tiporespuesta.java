/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tiporespuesta.findAll", query = "SELECT t FROM Tiporespuesta t"),
    @NamedQuery(name = "Tiporespuesta.findByIdtiporespuesta", query = "SELECT t FROM Tiporespuesta t WHERE t.idtiporespuesta = :idtiporespuesta"),
    @NamedQuery(name = "Tiporespuesta.findByTipo", query = "SELECT t FROM Tiporespuesta t WHERE t.tipo = :tipo")})
public class Tiporespuesta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtiporespuesta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String tipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtiporespuesta", fetch = FetchType.EAGER)
    private List<Pregunta> preguntaList;

    public Tiporespuesta() {
    }

    public Tiporespuesta(Integer idtiporespuesta) {
        this.idtiporespuesta = idtiporespuesta;
    }

    public Tiporespuesta(Integer idtiporespuesta, String tipo) {
        this.idtiporespuesta = idtiporespuesta;
        this.tipo = tipo;
    }

    public Integer getIdtiporespuesta() {
        return idtiporespuesta;
    }

    public void setIdtiporespuesta(Integer idtiporespuesta) {
        this.idtiporespuesta = idtiporespuesta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlTransient
    public List<Pregunta> getPreguntaList() {
        return preguntaList;
    }

    public void setPreguntaList(List<Pregunta> preguntaList) {
        this.preguntaList = preguntaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtiporespuesta != null ? idtiporespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tiporespuesta)) {
            return false;
        }
        Tiporespuesta other = (Tiporespuesta) object;
        if ((this.idtiporespuesta == null && other.idtiporespuesta != null) || (this.idtiporespuesta != null && !this.idtiporespuesta.equals(other.idtiporespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tiporespuesta[ idtiporespuesta=" + idtiporespuesta + " ]";
    }
    
}
