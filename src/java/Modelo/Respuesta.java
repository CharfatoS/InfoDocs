/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Respuesta.findAll", query = "SELECT r FROM Respuesta r"),
    @NamedQuery(name = "Respuesta.findByIdrespuesta", query = "SELECT r FROM Respuesta r WHERE r.idrespuesta = :idrespuesta"),
    @NamedQuery(name = "Respuesta.findByArchivado", query = "SELECT r FROM Respuesta r WHERE r.archivado = :archivado"),
    @NamedQuery(name = "Respuesta.findByEntregado", query = "SELECT r FROM Respuesta r WHERE r.entregado = :entregado"),
    @NamedQuery(name = "Respuesta.findByRespuesta", query = "SELECT r FROM Respuesta r WHERE r.respuesta = :respuesta"),
    @NamedQuery(name = "Respuesta.findByContenido", query = "SELECT r FROM Respuesta r WHERE r.contenido = :contenido"),
    @NamedQuery(name = "Respuesta.findBySugerencia", query = "SELECT r FROM Respuesta r WHERE r.sugerencia = :sugerencia"),
    @NamedQuery(name = "Respuesta.findByTiemporespuesta", query = "SELECT r FROM Respuesta r WHERE r.tiemporespuesta = :tiemporespuesta"),
    @NamedQuery(name = "Respuesta.findByObservacion", query = "SELECT r FROM Respuesta r WHERE r.observacion = :observacion")})
public class Respuesta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idrespuesta;
    @Temporal(TemporalType.DATE)
    private Date archivado;
    @Temporal(TemporalType.DATE)
    private Date entregado;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date respuesta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(nullable = false, length = 2147483647)
    private String contenido;
    @Size(max = 2147483647)
    @Column(length = 2147483647)
    private String sugerencia;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int tiemporespuesta;
    @Size(max = 255)
    @Column(length = 255)
    private String observacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrespuesta", fetch = FetchType.EAGER)
    private List<Asignado> asignadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrespuesta", fetch = FetchType.EAGER)
    private List<Archivo> archivoList;
    @JoinColumn(name = "idtipoentrega", referencedColumnName = "idtipoentrega", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private TipoEntrega idtipoentrega;

    public Respuesta() {
    }

    public Respuesta(Integer idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public Respuesta(Integer idrespuesta, Date respuesta, String contenido, int tiemporespuesta) {
        this.idrespuesta = idrespuesta;
        this.respuesta = respuesta;
        this.contenido = contenido;
        this.tiemporespuesta = tiemporespuesta;
    }

    public Integer getIdrespuesta() {
        return idrespuesta;
    }

    public void setIdrespuesta(Integer idrespuesta) {
        this.idrespuesta = idrespuesta;
    }

    public Date getArchivado() {
        return archivado;
    }

    public void setArchivado(Date archivado) {
        this.archivado = archivado;
    }

    public Date getEntregado() {
        return entregado;
    }

    public void setEntregado(Date entregado) {
        this.entregado = entregado;
    }

    public Date getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Date respuesta) {
        this.respuesta = respuesta;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getSugerencia() {
        return sugerencia;
    }

    public void setSugerencia(String sugerencia) {
        this.sugerencia = sugerencia;
    }

    public int getTiemporespuesta() {
        return tiemporespuesta;
    }

    public void setTiemporespuesta(int tiemporespuesta) {
        this.tiemporespuesta = tiemporespuesta;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @XmlTransient
    public List<Asignado> getAsignadoList() {
        return asignadoList;
    }

    public void setAsignadoList(List<Asignado> asignadoList) {
        this.asignadoList = asignadoList;
    }

    @XmlTransient
    public List<Archivo> getArchivoList() {
        return archivoList;
    }

    public void setArchivoList(List<Archivo> archivoList) {
        this.archivoList = archivoList;
    }

    public TipoEntrega getIdtipoentrega() {
        return idtipoentrega;
    }

    public void setIdtipoentrega(TipoEntrega idtipoentrega) {
        this.idtipoentrega = idtipoentrega;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrespuesta != null ? idrespuesta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Respuesta)) {
            return false;
        }
        Respuesta other = (Respuesta) object;
        if ((this.idrespuesta == null && other.idrespuesta != null) || (this.idrespuesta != null && !this.idrespuesta.equals(other.idrespuesta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Respuesta[ idrespuesta=" + idrespuesta + " ]";
    }
    
}
