package com.vur.usuario.facade;

import Modelo.Users;
import Modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class UsuarioFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Usuario entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Usuario entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Usuario entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Usuario find(Object id) {
        return getEntityManager().find(Usuario.class, id);
    }

    public List<Usuario> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Usuario.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Usuario> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Usuario.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Usuario> rt = cq.from(Usuario.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public String rol(Usuario usuario) {
        CriteriaQuery<Users> cq = em.getCriteriaBuilder().createQuery(Users.class);
        Root<Users> u = cq.from(Users.class);
        Join j = u.join("usuario", JoinType.LEFT);
        cq.select(j.get("rol"));
        Query q = getEntityManager().createQuery(cq);
        return ((String) q.getSingleResult());
    }

    public boolean BuscarPersonaNueva(Usuario currentPersona) {
        return false;
    }

    public Usuario findUsuarioConId(String di) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Usuario> consulta = cb.createQuery(Usuario.class);
            Root<Usuario> userW = consulta.from(Usuario.class);
            consulta.select(userW).where(cb.equal(userW.get("docId"), di));
            Query q = getEntityManager().createQuery(consulta);
            return (Usuario) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    public boolean verifiqueUsuarioConId(String di) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Usuario> consulta = cb.createQuery(Usuario.class);
            Root<Usuario> userW = consulta.from(Usuario.class);
            consulta.select(userW).where(cb.equal(userW.get("docId"), di));
            Query q = getEntityManager().createQuery(consulta);
            Usuario u = (Usuario) q.getSingleResult();
            System.out.println(" en facade "+ (u != null));
            return (u != null);
        } catch (Exception e) {
            return false;
        }
    }
}
