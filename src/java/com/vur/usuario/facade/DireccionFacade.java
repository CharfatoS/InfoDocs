
package com.vur.usuario.facade;

import Modelo.Direccion;
import Modelo.Users;
import Modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class DireccionFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Direccion entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Direccion entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Direccion entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Direccion find(Object id) {
        return getEntityManager().find(Direccion.class, id);
    }

    public List<Direccion> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Direccion.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Direccion> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Direccion.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Direccion> rt = cq.from(Direccion.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Direccion verDireccionDeUsuario(Usuario u) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Direccion> consulta = cb.createQuery(Direccion.class);
            Root<Direccion> dir = consulta.from(Direccion.class);
            consulta.select(dir).where(cb.equal(dir.get("idUsuario"), u));
            Query q = getEntityManager().createQuery(consulta);
            Direccion us = (Direccion) q.getSingleResult();
            return us;
        } catch (Exception e) {
            return null;
        }
    }
}

