package com.vur.usuario.facade;

import Modelo.Rol;
import Modelo.Users;
import Modelo.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class UsersFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    private CriteriaBuilder cb() {
        return getEntityManager().getCriteriaBuilder();
    }

    public void create(Users entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Users entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Users entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Users find(Object id) {
        return getEntityManager().find(Users.class, id);
    }

    public List<Users> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Users.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Users> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Users.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Users> rt = cq.from(Users.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Users loguear(String id) {
        CriteriaQuery cq = cb().createQuery(Users.class);
        Root<Users> u = cq.from(Users.class);
        cq.select(u);
        ParameterExpression<String> p = cb().parameter(String.class);
        cq.select(u).where(cb().equal(u.get("username"), id));
        Users usuario = (Users) getEntityManager().createQuery(cq).getSingleResult();
        return usuario;
    }

    public Users verUsersPorUsername(String username) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> consulta = cb.createQuery(Users.class);
            Root<Users> userW = consulta.from(Users.class);
            consulta.select(userW).where(cb.equal(userW.get("username"), username));
            Query q = getEntityManager().createQuery(consulta);
            return (Users) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public Rol buscarRolPorUsername(String username) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> consulta = cb.createQuery(Users.class);
            Root<Users> userW = consulta.from(Users.class);
            consulta.select(userW).where(cb.equal(userW.get("username"), username));
            Query q = getEntityManager().createQuery(consulta);
            Users us = (Users) q.getSingleResult();
            return us.getRol();
        } catch (Exception e) {
            return null;
        }
    }

    public Users verUsersPorUsuario(Usuario u) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> consulta = cb.createQuery(Users.class);
            Root<Users> userW = consulta.from(Users.class);
            consulta.select(userW).where(cb.equal(userW.get("idUsuario"), u));
            Query q = getEntityManager().createQuery(consulta);
            Users us = (Users) q.getSingleResult();
            return us;
        } catch (Exception e) {
            return null;
        }
    }
}
