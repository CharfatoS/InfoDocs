
package com.vur.usuario.facade;

import Modelo.Cargoempleadooficina;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesus
 */
@Stateless
public class UsuarioOficinaFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    private CriteriaBuilder cb() {
        return getEntityManager().getCriteriaBuilder();
    }

    public void create(Cargoempleadooficina entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Cargoempleadooficina entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Cargoempleadooficina entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Cargoempleadooficina find(Object id) {
        return getEntityManager().find(Cargoempleadooficina.class, id);
    }

    public List<Cargoempleadooficina> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Cargoempleadooficina.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Cargoempleadooficina> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Cargoempleadooficina.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Cargoempleadooficina> rt = cq.from(Cargoempleadooficina.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    
}
