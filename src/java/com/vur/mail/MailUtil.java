package com.vur.mail;

import java.io.Serializable;
import java.util.Properties;
import javax.ejb.Asynchronous;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * contacto@gitinnova.com Midavid48
 *
 * @author jesus
 */
public class MailUtil implements Serializable {

//    private String smtpHost;
//    private String port;
//    private Properties props;
    private String usuario;
    private String pass;
//    private Message mensaje;// = new MimeMessage(sesion);
//    @Resource(name = "mail/gmailAccount")
    private Session sesion;

    public MailUtil() {
    }

    public void establecerSesion(Properties prop, final String mail, final String pass) {
        Session s = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                mail, pass);// Specify the Username and the PassWord
                    }
                });
        sesion = s;
    }

    public PasswordAuthentication getPasswordAuthentication(String user, String pass) {
        String username = user;
        String password = pass;
        if ((username != null) && (username.length() > 0) && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        } else {
            return null;
        }
    }

    public PasswordAuthentication getPasswordAuthentication() {
        String username = usuario;
        String password = pass;
        if ((username != null) && (username.length() > 0) && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        } else {
            return null;
        }
    }

    public void establecerValidacion(String mail, String password) {
        usuario = mail;
        pass = password;
    }

    private Properties getPropertiesGmail() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.stmp.user", "viacreativa@gmail.com");

        //To use TLS
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.password", "j35usMarin");
        //To use SSL
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        return props;
    }

    @Asynchronous
    public String envioMailGmail(String correoTo, String msgs) {
        Properties prop = getPropertiesGmail();
        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, pass);// Specify the Username and the PassWord
            }
        });
        String to = "" + correoTo;
        String from = "viacreativa@gmail.com";
        String subject = "Ingrese al vur verifique su solicitud";
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(from));
            msg.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            msg.setSubject(subject);
            msg.setText("Consulte su correo, solicitud en proceso..!!"+msgs);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", 465, "username", "password");
            transport.send(msg);
//            System.out.println("fine Gmail!!");
            return "fine!!";
        } catch (Exception exc) {
            System.out.println(exc);
            return "No!! " + exc;

        }
    }

    private Properties getPropertiesGit() {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gitinnova.com");
//        props.put("mail.stmp.user", "viacreativa@gmail.com");
        //To use TLS
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtp.password", "j35usMarin");
        //To use SSL
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        return props;
    }

    @Asynchronous
    public String envioMailGit(String correoTo, String s) {
        Properties prop = getPropertiesGit();
        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, pass);// Specify the Username and the PassWord
            }
        });
        String to = "" + correoTo;
        String from = "contacto@gitinnova.com";
        String subject = "Emnsaje de Ventanilla Única de Registro";
        Message msg = new MimeMessage(session);
        String  text ="Tienes una solicitud ";
        try {
            msg.setFrom(new InternetAddress(from));
            msg.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(to));
            msg.setSubject(subject);
            msg.setText("Consulte su correo, solicitud en proceso..! "+s);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gitinnova.com", 465, "username", "password");
            transport.send(msg);
//            System.out.println("fine server  GitInnova!!");
            return "fine!! ine server  GitInnova!!";
        } catch (Exception exc) {
            System.out.println(exc);
            return "No!! GitInnova" + exc;

        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
