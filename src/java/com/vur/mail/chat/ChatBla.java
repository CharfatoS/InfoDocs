package com.vur.mail.chat;

import com.vur.usuario.bean.LoginBean;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jesus
 */
@ManagedBean(name = "chatBean", eager = true)
@SessionScoped
public class ChatBla implements Serializable {   
   
    private  Sala sala;  
    
    public  void ingresarAlChat(Sala sala){
        UsChat uc = new UsChat();
       
        sala.getChates().add(uc);
        
    }
    
    public void  salirDelChat(Sala sala, UsChat uc){
        sala.getChates().remove(uc);
    }

    public void  mandarMensaje(Sala sala, String m){
        
       // sala.getMensajes().add(m)
    }
    public ChatBla() {
    }

    
    public Sala getSala() {
        return sala;
    }

    public void setSala(Sala sala) {
        this.sala = sala;
    }

}
