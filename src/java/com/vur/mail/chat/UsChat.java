package com.vur.mail.chat;

import java.io.Serializable;

/**
 *
 * @author jesus
 */
public class UsChat implements Serializable {
    
    private String apodo;
   private Long id;
   private String nombre;
    
    public UsChat() {
    }

    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    
    

    
    
}
