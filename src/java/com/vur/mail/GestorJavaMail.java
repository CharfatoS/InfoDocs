package com.vur.mail;

import com.vur.controller.util.JsfUtil;
import java.io.Serializable;

/**
 *
 * @author jesus
 */
//@ManagedBean(name = "gestorJavaMail")
//@SessionScoped
public class GestorJavaMail implements Serializable {

    private MailUtil mail;

    public GestorJavaMail() {
    }

    public void sendGmail(String correoenvio, String pass, String correoU, String msgs) {
        mail = new MailUtil();
        mail.establecerValidacion(correoenvio, pass);
        String s = mail.envioMailGmail(correoU, msgs);
        JsfUtil.addSuccessMessage("Resultado:  " + s);
    }

    public void sendGit(String correoenvio, String pass, String correoU, String msgs) {
        mail = new MailUtil();
        mail.establecerValidacion(correoenvio, pass);
        String s = mail.envioMailGit(correoU, msgs);
        JsfUtil.addSuccessMessage("Resultado:  " + s);
    }

    public MailUtil getMail() {
        return mail;
    }

    public void setMail(MailUtil mail) {
        this.mail = mail;
    }

}
