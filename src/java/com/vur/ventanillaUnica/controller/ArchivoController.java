package com.vur.ventanillaUnica.controller;

import Modelo.Archivo;
import com.vur.controller.util.JsfUtil;
import com.vur.controller.util.PaginationHelper;
import com.vur.ventanillaUnica.facade.ArchivoFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "archivoController")
@SessionScoped
public class ArchivoController implements Serializable {

    private Archivo current;
    private DataModel items = null;
    @EJB
    private com.vur.ventanillaUnica.facade.ArchivoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public ArchivoController() {
    }

    public Archivo getSelected() {
        if (current == null) {
            current = new Archivo();
            selectedItemIndex = -1;
        }
        return current;
    }

    private ArchivoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Archivo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Archivo();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("ArchivoCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Archivo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("ArchivoUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Archivo) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("ArchivoDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public String linkDescargarArchivo(Archivo item) {
        String link = " ";
        if (item.getTipoarchivo().equals("application/pdf")) {
            //para pdf 
            link = "../../servletArchivo.pdf";
        } else if (item.getTipoarchivo().equals("application/zip")) {
            //para zip
            link = "../../servletArchivo.zip";
        } else if (item.getTipoarchivo().equals("application/vnd.ms-excel")) {
            //para  xls
            link = "../../servletArchivo.xls";
        } else if (item.getTipoarchivo().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
            //para  xlsx
            link = "../../servletArchivo.xlsx";
        } else if (item.getTipoarchivo().equals("application/x-rar")) {
            //para  rar
            link = "../../servletArchivo.rar";
        } else if (item.getTipoarchivo().equals("image/jpeg")) {
            //para  jpg
            link = "../../servletArchivo.jpg";
        } else if (item.getTipoarchivo().equals("image/png")) {
            //para  png
            link = "../../servletArchivo.png";
        } else if (item.getTipoarchivo().equals("application/msword")) {
            //para  doc
            link = "../../servletArchivo.doc";
        } else if (item.getTipoarchivo().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            //para  docx
            link = "../../servletArchivo.docx";
        } else {
            link = "../../servletArchivo.pdf";
        }
        link = link + "?id=" + item.getIdarchivo();
        return link;
    }

    @FacesConverter(forClass = Archivo.class)
    public static class ArchivoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ArchivoController controller = (ArchivoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "archivoController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Archivo) {
                Archivo o = (Archivo) object;
                return getStringKey(o.getIdarchivo());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Archivo.class.getName());
            }
        }

    }

}
