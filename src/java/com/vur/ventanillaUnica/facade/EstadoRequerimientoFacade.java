package com.vur.ventanillaUnica.facade;

import Modelo.Estadorequerimiento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class EstadoRequerimientoFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Estadorequerimiento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Estadorequerimiento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Estadorequerimiento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Estadorequerimiento find(Object id) {
        return getEntityManager().find(Estadorequerimiento.class, id);
    }

    public List<Estadorequerimiento> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Estadorequerimiento> rt = cq.from(Estadorequerimiento.class);
        cq.select(cq.from(Estadorequerimiento.class));
        cq.orderBy(cb.asc(rt.get("idEstadoRequerimiento")));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Estadorequerimiento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Estadorequerimiento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Estadorequerimiento> rt = cq.from(Estadorequerimiento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Estadorequerimiento findEstado(String estado) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Estadorequerimiento> consulta = cb.createQuery(Estadorequerimiento.class);
            Root<Estadorequerimiento> userW = consulta.from(Estadorequerimiento.class);
            consulta.select(userW)
                    .where(cb.equal(userW.get("estado"), estado)
                    );
            Query q = getEntityManager().createQuery(consulta);
            Estadorequerimiento us = (Estadorequerimiento) q.getSingleResult();
            return us;
        } catch (Exception e) {
            return null;
        }
    }
}
