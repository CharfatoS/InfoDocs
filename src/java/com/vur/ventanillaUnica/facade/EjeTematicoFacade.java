
package com.vur.ventanillaUnica.facade;

import Modelo.Dependencia;
import Modelo.Servicio;
import Modelo.Ejetematico;
import Modelo.Solicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class EjeTematicoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Ejetematico entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Ejetematico entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Ejetematico entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Ejetematico find(Object id) {
        return getEntityManager().find(Ejetematico.class, id);
    }

    public List<Ejetematico> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ejetematico.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Ejetematico> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ejetematico.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Ejetematico> rt = cq.from(Ejetematico.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public List<Ejetematico> verEjeTematicoDeDependencia(Dependencia d) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Ejetematico> consulta = cb.createQuery(Ejetematico.class);
            Root<Ejetematico> eje = consulta.from(Ejetematico.class);
            consulta.select(eje).where(
                    cb.equal(eje.get("idServicio").get("idDependencia"), d)          );
            consulta.orderBy(cb.asc(eje.get("idEjeTematico")));
            Query q = getEntityManager().createQuery(consulta);             
            return q.getResultList();
        } catch (Exception e) {             
            return null;
        }
    }

    public List<Ejetematico> verEjeTematicoDeServicio(Servicio s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Ejetematico> consulta = cb.createQuery(Ejetematico.class);
            Root<Ejetematico> eje = consulta.from(Ejetematico.class);
            consulta.select(eje).where(
                    cb.equal(eje.get("idServicio"), s)  
            );
            consulta.orderBy(cb.asc(eje.get("idEjeTematico")));
            Query q = getEntityManager().createQuery(consulta);             
            return q.getResultList();
        } catch (Exception e) {             
            return null;
        }
    }
}

