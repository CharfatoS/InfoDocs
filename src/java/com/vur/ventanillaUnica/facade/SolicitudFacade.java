package com.vur.ventanillaUnica.facade;

import Modelo.Dependencia;
import Modelo.Empleado;
import Modelo.Usuario;
import Modelo.Asignado;
import Modelo.Estadorequerimiento;
import Modelo.Respuesta;
import Modelo.Solicitud;
import Modelo.Tiporequerimiento;
import Modelo.Tiposolicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class SolicitudFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Solicitud entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Solicitud entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Solicitud entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Solicitud find(Object id) {
        return getEntityManager().find(Solicitud.class, id);
    }

    public List<Solicitud> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Solicitud.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Solicitud> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Solicitud.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Solicitud> rt = cq.from(Solicitud.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Solicitud> getSolicitudesUsuario(Usuario u) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idUsuario"), u)
            );
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeEmpleado(Empleado u) {
        try {
            CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Solicitud> query = criteriaBuilder.createQuery(Solicitud.class);
            Root<Asignado> from = query.from(Asignado.class);

            Join<Asignado, Solicitud> joinItem = from.join("idSolicitud", JoinType.LEFT);
            query.multiselect(from, joinItem.get("idEmpleado"));
            query.orderBy(criteriaBuilder.asc(from.get("vence")));
            Query q = getEntityManager().createQuery(query);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d)
            );
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d, Tiposolicitud ts) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d)
            );
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d, Estadorequerimiento es) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(solicitud.get("idEstadoRequerimiento"), es)
            );
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d, Tiporequerimiento es) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(solicitud.get("idTipoRequerimiento"), es));
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d, Respuesta r) {

        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(solicitud.get("idTRespuesta"), r));
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> getSolicitudesDeDependencia(Dependencia d, String docId) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(solicitud.get("idUsuario").get("docId"), docId));
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> buscarSolicitud(int id, String docid) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idUsuario").get("docId"), docid),
                    cb.equal(solicitud.get("idSolicitud"), id));
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> buscarSolicitud(String docid) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> consulta = cb.createQuery(Solicitud.class);
            Root<Solicitud> solicitud = consulta.from(Solicitud.class);
            consulta.select(solicitud).where(
                    cb.equal(solicitud.get("idUsuario").get("docId"), docid));
            consulta.orderBy(cb.asc(solicitud.get("vence")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public int countAsignadasFuncionario(Usuario u, String s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            c.select(cb.count(rt)).where(
                    cb.equal(rt.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(rt.get("idEstadoRequerimiento").get("estado"), s)
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    public List<Solicitud> getlistaSolicitudSegunEstado(Estadorequerimiento s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            c.select(sol).where(
                    cb.equal(sol.get("idEstadoRequerimiento"), s)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public List<Solicitud> getListaSolicitudSegunEstado(Estadorequerimiento s, long fechainicio, long fechafin) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> fr = sol.get("vence");
            c.select(sol).where(
                    cb.equal(sol.get("idEstadoRequerimiento"), s),
                    cb.between(fr, fechainicio, fechafin)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public List<Solicitud> getListaSolicitudSegunEstado(Estadorequerimiento s, long fechainicio, long fechafin, String fechaReferencia) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> fr = sol.get(fechaReferencia);
            c.select(sol).where(
                    cb.equal(sol.get("idEstadoRequerimiento"), s),
                    cb.between(fr, fechainicio, fechafin)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public List<Solicitud> getListaSolicitudSegunEstado(String s, long fechainicio, long fechafin) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> vence = sol.get("vence");
            c.select(sol).where(
                    cb.equal(sol.get("idEstadoRequerimiento").get("estado"), s),
                    cb.between(vence, fechainicio, fechafin)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public List<Solicitud> getlistaSolicitudSegunEstado(Usuario u, Estadorequerimiento s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            c.select(sol).where(
                    cb.equal(sol.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(sol.get("idEstadoRequerimiento"), s)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public List<Solicitud> getlistaAsignadasFuncionario(Usuario u, String s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Solicitud> c = cb.createQuery(Solicitud.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            c.select(sol).where(
                    cb.equal(sol.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(sol.get("idEstadoRequerimiento").get("estado"), s)
            );
            c.orderBy(cb.asc(sol.get("vence")));
            Query q = getEntityManager().createQuery(c);
            return q.getResultList();
        } catch (Exception e) {
            return null;//
        }
    }

    public int countAsignadasFuncionarioVencidas(Usuario u, String asignado) {
        long hoy = System.currentTimeMillis() / 1000;
        //86400 * 2 =172800
        long antier = hoy - 172800;
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> vence = sol.get("vence");
            c.select(cb.count(sol))
                    .where(
                            cb.equal(sol.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                            cb.equal(sol.get("idEstadoRequerimiento").get("estado"), asignado),
                            cb.lt(vence, hoy)
                    );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    public int countAsignadasFuncionarioPorVencerse(Usuario u, String asignado) {
        long hoy = System.currentTimeMillis() / 1000;
        //86400 * 2 =172800
        long hoy2 = hoy + (172800);
        long antier = hoy - 172800;
//        System.out.println("hoy "+hoy+"/antier "+antier);
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> vence = sol.get("vence");
            c.select(cb.count(sol))
                    .where(
                            cb.equal(sol.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                            cb.equal(sol.get("idEstadoRequerimiento").get("estado"), asignado),
                            cb.lt(vence, hoy2),//menor a pasado mañana (sol.get(), hoy),
                            cb.gt(vence, hoy)//si es menor a hoy, esta vencida, debe ser mayor a hoy
                    );
            Query q = getEntityManager().createQuery(c);
            List<Solicitud> l = q.getResultList();
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    /*cuenta las solicitudes de un usuario a partir d una fecha de inicio  segun el estado
     @param Usuario el usuario login
     @param String estado del requerimiento
     @param long fecha de inicio de conteo
     *@return int
     */
    public int countSolicitudFuncionarioFechaActual(Usuario u, String s, long fecha) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            Path<Long> vence = rt.get("vence");
            c.select(cb.count(rt)).where(
                    cb.equal(rt.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(rt.get("idEstadoRequerimiento").get("estado"), s),
                    cb.gt(vence, fecha)//f respersenta al primer segundo del dia, o de mes o del año
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    /*cuenta las solicitudes de un usuario a partir d una fecha de inicio  segun el estado    
     @param String estado del requerimiento
     @param long fecha de inicio de conteo
     *@return int
     */
    public int countSolicitudSegunEstadoFechaInicioHastaHoy(String s, long fechaInicio) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            Path<Long> vence = rt.get("vence");
            c.select(cb.count(rt)).where(
                    // cb.equal(rt.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(rt.get("idEstadoRequerimiento").get("estado"), s),
                    cb.gt(vence, fechaInicio)//f respersenta al primer segundo del dia, o de mes o del año
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }
    /*cuenta las solicitudes de un usuario a partir d una fecha de inicio  segun el estado    
     @param EstadoRequerimiento estado del requerimiento
     @param long fecha de inicio de conteo
     *@return int
     */

    public int countSolicitudSegunEstadoFechaInicioHastaHoy(Estadorequerimiento s, long fechaInicio) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            Path<Long> vence = rt.get("vence");
            c.select(cb.count(rt)).where(
                    // cb.equal(rt.get("idAsignado").get("idEmpleado").get("idUsuario"), u),
                    cb.equal(rt.get("idEstadoRequerimiento"), s),
                    cb.gt(vence, fechaInicio)//f respersenta al primer segundo del dia, o de mes o del año
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    public int countSolicitudesSegunEstadoEnFechas(Estadorequerimiento s, long fechainicio, long fechafin) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> vence = sol.get("vence");
            c.select(cb.count(sol)).where(
                    cb.equal(sol.get("idEstadoRequerimiento"), s),
                    cb.between(vence, fechainicio, fechafin)
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    public int countSolicitudesSegunEstadoEnFechas(Estadorequerimiento s, long fechainicio, long fechafin, String fechaAtributo) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> fa = sol.get(fechaAtributo);
            c.select(cb.count(sol)).where(
                    cb.equal(sol.get("idEstadoRequerimiento"), s),
                    cb.between(fa, fechainicio, fechafin)
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    public int countSolicitudesSegunEstadoEnFechas(String s, long fechainicio, long fechafin) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> sol = c.from(Solicitud.class);
            Path<Long> vence = sol.get("vence");
            c.select(cb.count(sol)).where(
                    cb.equal(sol.get("idEstadoRequerimiento").get("estado"), s),
                    cb.between(vence, fechainicio, fechafin)
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

    /*cuenta las solicitudes de un usuario a partir d una fecha de inicio  segun el estado    
     @param EstadoRequerimiento estado del requerimiento
     @param Dependencia
     @param long fecha de inicio de conteo
     *@return int
     */
    public int countSolicitudDependenciaSegunEstadoFechaInicioHastaHoy(Estadorequerimiento s, Dependencia d, long fechaInicio) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            Path<Long> vence = rt.get("vence");
            c.select(cb.count(rt)).where(
                    cb.equal(rt.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(rt.get("idEstadoRequerimiento"), s),
                    cb.gt(vence, fechaInicio)//f respersenta al primer segundo del dia, o de mes o del año
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }
    /*cuenta las solicitudes de un usuario a partir d una fecha de inicio  segun el estado    
     @param String estado del requerimiento
     @param Dependencia
     @param long fecha de inicio de conteo
     *@return int
     */

    public int countSolicitudDependenciaSegunEstadoFechaInicioHastaHoy(String s, Dependencia d, long fechaInicio) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Long> c = cb.createQuery(Long.class);
            Root<Solicitud> rt = c.from(Solicitud.class);
            Path<Long> vence = rt.get("vence");
            c.select(cb.count(rt)).where(
                    cb.equal(rt.get("idEjeTematico").get("idServicio").get("idDependencia"), d),
                    cb.equal(rt.get("idEstadoRequerimiento").get("estado"), s),
                    cb.gt(vence, fechaInicio)//f respersenta al primer segundo del dia, o de mes o del año
            );
            Query q = getEntityManager().createQuery(c);
            return ((Long) q.getSingleResult()).intValue();
        } catch (Exception e) {
            return 0;//
        }
    }

}
