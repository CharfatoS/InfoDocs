package com.vur.ventanillaUnica.facade;

import Modelo.Tiposolicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class TipoSolicitudFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Tiposolicitud entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Tiposolicitud entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Tiposolicitud entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Tiposolicitud find(Object id) {
        return getEntityManager().find(Tiposolicitud.class, id);
    }

    public List<Tiposolicitud> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Tiposolicitud> rt = cq.from(Tiposolicitud.class);
        cq.select(cq.from(Tiposolicitud.class));
        cq.orderBy(cb.asc(rt.get("idTipoSolicitud")));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Tiposolicitud> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tiposolicitud.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Tiposolicitud> rt = cq.from(Tiposolicitud.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public Tiposolicitud findTipo(String tipo) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Tiposolicitud> consulta = cb.createQuery(Tiposolicitud.class);
            Root<Tiposolicitud> userW = consulta.from(Tiposolicitud.class);
            consulta.select(userW)
                    .where(cb.equal(userW.get("tipo"), tipo)
                    );
            Query q = getEntityManager().createQuery(consulta);
            Tiposolicitud us = (Tiposolicitud) q.getSingleResult();
            return us;
        } catch (Exception e) {
            return null;
        }
    }
}
