package com.vur.ventanillaUnica.servlet;


import com.vur.ventanillaUnica.bean.Adjunto;
import com.vur.ventanillaUnica.bean.SubirArchivosBean;
import com.vur.ventanillaUnica.bean.Entrada;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * @author dsilva 12
 */
@MultipartConfig
@WebServlet(name = "UploadServlet", urlPatterns = {"/servlet/upload"})
public class UploadServlet extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Part filePart = request.getPart("archivo"); //obtengo el archivo adjunto
        String nombreArchivo = extraerNombre(filePart.getHeader("content-disposition")); //extraigo el nombre

        InputStream is = filePart.getInputStream(); //obtengo el Stream
        long size = filePart.getSize(); //... el tamaño
        byte[] buffer = new byte[(int) size]; //.. creo el buffer
        is.read(buffer); //.. leo el buffer en un solo bloque
        is.close(); //... cierro el buffer
        String mimeType = filePart.getContentType(); //... obtengo el tipo de archivo
        Adjunto adjunto = new Adjunto(buffer, mimeType, nombreArchivo); //... creo el objeto ajdjunto
        SubirArchivosBean bitacoraBean = (SubirArchivosBean) request.getSession().getAttribute("subirArchivosBean"); //obtengo el bean
        Entrada entradaActual = bitacoraBean.getEntradaActual(); //.. obtengo la entrada actual que está con el formulario
        entradaActual.addAdjunto(adjunto); //... adjunto el objeto del archivo
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<html><head>"); //imprimo en el HTML para ejecutar un javascript
            out.println("<script type=\"text/javascript\">");
            out.println("window.opener.document.getElementById(\"form:recargar\").click()"); //busco el boton para recargar
            out.println("window.close()"); //cierro ventana
            out.println("</script>");
             out.println(" <style type=\"text/css\">");
            out.println("body {");    
            out.println("background-color:#C8CC88}");
            out.println("</style>");
            out.println("</head><body>");
             //out.println(""+nombreArchivo);
            out.println("</body></html>");
        } finally {
            out.close();
        }
    }

   
    static private String extraerNombre(String header) {
        String[] parts = header.split(";");
        for (String part : parts) {
            if (part.trim().startsWith("filename=")) { //busco todos los que comienzan con filename
                String[] $parts = part.split("="); //separo el nombre
                StringBuilder $sb = new StringBuilder($parts[1]);
                String fn = $sb.substring(1, $sb.length() - 1); //el segundo es el nombre
                File f = new File(fn); //creo una entrada de archivo
                return f.getName(); //devuelvo el nombre del archivo
            }
        }
        return "";
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "UploadServlet";
    }// </editor-fold>
}
