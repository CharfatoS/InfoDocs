package com.vur.ventanillaUnica.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlDataTable;
import java.io.Serializable;

/**
 * BitacoraBean
 *
 * @author dsilva
 */
@ManagedBean(name = "subirArchivosBean")
@SessionScoped
public class SubirArchivosBean implements Serializable {

    private Entrada entradaActual; //entrada actual
    private Adjunto adjunto;
    private HtmlDataTable adjuntosDataTable; //para enlazar al DataTable
    private double notaMaxima;
    

   

    public SubirArchivosBean() {
    }

    public Entrada getEntradaActual() {
        if (entradaActual == null) {
            entradaActual = new Entrada();
        }
        return entradaActual;
    }

    public String quitarArchivoSubir() {
        String h = quitarAdjunto("nuevoRequerimiento");
        return "cargarArchivo";
    }
    public String quitarAdjunto(Adjunto a) {
        //adjunto = a;
        entradaActual.quitarAdjunto(a);
        return "gestionRequerimientos";
    }

    public String quitarAdjunto(String pg) {
        adjunto = (Adjunto) adjuntosDataTable.getRowData();
        entradaActual.quitarAdjunto(adjunto);
        return pg;
    }
    
  

    /*---- Para Doumento  --*/
    private void asignarArchivoSeleccionado() {
        adjunto = (Adjunto) adjuntosDataTable.getRowData();
    }

    

    public void setEntradaActual(Entrada entradaActual) {
        this.entradaActual = entradaActual;
    }

    public HtmlDataTable getAdjuntosDataTable() {
        return adjuntosDataTable;
    }

    public void setAdjuntosDataTable(HtmlDataTable adjuntosDataTable) {
        this.adjuntosDataTable = adjuntosDataTable;
    }

    public Adjunto getXls() {
        return adjunto;
    }

    public void setXls(Adjunto adjunto) {
        this.adjunto = adjunto;
    }


    public double getNotaMaxima() {
        return notaMaxima;
    }

    public void setNotaMaxima(double notaMaxima) {
        this.notaMaxima = notaMaxima;
    }
}
