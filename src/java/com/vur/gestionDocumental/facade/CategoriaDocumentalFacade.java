
package com.vur.gestionDocumental.facade;

import Modelo.Categoriadocumental;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class CategoriaDocumentalFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Categoriadocumental entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Categoriadocumental entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Categoriadocumental entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Categoriadocumental find(Object id) {
        return getEntityManager().find(Categoriadocumental.class, id);
    }

    public List<Categoriadocumental> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Categoriadocumental.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Categoriadocumental> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Categoriadocumental.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Categoriadocumental> rt = cq.from(Categoriadocumental.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
