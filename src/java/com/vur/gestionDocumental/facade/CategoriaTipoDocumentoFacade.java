
package com.vur.gestionDocumental.facade;

import Modelo.Categoriatipodocumento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class CategoriaTipoDocumentoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Categoriatipodocumento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Categoriatipodocumento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Categoriatipodocumento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Categoriatipodocumento find(Object id) {
        return getEntityManager().find(Categoriatipodocumento.class, id);
    }

    public List<Categoriatipodocumento> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Categoriatipodocumento.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Categoriatipodocumento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Categoriatipodocumento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Categoriatipodocumento> rt = cq.from(Categoriatipodocumento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
