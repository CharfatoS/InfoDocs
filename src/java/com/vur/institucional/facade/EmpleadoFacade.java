
package com.vur.institucional.facade;

import Modelo.Dependencia;
import Modelo.Empleado;
import Modelo.Usuario;
import Modelo.Asignado;
import Modelo.Solicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class EmpleadoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Empleado entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Empleado entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Empleado entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Empleado find(Object id) {
        return getEntityManager().find(Empleado.class, id);
    }

    public List<Empleado> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Empleado.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Empleado> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Empleado.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Empleado> rt = cq.from(Empleado.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public Empleado verEmpleadoAsignadoDeSolicitud(Solicitud s){
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            Root<Empleado> rt = cq.from(Empleado.class);            
            cq.select(rt)
                    .where(cb.equal(rt.get("idAsignado").get("idSolicitud"), s)
                    );            
            Query q = getEntityManager().createQuery(cq); 
            List<Empleado> a =  q.getResultList();
            Empleado u = a.get(0);
            System.out.println("facade verEmpleadoAsignadoDeSolicitud "+u.toString());
            return u;
        } catch (Exception e) {
            return null;
        }
    }

    public Empleado verEmpleadoDeUsuario(Usuario u) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Empleado> consulta = cb.createQuery(Empleado.class);
            Root<Empleado> emp = consulta.from(Empleado.class);
            consulta.select(emp).where(cb.equal(emp.get("idUsuario"), u));
            Query q = getEntityManager().createQuery(consulta);
            Empleado us = (Empleado) q.getSingleResult();
            return us;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Empleado> verListaEmpleadoPorDependencia(Dependencia d) {
         try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Empleado> consulta = cb.createQuery(Empleado.class);
            Root<Empleado> emp = consulta.from(Empleado.class);
            consulta.select(emp).where(cb.equal(emp.get("idCargo").get("idDependencia"), d));
            consulta.orderBy(cb.asc(emp.get("idUsuario").get("apellido")));
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    public Empleado verEmpleadoDeAsignado(Asignado a) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Empleado> consulta = cb.createQuery(Empleado.class);
            Root<Empleado> emp = consulta.from(Empleado.class);
            consulta.select(emp).where(cb.equal(emp.get("idAsignado"), a));            
            Query q = getEntityManager().createQuery(consulta);
            return (Empleado) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
