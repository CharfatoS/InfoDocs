
package com.vur.institucional.facade;

import Modelo.Institucion;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class InstitucionFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Institucion entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Institucion entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Institucion entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Institucion find(Object id) {
        return getEntityManager().find(Institucion.class, id);
    }

    public List<Institucion> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Institucion.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Institucion> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Institucion.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Institucion> rt = cq.from(Institucion.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
