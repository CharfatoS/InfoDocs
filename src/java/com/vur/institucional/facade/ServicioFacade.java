
package com.vur.institucional.facade;

import Modelo.Dependencia;
import Modelo.Servicio;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class ServicioFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Servicio entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Servicio entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Servicio entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Servicio find(Object id) {
        return getEntityManager().find(Servicio.class, id);
    }

    public List<Servicio> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Servicio.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Servicio> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Servicio.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Servicio> rt = cq.from(Servicio.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public boolean verifiqueServicioNombre(String n){
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Servicio> consulta = cb.createQuery(Servicio.class);
            Root<Servicio> servicioW = consulta.from(Servicio.class);
            consulta.select(servicioW).where(cb.equal(servicioW.get("nombre"), n));
            Query q = getEntityManager().createQuery(consulta);
            Servicio s = (Servicio) q.getSingleResult();
            return (s != null);
            
        } catch (Exception e) {
            return false;
        }
    }

    public List<Servicio> verServiciosDependencia(Dependencia d) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Servicio> consulta = cb.createQuery(Servicio.class);
            Root<Servicio> servicioW = consulta.from(Servicio.class);
            consulta.select(servicioW)
                    .where(cb.equal(servicioW.get("idDependencia"), d)
                    );
             consulta.orderBy(cb.asc(servicioW.get("nombre")));
            Query q = getEntityManager().createQuery(consulta);
           
            return q.getResultList();
            
        } catch (Exception e) {
            return null;
        }
    }

}
