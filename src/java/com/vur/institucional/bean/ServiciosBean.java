package com.vur.institucional.bean;

import com.vur.controller.util.JsfUtil;
import Modelo.Categoria;
import Modelo.Dependencia;
import Modelo.Empleado;
import Modelo.Servicio;
import com.vur.institucional.facade.CategoriaFacade;
import com.vur.institucional.facade.EmpleadoFacade;
import com.vur.institucional.facade.ServicioFacade;
import com.vur.usuario.bean.LoginBean;
import com.vur.ventanillaUnica.bean.SolicitudBean;
import Modelo.Ejetematico;
import Modelo.Solicitud;
import com.vur.ventanillaUnica.facade.EjeTematicoFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author jesus
 */
@ManagedBean(name = "serviciosBean")
@SessionScoped
public class ServiciosBean implements Serializable {

    private Solicitud solicitud;
    private Ejetematico ejeTematico;
    private Empleado empleado;
    private Categoria categoria;
    private Servicio servicio;
    private Dependencia dependencia;
    private int elDiv;

    @EJB
    private CategoriaFacade categoriaEjb;
    @EJB
    private ServicioFacade servicioEjb;
    @EJB
    private EjeTematicoFacade ejeTematicoEjb;
    @EJB
    private EmpleadoFacade empleadoEjb;

    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{solicitudBean}")
    private SolicitudBean solicitudBean;

    public ServiciosBean() {
    }

    public String actualizarCrearCategoria() {
        if (categoria.getIdcategoria()> 0) {
            updateCategoria();
        } else if (categoria.getIdcategoria() < 0) {
            boolean existe = consulteNombreCategoria(categoria.getNombre());
//            JsfUtil.addSuccessMessage(categoria.getIdCategoria()+" "+categoria.getNombre()+" "+categoria.getDescripcion());
            if (existe) {
                JsfUtil.addSuccessMessage("El nombre de la categoría ya existe");
                return "gestionarServicios";
            } else {
                createCategoria();
                JsfUtil.addSuccessMessage("Se ha creado correctamente la categoría " + categoria.getNombre());
                categoria = new Categoria();
                return "gestionServicios";
            }
        }
        return null;
    }

    public String actualizarCrearServicio() {
        elDiv = 0;
        if (servicio.getIdservicio()> 0) {
            updateServicio();
        } else if (servicio.getIdservicio() < 0) {
            createServicio();
            JsfUtil.addSuccessMessage("Se ha creado correctamente el servicio " + servicio.getNombre());
            //servicio = new Servicio(); 
        }
        return "gestionServicios";
    }

    public String actualizarCrearEjeTematico() {
        if (ejeTematico.getIdejetematico()> 0) {
            updateEjeTematico();
        } else if (ejeTematico.getIdejetematico() < 0) {
            createEjeTematico();
            JsfUtil.addSuccessMessage("Se ha creado correctamente el eje temático " + ejeTematico.getTema());
            ejeTematico = new Ejetematico();
            return "gestionServicios";
        }
        return null;
    }

    public boolean consulteNombreCategoria(String n) {
        try {
            return getServicioEjb().verifiqueServicioNombre(n);
        } catch (Exception e) {
            return false;
        }

    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    public String visualizarCategoria() {
        elDiv = 1;
        categoria = new Categoria();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionServicios";
    }

    public String[] getObligatoriosSiNo() {
        String[] s = {"---", "Si", "No"};
        return s;
    }

    public String visualizarListaServiciosDependencia() {
        Dependencia d = getDependenciaFuncionario();
        elDiv = 0;
        JsfUtil.addSuccessMessage("Lista de servicios de la dependencia  " + d.toString());
        return "gestionServicios";
    }

    public String visualizarListaServiciosDependenciaEnSuper() {
        try {
            elDiv = 0;
            List<Servicio> j = getListaServiciosDependencia();
            JsfUtil.addSuccessMessage("Lista de servicios de la dependencia en super ");
        } catch (Exception e) {
            elDiv = 0;
            //
        }

        return "gestionServicios";
    }

    public List<Servicio> getListaServiciosDependencia() {
        try {
            return getServicioEjb().verServiciosDependencia(dependencia);
        } catch (Exception e) {
            return null;
        }
    }

    public Dependencia getDependenciaFuncionario() {
        Dependencia d = new Dependencia();
        try {
            d = getLoginBean().getEmpleado().getIdCargo().getIdDependencia();
        } catch (Exception e) {
        }
        return d;
    }

    public List<Servicio> verListaServiciosDependenciaFuncionario(Dependencia d) {
        try {
            return getServicioEjb().verServiciosDependencia(d);
        } catch (Exception e) {
            return null;
        }
    }

    public String visualizarServicio(Servicio item) {
        try {
            if (dependencia == null) {
                Dependencia d = getLoginBean().getEmpleado().getIdCargo().getIdDependencia();
            }
        } catch (Exception e) {
        }

        elDiv = 1;
        servicio = item;
        JsfUtil.addSuccessMessage("Datos del servicio seleccionado");
        return "gestionServicios";
    }

    public String verEditarServicio() {
        elDiv = 2;
        return "gestionServicios";
    }

    public String iniciarServicioEnSuper() {
        elDiv = 2;
        servicio = new Servicio();
        servicio.setIddependencia(dependencia);
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionServicios";
    }

    public String iniciarServicio() {
        Dependencia d = getLoginBean().getEmpleado().getIdCargo().getIdDependencia();
        elDiv = 2;
        servicio = new Servicio();
        servicio.setIddependencia(getDependenciaFuncionario());
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionServicios";
    }

    public String verListaEjeTematico() {
        elDiv = 3;
        //JsfUtil.addSuccessMessage("lista de ejes tematicos el div  " + elDiv);
        return "gestionServicios";
    }

    public String verEjeTematicoSeleccionado(Ejetematico item) {
        elDiv = 4;
        ejeTematico = item;
        //JsfUtil.addSuccessMessage("ver ejes tematicos seleccionado el div  " + elDiv);
        return "gestionServicios";
    }

    public String irEditarEjeTematico() {
        elDiv = 5;
        JsfUtil.addSuccessMessage("ver ejes tematicos seleccionado el div  " + elDiv);
        return "gestionServicios";
    }

    public String commitEjeTematico() {
        elDiv = 4;
        JsfUtil.addSuccessMessage("createEjeTematico el id  " + ejeTematico.getIdservicio().toString());
        if (ejeTematico.getIdejetematico()< 0) {
            // crea eje tematico
            createEjeTematico();
            JsfUtil.addSuccessMessage("createEjeTematico el id  " + ejeTematico.getIdejetematico());
        } else if (ejeTematico.getIdejetematico() > 0) {
            //edita
            updateEjeTematico();
            JsfUtil.addSuccessMessage("updateEjeTematico el id  " + ejeTematico.getIdejetematico());
        }
        JsfUtil.addSuccessMessage("ver ejes tematicos seleccionado el div  " + elDiv);
        return "gestionServicios";
    }

    public String nuevoEjeTematico() {
        elDiv = 5;
        ejeTematico = new Ejetematico();
        ejeTematico.setTiemporespuesta(5);
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionServicios";
    }

    public String nuevoEjeTematicoEnSuper() {
        elDiv = 5;
        ejeTematico = new Ejetematico();
        ejeTematico.setTiemporespuesta(10);
        ejeTematico.setIdservicio(servicio);
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionServicios";
    }

    public SelectItem[] verItemsServiciosDedependencia(Dependencia d) {
        return JsfUtil.getSelectItems(getServicioEjb().verServiciosDependencia(d), true);
    }

    public List<Ejetematico> getListaTemasDeServicio() {
        try {
            return getEjeTematicoEjb().verEjeTematicoDeServicio(servicio);
        } catch (Exception e) {
            return null;
        }
    }

    public Ejetematico getEjeTematico() {
        return ejeTematico;
    }

    public EjeTematicoFacade getEjeTematicoEjb() {
        return ejeTematicoEjb;
    }

    private EmpleadoFacade getEmpleadoEjb() {
        return empleadoEjb;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public SolicitudBean getSolicitudBean() {
        return solicitudBean;
    }

    public void setSolicitudBean(SolicitudBean solicitudBean) {
        this.solicitudBean = solicitudBean;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    private CategoriaFacade getCategoriaEjb() {
        return categoriaEjb;
    }

    private ServicioFacade getServicioEjb() {
        return servicioEjb;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }

    private void createCategoria() {
        try {
            getCategoriaEjb().create(categoria);
        } catch (Exception e) {
            //
        }
    }

    private void updateCategoria() {
        try {
            getCategoriaEjb().edit(categoria);
        } catch (Exception e) {
        }
    }

    private void updateServicio() {
        try {
            getServicioEjb().edit(servicio);
        } catch (Exception e) {
        }
    }

    private void createServicio() {
        try {
            getServicioEjb().create(servicio);
        } catch (Exception e) {
        }
    }

    private void updateEjeTematico() {
       // String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
              //  id = id + ejeTematico.getDescripcion();
            } catch (Exception e) {
            }
            //ejeTematico.setDescripcion(id);
            getEjeTematicoEjb().edit(ejeTematico);
        } catch (Exception e) {
        }
    }

    private void createEjeTematico() {
        //String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
               // id = id + ejeTematico.getDescripcion();
            } catch (Exception e) {
            }

           // ejeTematico.setDescripcion(id);
            getEjeTematicoEjb().create(ejeTematico);
        } catch (Exception e) {
        }

    }

}
