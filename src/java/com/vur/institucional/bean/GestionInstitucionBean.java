
package com.vur.institucional.bean;

import com.vur.controller.util.JsfUtil;
import Modelo.Institucion;
import com.vur.institucional.facade.InstitucionFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jesusmarin
 */
@ManagedBean(name = "gestionInstitucionBean")
@SessionScoped
public class GestionInstitucionBean implements Serializable {

    private Institucion institucion;
    private int elDiv;

    @EJB
    private InstitucionFacade institucionEjb;

    public GestionInstitucionBean() {
    }

    public Institucion getSeleccion() {
        institucion = verInstitucionBD();
        if (institucion == null) {
            institucion = new Institucion();
        }
        return institucion;
    }
    
    public boolean verifiqueInstitucion(){
        institucion = getSeleccion();
        if(institucion.getIdinstitucion() > 0){
            return true;
        }else{
            return false;
        }
        
    }
    
    public  String[] getItemsTipoInstitucion(){
        String[] l = {"---", "Pública","Privada", "Mixta"};
        return l;
    }
    
    public  String[] getItemsCaracterInstitucion(){
        String[] l = {"---", "Nacional","Departamental", "Municipal", "Otra"};
        return l;
    }

    private Institucion verInstitucionBD() {
        try {
            return getInstitucionEjb().find(1);
        } catch (Exception e) {
            return null; //
        }
    }

    private InstitucionFacade getInstitucionEjb() {
        return institucionEjb;
    }
    
    public String irInstitucion(){
        boolean v = verifiqueInstitucion();
        if (v) {
            JsfUtil.addSuccessMessage("La "+getSeleccion().toString());
            elDiv = 2;
        }else {
            elDiv = 1;
        }
        return "gestionInstitucion";
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }
    
    public String crearActualizarInstitucion(){
        elDiv = 2;
        if(institucion.getIdinstitucion()< 0){
            createInstitucion();
            institucion = getSeleccion();
        } else if (institucion.getIdinstitucion() > 0){
            updateInstitucion();
            institucion = getSeleccion();
        }        
        return "gestionInstitucion";
    }

    private void createInstitucion() {
        try {
            institucionEjb.create(institucion);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha creado la institución, error: "+e);
        }
    }

    private void updateInstitucion() {
        try {
            institucionEjb.edit(institucion);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha actualizado la institución, error: "+e);
        }
    }
    
    public String cargarVistaEditar(){
        elDiv = 3;
        return "gestionInstitucion";
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }
    
    
}
