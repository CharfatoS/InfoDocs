package com.vur.institucional.bean;

import com.vur.controller.util.JsfUtil;
import Modelo.Cargo;
import Modelo.Dependencia;
import com.vur.institucional.facade.CargoFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jesusmarin
 */
@ManagedBean(name = "gestionCargoBean")
@SessionScoped
public class GestionCargoBean implements Serializable {

    private Cargo cargo;
    private Dependencia dependencia;
    private int elDiv;

    @EJB
    private CargoFacade cargoEjb;
    @ManagedProperty(value = "#{gestionDependenciaBean}")
    private GestionDependenciaBean gestionDependenciaBean;

    public GestionCargoBean() {
    }

    public String verCargos(){
        elDiv = 1;
        return "gestionCargo";
    }
    
    public String verListaCargosDeDependencia(){  
         elDiv = 1;
         return "gestionCargo";
    }
    
    public String irCargo() {
        boolean v = verificarCargo();
        if (v) {
            elDiv = 1;
        } else {
            elDiv = 2;
        }
        return "gestionCargo";
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    private CargoFacade getCargoEjb() {
        return cargoEjb;
    }

    public boolean verificarCargo() {
        if (cargo == null) {
            return true;
        } else {
            return false;
        }
    }
    

    public List<Cargo> getSeleccionLista() {       
        return verTodosLosCargos();
    }
    
     public List<Cargo> getListaCargosDeDependencia() {
        try {
            return getCargoEjb().verCargosDeDependencia(dependencia);
        } catch (Exception e) {
            return null;
        }
    }

    private List<Cargo> verTodosLosCargos() {
        try {
            return getCargoEjb().findAll();
        } catch (Exception e) {
            return null;
        }
    }

    public String crearActualizarCargo() {
        elDiv = 1;
        if (cargo.getIdcargo()< 0) {
            createCargo();            
        } else if (cargo.getIdcargo() > 0) {
            updateCargo();            
        }
        return "gestionCargo";
    }

    private void createCargo() {
        try {
            //cargo.setIdInstitucion(gestionInstitucionBean.getSeleccion());
            cargoEjb.create(cargo);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha creado el cargo, error: " + e);
        }
    }

    private void updateCargo() {
        try {
            cargoEjb.edit(cargo);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha actualizado el cargo, error: " + e);
        }
    }

    public String cargarVistaNuevo() {
        elDiv = 3;
        cargo = new Cargo();
        cargo.setEstado(true); 
        cargo.setIddependencia(dependencia);
        return "gestionCargo";
    }

    public String cargarVistaEditar() {
        elDiv = 3;
        return "gestionCargo";
    }

    public String visualizarCargo(Cargo item) {
        elDiv = 3;
        //JsfUtil.addSuccessMessage(""+item.getNombre());
        cargo = item;
        return "gestionCargo";
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }
    

    public GestionDependenciaBean getGestionDependenciaBean() {
        return gestionDependenciaBean;
    }

    public void setGestionDependenciaBean(GestionDependenciaBean gestionDependenciaBean) {
        this.gestionDependenciaBean = gestionDependenciaBean;
    }

}
