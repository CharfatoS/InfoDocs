
package com.vur.geo.facade;

import Modelo.Ciudad;
import Modelo.Lugar;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesus
 */
@Stateless
public class LugarFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Lugar entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Lugar entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Lugar entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Lugar find(Object id) {
        return getEntityManager().find(Lugar.class, id);
    }

    public List<Lugar> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Lugar.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Lugar> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Lugar.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Lugar> rt = cq.from(Lugar.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Lugar> getLugarDeCiudad(Ciudad d) {
         try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Lugar> consulta = cb.createQuery(Lugar.class);
            Root<Lugar> userW = consulta.from(Lugar.class);
            consulta.select(userW).where(cb.equal(userW.get("idCiudad"), d));
            //consulta.orderBy(cb.asc(userW.get("nombre")));
            Query q = getEntityManager().createQuery(consulta);            
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
       
    }
}

    

