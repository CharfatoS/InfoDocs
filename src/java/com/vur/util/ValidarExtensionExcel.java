package com.vur.util;

/**
 * valida si el excel ees xls o xlsx
 *
 * @author JesusMarin
 */
public class ValidarExtensionExcel {

    public ValidarExtensionExcel() {
    }

    public int validarExtension(String fileName) {
        String extension = null;
        try {
            extension = fileName.substring(fileName.lastIndexOf("."),
                    fileName.length());
            if (extension.equals(".xlsx") == true) {
                return 0; // retorna 0 si es xlsx
            } else if (extension.equals(".xls") == true) {
                return 1;//retorna 1 si es xls
            } else {
                return -1;//retorna -1 si es xls
            }
        } catch (Exception e) {
            //
            return -1;
        }

    }
}
