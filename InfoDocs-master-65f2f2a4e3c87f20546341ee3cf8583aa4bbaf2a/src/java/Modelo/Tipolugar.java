/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"nombre"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipolugar.findAll", query = "SELECT t FROM Tipolugar t"),
    @NamedQuery(name = "Tipolugar.findByIdtipolugar", query = "SELECT t FROM Tipolugar t WHERE t.idtipolugar = :idtipolugar"),
    @NamedQuery(name = "Tipolugar.findByNombre", query = "SELECT t FROM Tipolugar t WHERE t.nombre = :nombre")})
public class Tipolugar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtipolugar;
    @Size(max = 255)
    @Column(length = 255)
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipolugar", fetch = FetchType.EAGER)
    private List<Lugar> lugarList;

    public Tipolugar() {
    }

    public Tipolugar(Integer idtipolugar) {
        this.idtipolugar = idtipolugar;
    }

    public Integer getIdtipolugar() {
        return idtipolugar;
    }

    public void setIdtipolugar(Integer idtipolugar) {
        this.idtipolugar = idtipolugar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Lugar> getLugarList() {
        return lugarList;
    }

    public void setLugarList(List<Lugar> lugarList) {
        this.lugarList = lugarList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipolugar != null ? idtipolugar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipolugar)) {
            return false;
        }
        Tipolugar other = (Tipolugar) object;
        if ((this.idtipolugar == null && other.idtipolugar != null) || (this.idtipolugar != null && !this.idtipolugar.equals(other.idtipolugar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tipolugar[ idtipolugar=" + idtipolugar + " ]";
    }
    
}
