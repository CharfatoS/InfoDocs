/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccionusuario.findAll", query = "SELECT d FROM Direccionusuario d"),
    @NamedQuery(name = "Direccionusuario.findByIddireccionusuario", query = "SELECT d FROM Direccionusuario d WHERE d.iddireccionusuario = :iddireccionusuario"),
    @NamedQuery(name = "Direccionusuario.findByFechainicial", query = "SELECT d FROM Direccionusuario d WHERE d.fechainicial = :fechainicial"),
    @NamedQuery(name = "Direccionusuario.findByFechafinal", query = "SELECT d FROM Direccionusuario d WHERE d.fechafinal = :fechafinal"),
    @NamedQuery(name = "Direccionusuario.findByEstado", query = "SELECT d FROM Direccionusuario d WHERE d.estado = :estado")})
public class Direccionusuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer iddireccionusuario;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechainicial;
    @Temporal(TemporalType.DATE)
    private Date fechafinal;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short estado;
    @JoinColumn(name = "iddireccion", referencedColumnName = "iddireccion", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Direccion iddireccion;
    @JoinColumn(name = "idusuario", referencedColumnName = "docid", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario idusuario;

    public Direccionusuario() {
    }

    public Direccionusuario(Integer iddireccionusuario) {
        this.iddireccionusuario = iddireccionusuario;
    }

    public Direccionusuario(Integer iddireccionusuario, Date fechainicial, short estado) {
        this.iddireccionusuario = iddireccionusuario;
        this.fechainicial = fechainicial;
        this.estado = estado;
    }

    public Integer getIddireccionusuario() {
        return iddireccionusuario;
    }

    public void setIddireccionusuario(Integer iddireccionusuario) {
        this.iddireccionusuario = iddireccionusuario;
    }

    public Date getFechainicial() {
        return fechainicial;
    }

    public void setFechainicial(Date fechainicial) {
        this.fechainicial = fechainicial;
    }

    public Date getFechafinal() {
        return fechafinal;
    }

    public void setFechafinal(Date fechafinal) {
        this.fechafinal = fechafinal;
    }

    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

    public Direccion getIddireccion() {
        return iddireccion;
    }

    public void setIddireccion(Direccion iddireccion) {
        this.iddireccion = iddireccion;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddireccionusuario != null ? iddireccionusuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccionusuario)) {
            return false;
        }
        Direccionusuario other = (Direccionusuario) object;
        if ((this.iddireccionusuario == null && other.iddireccionusuario != null) || (this.iddireccionusuario != null && !this.iddireccionusuario.equals(other.iddireccionusuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Direccionusuario[ iddireccionusuario=" + iddireccionusuario + " ]";
    }
    
}
