/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RespuestaFAQ.findAll", query = "SELECT r FROM RespuestaFAQ r"),
    @NamedQuery(name = "RespuestaFAQ.findByIdrespuestafaq", query = "SELECT r FROM RespuestaFAQ r WHERE r.idrespuestafaq = :idrespuestafaq"),
    @NamedQuery(name = "RespuestaFAQ.findByEstado", query = "SELECT r FROM RespuestaFAQ r WHERE r.estado = :estado"),
    @NamedQuery(name = "RespuestaFAQ.findByNumerica", query = "SELECT r FROM RespuestaFAQ r WHERE r.numerica = :numerica"),
    @NamedQuery(name = "RespuestaFAQ.findByTexto", query = "SELECT r FROM RespuestaFAQ r WHERE r.texto = :texto")})
public class RespuestaFAQ implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idrespuestafaq;
    private Short estado;
    private Integer numerica;
    @Size(max = 255)
    @Column(length = 255)
    private String texto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrespuestafaq", fetch = FetchType.EAGER)
    private List<Preguntafrecuente> preguntafrecuenteList;

    public RespuestaFAQ() {
    }

    public RespuestaFAQ(Integer idrespuestafaq) {
        this.idrespuestafaq = idrespuestafaq;
    }

    public Integer getIdrespuestafaq() {
        return idrespuestafaq;
    }

    public void setIdrespuestafaq(Integer idrespuestafaq) {
        this.idrespuestafaq = idrespuestafaq;
    }

    public Short getEstado() {
        return estado;
    }

    public void setEstado(Short estado) {
        this.estado = estado;
    }

    public Integer getNumerica() {
        return numerica;
    }

    public void setNumerica(Integer numerica) {
        this.numerica = numerica;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @XmlTransient
    public List<Preguntafrecuente> getPreguntafrecuenteList() {
        return preguntafrecuenteList;
    }

    public void setPreguntafrecuenteList(List<Preguntafrecuente> preguntafrecuenteList) {
        this.preguntafrecuenteList = preguntafrecuenteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrespuestafaq != null ? idrespuestafaq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaFAQ)) {
            return false;
        }
        RespuestaFAQ other = (RespuestaFAQ) object;
        if ((this.idrespuestafaq == null && other.idrespuestafaq != null) || (this.idrespuestafaq != null && !this.idrespuestafaq.equals(other.idrespuestafaq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.RespuestaFAQ[ idrespuestafaq=" + idrespuestafaq + " ]";
    }
    
}
