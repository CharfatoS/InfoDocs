/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"mail"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u"),
    @NamedQuery(name = "Users.findByMail", query = "SELECT u FROM Users u WHERE u.mail = :mail"),
    @NamedQuery(name = "Users.findByLogusers", query = "SELECT u FROM Users u WHERE u.logusers = :logusers"),
    @NamedQuery(name = "Users.findByNumaccesos", query = "SELECT u FROM Users u WHERE u.numaccesos = :numaccesos"),
    @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password"),
    @NamedQuery(name = "Users.findByUltimoacceso", query = "SELECT u FROM Users u WHERE u.ultimoacceso = :ultimoacceso")})
public class Users implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String mail;
    @Size(max = 255)
    @Column(length = 255)
    private String logusers;
    private Integer numaccesos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String password;
    @Temporal(TemporalType.DATE)
    private Date ultimoacceso;
    @JoinColumn(name = "rol", referencedColumnName = "rol", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Rol rol;
    @JoinColumn(name = "idusuario", referencedColumnName = "docid", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario idusuario;

    public Users() {
    }

    public Users(String mail) {
        this.mail = mail;
    }

    public Users(String mail, String password) {
        this.mail = mail;
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLogusers() {
        return logusers;
    }

    public void setLogusers(String logusers) {
        this.logusers = logusers;
    }

    public Integer getNumaccesos() {
        return numaccesos;
    }

    public void setNumaccesos(Integer numaccesos) {
        this.numaccesos = numaccesos;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUltimoacceso() {
        return ultimoacceso;
    }

    public void setUltimoacceso(Date ultimoacceso) {
        this.ultimoacceso = ultimoacceso;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Usuario getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Usuario idusuario) {
        this.idusuario = idusuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mail != null ? mail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.mail == null && other.mail != null) || (this.mail != null && !this.mail.equals(other.mail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Users[ mail=" + mail + " ]";
    }
    
}
