/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pregunta.findAll", query = "SELECT p FROM Pregunta p"),
    @NamedQuery(name = "Pregunta.findByIdpregunta", query = "SELECT p FROM Pregunta p WHERE p.idpregunta = :idpregunta"),
    @NamedQuery(name = "Pregunta.findByNumrespuestas", query = "SELECT p FROM Pregunta p WHERE p.numrespuestas = :numrespuestas"),
    @NamedQuery(name = "Pregunta.findByPregunta", query = "SELECT p FROM Pregunta p WHERE p.pregunta = :pregunta")})
public class Pregunta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idpregunta;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int numrespuestas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String pregunta;
    @JoinColumn(name = "idencuesta", referencedColumnName = "idencuesta", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Encuesta idencuesta;
    @JoinColumn(name = "idtiporespuesta", referencedColumnName = "idtiporespuesta", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tiporespuesta idtiporespuesta;

    public Pregunta() {
    }

    public Pregunta(Integer idpregunta) {
        this.idpregunta = idpregunta;
    }

    public Pregunta(Integer idpregunta, int numrespuestas, String pregunta) {
        this.idpregunta = idpregunta;
        this.numrespuestas = numrespuestas;
        this.pregunta = pregunta;
    }

    public Integer getIdpregunta() {
        return idpregunta;
    }

    public void setIdpregunta(Integer idpregunta) {
        this.idpregunta = idpregunta;
    }

    public int getNumrespuestas() {
        return numrespuestas;
    }

    public void setNumrespuestas(int numrespuestas) {
        this.numrespuestas = numrespuestas;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Encuesta getIdencuesta() {
        return idencuesta;
    }

    public void setIdencuesta(Encuesta idencuesta) {
        this.idencuesta = idencuesta;
    }

    public Tiporespuesta getIdtiporespuesta() {
        return idtiporespuesta;
    }

    public void setIdtiporespuesta(Tiporespuesta idtiporespuesta) {
        this.idtiporespuesta = idtiporespuesta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpregunta != null ? idpregunta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pregunta)) {
            return false;
        }
        Pregunta other = (Pregunta) object;
        if ((this.idpregunta == null && other.idpregunta != null) || (this.idpregunta != null && !this.idpregunta.equals(other.idpregunta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Pregunta[ idpregunta=" + idpregunta + " ]";
    }
    
}
