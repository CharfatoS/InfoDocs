/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s"),
    @NamedQuery(name = "Servicio.findByIdservicio", query = "SELECT s FROM Servicio s WHERE s.idservicio = :idservicio"),
    @NamedQuery(name = "Servicio.findByNombre", query = "SELECT s FROM Servicio s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Servicio.findByObligatorio", query = "SELECT s FROM Servicio s WHERE s.obligatorio = :obligatorio"),
    @NamedQuery(name = "Servicio.findByTiemporespuesta", query = "SELECT s FROM Servicio s WHERE s.tiemporespuesta = :tiemporespuesta")})
public class Servicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idservicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private short obligatorio;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    private int tiemporespuesta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idservicio", fetch = FetchType.EAGER)
    private List<Ejetematico> ejetematicoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idservicio", fetch = FetchType.EAGER)
    private List<Preguntafrecuente> preguntafrecuenteList;
    @JoinColumn(name = "idcategoria", referencedColumnName = "idcategoria", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Categoria idcategoria;
    @JoinColumn(name = "iddependencia", referencedColumnName = "iddependencia", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Dependencia iddependencia;

    public Servicio() {
    }

    public Servicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public Servicio(Integer idservicio, String nombre, short obligatorio, int tiemporespuesta) {
        this.idservicio = idservicio;
        this.nombre = nombre;
        this.obligatorio = obligatorio;
        this.tiemporespuesta = tiemporespuesta;
    }

    public Integer getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(Integer idservicio) {
        this.idservicio = idservicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(short obligatorio) {
        this.obligatorio = obligatorio;
    }

    public int getTiemporespuesta() {
        return tiemporespuesta;
    }

    public void setTiemporespuesta(int tiemporespuesta) {
        this.tiemporespuesta = tiemporespuesta;
    }

    @XmlTransient
    public List<Ejetematico> getEjetematicoList() {
        return ejetematicoList;
    }

    public void setEjetematicoList(List<Ejetematico> ejetematicoList) {
        this.ejetematicoList = ejetematicoList;
    }

    @XmlTransient
    public List<Preguntafrecuente> getPreguntafrecuenteList() {
        return preguntafrecuenteList;
    }

    public void setPreguntafrecuenteList(List<Preguntafrecuente> preguntafrecuenteList) {
        this.preguntafrecuenteList = preguntafrecuenteList;
    }

    public Categoria getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(Categoria idcategoria) {
        this.idcategoria = idcategoria;
    }

    public Dependencia getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(Dependencia iddependencia) {
        this.iddependencia = iddependencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idservicio != null ? idservicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.idservicio == null && other.idservicio != null) || (this.idservicio != null && !this.idservicio.equals(other.idservicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Servicio[ idservicio=" + idservicio + " ]";
    }
    
}
