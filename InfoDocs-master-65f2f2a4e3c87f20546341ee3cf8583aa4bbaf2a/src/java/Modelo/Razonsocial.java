/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"razonsocial"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Razonsocial.findAll", query = "SELECT r FROM Razonsocial r"),
    @NamedQuery(name = "Razonsocial.findByIdrazonsocial", query = "SELECT r FROM Razonsocial r WHERE r.idrazonsocial = :idrazonsocial"),
    @NamedQuery(name = "Razonsocial.findByRazonsocial", query = "SELECT r FROM Razonsocial r WHERE r.razonsocial = :razonsocial")})
public class Razonsocial implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idrazonsocial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String razonsocial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idrazonsocial", fetch = FetchType.EAGER)
    private List<Empjuridico> empjuridicoList;

    public Razonsocial() {
    }

    public Razonsocial(Integer idrazonsocial) {
        this.idrazonsocial = idrazonsocial;
    }

    public Razonsocial(Integer idrazonsocial, String razonsocial) {
        this.idrazonsocial = idrazonsocial;
        this.razonsocial = razonsocial;
    }

    public Integer getIdrazonsocial() {
        return idrazonsocial;
    }

    public void setIdrazonsocial(Integer idrazonsocial) {
        this.idrazonsocial = idrazonsocial;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    @XmlTransient
    public List<Empjuridico> getEmpjuridicoList() {
        return empjuridicoList;
    }

    public void setEmpjuridicoList(List<Empjuridico> empjuridicoList) {
        this.empjuridicoList = empjuridicoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrazonsocial != null ? idrazonsocial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Razonsocial)) {
            return false;
        }
        Razonsocial other = (Razonsocial) object;
        if ((this.idrazonsocial == null && other.idrazonsocial != null) || (this.idrazonsocial != null && !this.idrazonsocial.equals(other.idrazonsocial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Razonsocial[ idrazonsocial=" + idrazonsocial + " ]";
    }
    
}
