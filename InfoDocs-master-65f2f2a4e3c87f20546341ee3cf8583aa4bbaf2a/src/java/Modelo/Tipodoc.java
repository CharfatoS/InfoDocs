/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"tipodoc"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipodoc.findAll", query = "SELECT t FROM Tipodoc t"),
    @NamedQuery(name = "Tipodoc.findByIdtipodoc", query = "SELECT t FROM Tipodoc t WHERE t.idtipodoc = :idtipodoc"),
    @NamedQuery(name = "Tipodoc.findByTipodoc", query = "SELECT t FROM Tipodoc t WHERE t.tipodoc = :tipodoc")})
public class Tipodoc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtipodoc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(nullable = false, length = 45)
    private String tipodoc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipodoc", fetch = FetchType.EAGER)
    private List<Usuario> usuarioList;

    public Tipodoc() {
    }

    public Tipodoc(Integer idtipodoc) {
        this.idtipodoc = idtipodoc;
    }

    public Tipodoc(Integer idtipodoc, String tipodoc) {
        this.idtipodoc = idtipodoc;
        this.tipodoc = tipodoc;
    }

    public Integer getIdtipodoc() {
        return idtipodoc;
    }

    public void setIdtipodoc(Integer idtipodoc) {
        this.idtipodoc = idtipodoc;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipodoc != null ? idtipodoc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipodoc)) {
            return false;
        }
        Tipodoc other = (Tipodoc) object;
        if ((this.idtipodoc == null && other.idtipodoc != null) || (this.idtipodoc != null && !this.idtipodoc.equals(other.idtipodoc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tipodoc[ idtipodoc=" + idtipodoc + " ]";
    }
    
}
