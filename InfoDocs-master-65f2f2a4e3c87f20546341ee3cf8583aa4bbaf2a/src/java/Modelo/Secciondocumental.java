/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Secciondocumental.findAll", query = "SELECT s FROM Secciondocumental s"),
    @NamedQuery(name = "Secciondocumental.findByIdsecciondocumental", query = "SELECT s FROM Secciondocumental s WHERE s.idsecciondocumental = :idsecciondocumental"),
    @NamedQuery(name = "Secciondocumental.findByCodigo", query = "SELECT s FROM Secciondocumental s WHERE s.codigo = :codigo"),
    @NamedQuery(name = "Secciondocumental.findByProcedimiento", query = "SELECT s FROM Secciondocumental s WHERE s.procedimiento = :procedimiento"),
    @NamedQuery(name = "Secciondocumental.findBySeccion", query = "SELECT s FROM Secciondocumental s WHERE s.seccion = :seccion")})
public class Secciondocumental implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idsecciondocumental;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String procedimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String seccion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsecciondocumental", fetch = FetchType.EAGER)
    private List<Categoriadocumental> categoriadocumentalList;

    public Secciondocumental() {
    }

    public Secciondocumental(Integer idsecciondocumental) {
        this.idsecciondocumental = idsecciondocumental;
    }

    public Secciondocumental(Integer idsecciondocumental, String codigo, String procedimiento, String seccion) {
        this.idsecciondocumental = idsecciondocumental;
        this.codigo = codigo;
        this.procedimiento = procedimiento;
        this.seccion = seccion;
    }

    public Integer getIdsecciondocumental() {
        return idsecciondocumental;
    }

    public void setIdsecciondocumental(Integer idsecciondocumental) {
        this.idsecciondocumental = idsecciondocumental;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    @XmlTransient
    public List<Categoriadocumental> getCategoriadocumentalList() {
        return categoriadocumentalList;
    }

    public void setCategoriadocumentalList(List<Categoriadocumental> categoriadocumentalList) {
        this.categoriadocumentalList = categoriadocumentalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsecciondocumental != null ? idsecciondocumental.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Secciondocumental)) {
            return false;
        }
        Secciondocumental other = (Secciondocumental) object;
        if ((this.idsecciondocumental == null && other.idsecciondocumental != null) || (this.idsecciondocumental != null && !this.idsecciondocumental.equals(other.idsecciondocumental))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Secciondocumental[ idsecciondocumental=" + idsecciondocumental + " ]";
    }
    
}
