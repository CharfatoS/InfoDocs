/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipodocumento.findAll", query = "SELECT t FROM Tipodocumento t"),
    @NamedQuery(name = "Tipodocumento.findByIdtipodocumento", query = "SELECT t FROM Tipodocumento t WHERE t.idtipodocumento = :idtipodocumento"),
    @NamedQuery(name = "Tipodocumento.findByTipodoc", query = "SELECT t FROM Tipodocumento t WHERE t.tipodoc = :tipodoc")})
public class Tipodocumento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idtipodocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String tipodoc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipodocumento", fetch = FetchType.EAGER)
    private List<Categoriatipodocumento> categoriatipodocumentoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtipodocumento", fetch = FetchType.EAGER)
    private List<Documento> documentoList;

    public Tipodocumento() {
    }

    public Tipodocumento(Integer idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public Tipodocumento(Integer idtipodocumento, String tipodoc) {
        this.idtipodocumento = idtipodocumento;
        this.tipodoc = tipodoc;
    }

    public Integer getIdtipodocumento() {
        return idtipodocumento;
    }

    public void setIdtipodocumento(Integer idtipodocumento) {
        this.idtipodocumento = idtipodocumento;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    @XmlTransient
    public List<Categoriatipodocumento> getCategoriatipodocumentoList() {
        return categoriatipodocumentoList;
    }

    public void setCategoriatipodocumentoList(List<Categoriatipodocumento> categoriatipodocumentoList) {
        this.categoriatipodocumentoList = categoriatipodocumentoList;
    }

    @XmlTransient
    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipodocumento != null ? idtipodocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipodocumento)) {
            return false;
        }
        Tipodocumento other = (Tipodocumento) object;
        if ((this.idtipodocumento == null && other.idtipodocumento != null) || (this.idtipodocumento != null && !this.idtipodocumento.equals(other.idtipodocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Tipodocumento[ idtipodocumento=" + idtipodocumento + " ]";
    }
    
}
