/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rotulo.findAll", query = "SELECT r FROM Rotulo r"),
    @NamedQuery(name = "Rotulo.findByIdrotulo", query = "SELECT r FROM Rotulo r WHERE r.idrotulo = :idrotulo"),
    @NamedQuery(name = "Rotulo.findByBarras", query = "SELECT r FROM Rotulo r WHERE r.barras = :barras"),
    @NamedQuery(name = "Rotulo.findByCodigo", query = "SELECT r FROM Rotulo r WHERE r.codigo = :codigo"),
    @NamedQuery(name = "Rotulo.findByFecha", query = "SELECT r FROM Rotulo r WHERE r.fecha = :fecha"),
    @NamedQuery(name = "Rotulo.findByRotulo", query = "SELECT r FROM Rotulo r WHERE r.rotulo = :rotulo")})
public class Rotulo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idrotulo;
    @Size(max = 255)
    @Column(length = 255)
    private String barras;
    @Size(max = 255)
    @Column(length = 255)
    private String codigo;
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 255)
    @Column(length = 255)
    private String rotulo;
    @JoinColumn(name = "idsolicitud", referencedColumnName = "idsolicitud", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Solicitud idsolicitud;

    public Rotulo() {
    }

    public Rotulo(Integer idrotulo) {
        this.idrotulo = idrotulo;
    }

    public Integer getIdrotulo() {
        return idrotulo;
    }

    public void setIdrotulo(Integer idrotulo) {
        this.idrotulo = idrotulo;
    }

    public String getBarras() {
        return barras;
    }

    public void setBarras(String barras) {
        this.barras = barras;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getRotulo() {
        return rotulo;
    }

    public void setRotulo(String rotulo) {
        this.rotulo = rotulo;
    }

    public Solicitud getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Solicitud idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrotulo != null ? idrotulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rotulo)) {
            return false;
        }
        Rotulo other = (Rotulo) object;
        if ((this.idrotulo == null && other.idrotulo != null) || (this.idrotulo != null && !this.idrotulo.equals(other.idrotulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Rotulo[ idrotulo=" + idrotulo + " ]";
    }
    
}
