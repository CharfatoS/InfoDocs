/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Solicitud.findAll", query = "SELECT s FROM Solicitud s"),
    @NamedQuery(name = "Solicitud.findByIdsolicitud", query = "SELECT s FROM Solicitud s WHERE s.idsolicitud = :idsolicitud"),
    @NamedQuery(name = "Solicitud.findByConcepto", query = "SELECT s FROM Solicitud s WHERE s.concepto = :concepto"),
    @NamedQuery(name = "Solicitud.findByCreado", query = "SELECT s FROM Solicitud s WHERE s.creado = :creado"),
    @NamedQuery(name = "Solicitud.findByDescripcion", query = "SELECT s FROM Solicitud s WHERE s.descripcion = :descripcion"),
    @NamedQuery(name = "Solicitud.findByUltimaactualizacion", query = "SELECT s FROM Solicitud s WHERE s.ultimaactualizacion = :ultimaactualizacion"),
    @NamedQuery(name = "Solicitud.findByVence", query = "SELECT s FROM Solicitud s WHERE s.vence = :vence")})
public class Solicitud implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer idsolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String concepto;
    @Temporal(TemporalType.DATE)
    private Date creado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(nullable = false, length = 2147483647)
    private String descripcion;
    @Temporal(TemporalType.DATE)
    private Date ultimaactualizacion;
    @Temporal(TemporalType.DATE)
    private Date vence;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitud", fetch = FetchType.EAGER)
    private List<Asignado> asignadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitud", fetch = FetchType.EAGER)
    private List<Rotulo> rotuloList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idsolicitud", fetch = FetchType.EAGER)
    private List<Archivo> archivoList;
    @JoinColumn(name = "idejetematico", referencedColumnName = "idejetematico", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ejetematico idejetematico;
    @JoinColumn(name = "idestadorequerimiento", referencedColumnName = "idestadorequerimiento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Estadorequerimiento idestadorequerimiento;
    @JoinColumn(name = "idtiporequerimiento", referencedColumnName = "idtiporequerimiento", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tiporequerimiento idtiporequerimiento;
    @JoinColumn(name = "idtiposolicutd", referencedColumnName = "idtiposolicitud", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tiposolicitud idtiposolicutd;
    @JoinColumn(name = "usuariodocid", referencedColumnName = "docid", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario usuariodocid;

    public Solicitud() {
    }

    public Solicitud(Integer idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public Solicitud(Integer idsolicitud, String concepto, String descripcion) {
        this.idsolicitud = idsolicitud;
        this.concepto = concepto;
        this.descripcion = descripcion;
    }

    public Integer getIdsolicitud() {
        return idsolicitud;
    }

    public void setIdsolicitud(Integer idsolicitud) {
        this.idsolicitud = idsolicitud;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getUltimaactualizacion() {
        return ultimaactualizacion;
    }

    public void setUltimaactualizacion(Date ultimaactualizacion) {
        this.ultimaactualizacion = ultimaactualizacion;
    }

    public Date getVence() {
        return vence;
    }

    public void setVence(Date vence) {
        this.vence = vence;
    }

    @XmlTransient
    public List<Asignado> getAsignadoList() {
        return asignadoList;
    }

    public void setAsignadoList(List<Asignado> asignadoList) {
        this.asignadoList = asignadoList;
    }

    @XmlTransient
    public List<Rotulo> getRotuloList() {
        return rotuloList;
    }

    public void setRotuloList(List<Rotulo> rotuloList) {
        this.rotuloList = rotuloList;
    }

    @XmlTransient
    public List<Archivo> getArchivoList() {
        return archivoList;
    }

    public void setArchivoList(List<Archivo> archivoList) {
        this.archivoList = archivoList;
    }

    public Ejetematico getIdejetematico() {
        return idejetematico;
    }

    public void setIdejetematico(Ejetematico idejetematico) {
        this.idejetematico = idejetematico;
    }

    public Estadorequerimiento getIdestadorequerimiento() {
        return idestadorequerimiento;
    }

    public void setIdestadorequerimiento(Estadorequerimiento idestadorequerimiento) {
        this.idestadorequerimiento = idestadorequerimiento;
    }

    public Tiporequerimiento getIdtiporequerimiento() {
        return idtiporequerimiento;
    }

    public void setIdtiporequerimiento(Tiporequerimiento idtiporequerimiento) {
        this.idtiporequerimiento = idtiporequerimiento;
    }

    public Tiposolicitud getIdtiposolicutd() {
        return idtiposolicutd;
    }

    public void setIdtiposolicutd(Tiposolicitud idtiposolicutd) {
        this.idtiposolicutd = idtiposolicutd;
    }

    public Usuario getUsuariodocid() {
        return usuariodocid;
    }

    public void setUsuariodocid(Usuario usuariodocid) {
        this.usuariodocid = usuariodocid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsolicitud != null ? idsolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitud)) {
            return false;
        }
        Solicitud other = (Solicitud) object;
        if ((this.idsolicitud == null && other.idsolicitud != null) || (this.idsolicitud != null && !this.idsolicitud.equals(other.idsolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Solicitud[ idsolicitud=" + idsolicitud + " ]";
    }
    
}
