/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findAll", query = "SELECT d FROM Direccion d"),
    @NamedQuery(name = "Direccion.findByIddireccion", query = "SELECT d FROM Direccion d WHERE d.iddireccion = :iddireccion"),
    @NamedQuery(name = "Direccion.findByBarrio", query = "SELECT d FROM Direccion d WHERE d.barrio = :barrio"),
    @NamedQuery(name = "Direccion.findByCel", query = "SELECT d FROM Direccion d WHERE d.cel = :cel"),
    @NamedQuery(name = "Direccion.findByDireccion", query = "SELECT d FROM Direccion d WHERE d.direccion = :direccion"),
    @NamedQuery(name = "Direccion.findByTelcasa", query = "SELECT d FROM Direccion d WHERE d.telcasa = :telcasa"),
    @NamedQuery(name = "Direccion.findByTeloficina", query = "SELECT d FROM Direccion d WHERE d.teloficina = :teloficina"),
    @NamedQuery(name = "Direccion.findByWeb", query = "SELECT d FROM Direccion d WHERE d.web = :web")})
public class Direccion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer iddireccion;
    @Size(max = 255)
    @Column(length = 255)
    private String barrio;
    @Size(max = 255)
    @Column(length = 255)
    private String cel;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String direccion;
    @Size(max = 255)
    @Column(length = 255)
    private String telcasa;
    @Size(max = 255)
    @Column(length = 255)
    private String teloficina;
    @Size(max = 255)
    @Column(length = 255)
    private String web;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddireccion", fetch = FetchType.EAGER)
    private List<Institucion> institucionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddireccion", fetch = FetchType.EAGER)
    private List<Direccionusuario> direccionusuarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iddireccion", fetch = FetchType.EAGER)
    private List<Empjuridico> empjuridicoList;
    @JoinColumn(name = "idciudad", referencedColumnName = "idciudad", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Ciudad idciudad;
    @JoinColumn(name = "idlugar", referencedColumnName = "idlugar", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Lugar idlugar;

    public Direccion() {
    }

    public Direccion(Integer iddireccion) {
        this.iddireccion = iddireccion;
    }

    public Direccion(Integer iddireccion, String direccion) {
        this.iddireccion = iddireccion;
        this.direccion = direccion;
    }

    public Integer getIddireccion() {
        return iddireccion;
    }

    public void setIddireccion(Integer iddireccion) {
        this.iddireccion = iddireccion;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelcasa() {
        return telcasa;
    }

    public void setTelcasa(String telcasa) {
        this.telcasa = telcasa;
    }

    public String getTeloficina() {
        return teloficina;
    }

    public void setTeloficina(String teloficina) {
        this.teloficina = teloficina;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @XmlTransient
    public List<Institucion> getInstitucionList() {
        return institucionList;
    }

    public void setInstitucionList(List<Institucion> institucionList) {
        this.institucionList = institucionList;
    }

    @XmlTransient
    public List<Direccionusuario> getDireccionusuarioList() {
        return direccionusuarioList;
    }

    public void setDireccionusuarioList(List<Direccionusuario> direccionusuarioList) {
        this.direccionusuarioList = direccionusuarioList;
    }

    @XmlTransient
    public List<Empjuridico> getEmpjuridicoList() {
        return empjuridicoList;
    }

    public void setEmpjuridicoList(List<Empjuridico> empjuridicoList) {
        this.empjuridicoList = empjuridicoList;
    }

    public Ciudad getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Ciudad idciudad) {
        this.idciudad = idciudad;
    }

    public Lugar getIdlugar() {
        return idlugar;
    }

    public void setIdlugar(Lugar idlugar) {
        this.idlugar = idlugar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddireccion != null ? iddireccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.iddireccion == null && other.iddireccion != null) || (this.iddireccion != null && !this.iddireccion.equals(other.iddireccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Direccion[ iddireccion=" + iddireccion + " ]";
    }
    
}
