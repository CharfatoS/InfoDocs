/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cfaja
 */
@Entity
@Table(catalog = "vur", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByDocid", query = "SELECT u FROM Usuario u WHERE u.docid = :docid"),
    @NamedQuery(name = "Usuario.findByApellido", query = "SELECT u FROM Usuario u WHERE u.apellido = :apellido"),
    @NamedQuery(name = "Usuario.findByFechanacimiento", query = "SELECT u FROM Usuario u WHERE u.fechanacimiento = :fechanacimiento"),
    @NamedQuery(name = "Usuario.findByNombre", query = "SELECT u FROM Usuario u WHERE u.nombre = :nombre"),
    @NamedQuery(name = "Usuario.findByOtroapellido", query = "SELECT u FROM Usuario u WHERE u.otroapellido = :otroapellido"),
    @NamedQuery(name = "Usuario.findByOtronombre", query = "SELECT u FROM Usuario u WHERE u.otronombre = :otronombre")})
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Long docid;
    @Size(max = 255)
    @Column(length = 255)
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(nullable = false, length = 255)
    private String nombre;
    @Size(max = 255)
    @Column(length = 255)
    private String otroapellido;
    @Size(max = 255)
    @Column(length = 255)
    private String otronombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idusuario", fetch = FetchType.EAGER)
    private List<Direccionusuario> direccionusuarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "docidusuario", fetch = FetchType.EAGER)
    private List<Empleado> empleadoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idusuario", fetch = FetchType.EAGER)
    private List<Users> usersList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuariodocid", fetch = FetchType.EAGER)
    private List<Solicitud> solicitudList;
    @JoinColumn(name = "idempjuridico", referencedColumnName = "nit", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Empjuridico idempjuridico;
    @JoinColumn(name = "tipodoc", referencedColumnName = "idtipodoc", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Tipodoc tipodoc;

    public Usuario() {
    }

    public Usuario(Long docid) {
        this.docid = docid;
    }

    public Usuario(Long docid, Date fechanacimiento, String nombre) {
        this.docid = docid;
        this.fechanacimiento = fechanacimiento;
        this.nombre = nombre;
    }

    public Long getDocid() {
        return docid;
    }

    public void setDocid(Long docid) {
        this.docid = docid;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOtroapellido() {
        return otroapellido;
    }

    public void setOtroapellido(String otroapellido) {
        this.otroapellido = otroapellido;
    }

    public String getOtronombre() {
        return otronombre;
    }

    public void setOtronombre(String otronombre) {
        this.otronombre = otronombre;
    }

    @XmlTransient
    public List<Direccionusuario> getDireccionusuarioList() {
        return direccionusuarioList;
    }

    public void setDireccionusuarioList(List<Direccionusuario> direccionusuarioList) {
        this.direccionusuarioList = direccionusuarioList;
    }

    @XmlTransient
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @XmlTransient
    public List<Users> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<Users> usersList) {
        this.usersList = usersList;
    }

    @XmlTransient
    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    public Empjuridico getIdempjuridico() {
        return idempjuridico;
    }

    public void setIdempjuridico(Empjuridico idempjuridico) {
        this.idempjuridico = idempjuridico;
    }

    public Tipodoc getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(Tipodoc tipodoc) {
        this.tipodoc = tipodoc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (docid != null ? docid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.docid == null && other.docid != null) || (this.docid != null && !this.docid.equals(other.docid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modelo.Usuario[ docid=" + docid + " ]";
    }
    
}
