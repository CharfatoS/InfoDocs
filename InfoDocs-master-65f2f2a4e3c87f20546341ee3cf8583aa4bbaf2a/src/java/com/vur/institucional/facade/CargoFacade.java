package com.vur.institucional.facade;

import Modelo.Cargo;
import Modelo.Dependencia;
import Modelo.Empjuridico;
import Modelo.Oficina;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class CargoFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Cargo entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Cargo entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Cargo entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Cargo find(Object id) {
        return getEntityManager().find(Cargo.class, id);
    }

    public List<Cargo> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Cargo.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Cargo> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Cargo.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Cargo> rt = cq.from(Cargo.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Cargo> verCargosDeDependencia(Dependencia s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Cargo> consulta = cb.createQuery(Cargo.class);
            Root<Cargo> userW = consulta.from(Cargo.class);
            consulta.select(userW)
                    .where(cb.equal(userW.get("idDependencia"), s)
                    );
            Query q = getEntityManager().createQuery(consulta);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
