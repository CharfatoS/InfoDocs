
package com.vur.institucional.facade;

import Modelo.Dependencia;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class DependenciaFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Dependencia entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Dependencia entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Dependencia entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Dependencia find(Object id) {
        return getEntityManager().find(Dependencia.class, id);
    }

    public List<Dependencia> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Dependencia.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Dependencia> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Dependencia.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Dependencia> rt = cq.from(Dependencia.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}