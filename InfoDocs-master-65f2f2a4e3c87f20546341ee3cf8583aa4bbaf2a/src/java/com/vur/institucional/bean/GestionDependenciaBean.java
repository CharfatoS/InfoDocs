/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vur.institucional.bean;

import com.vur.controller.util.JsfUtil;
import Modelo.Dependencia;
import com.vur.institucional.facade.DependenciaFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author jesusmarin
 */
@ManagedBean(name = "gestionDependenciaBean")
@SessionScoped
public class GestionDependenciaBean implements Serializable {

    private Dependencia dependencia;
    private int elDiv;
    private List<Dependencia> dependencialist;

    @EJB
    private DependenciaFacade dependenciaEjb;
    @ManagedProperty(value = "#{gestionInstitucionBean}")
    private GestionInstitucionBean gestionInstitucionBean;

    public GestionDependenciaBean() {

    }

    public String irDependencia() {
        boolean v = verificarDependencia();
        if (v) {
            elDiv = 1;
        } else {
            elDiv = 2;
        }

        return "gestionDependencia";
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    private DependenciaFacade getDependenciaEjb() {
        return dependenciaEjb;
    }

    public boolean verificarDependencia() {
        dependencia = getSeleccion();
        dependencialist = getSeleccionLista();
        if (dependencialist.isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }

    public Dependencia getSeleccion() {
        dependencialist = verficarDependenciaBD();
        if (dependencialist.isEmpty() == false){
            return dependencia;
        } else {
            return dependencia = new Dependencia();
        }        
    }

    public List<Dependencia> getSeleccionLista() {
        dependencialist = verficarDependenciaBD();
        if (dependencialist.isEmpty() == true) {
            dependencia = new Dependencia();
        }
        return dependencialist;
    }

    private List<Dependencia> verficarDependenciaBD() {
        try {
            return getDependenciaEjb().findAll();
        } catch (Exception e) {
            return null;
        }
    }
    
    public String crearActualizarDependencia(){
        elDiv = 1;
        if(dependencia.getIddependencia()< 0){
            createDependencia();
            dependencia = getSeleccion();
        } else if (dependencia.getIddependencia()> 0){
            updateDependencia();
            dependencia = getSeleccion();
        }
        return "gestionDependencia";
    }
    
    private void createDependencia() {
        try {
            dependencia.setIdinstitucion(gestionInstitucionBean.getSeleccion());
            dependenciaEjb.create(dependencia);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha creado la institución, error: "+e);
        }
    }

    private void updateDependencia() {
        try {
            dependenciaEjb.edit(dependencia);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("No se ha actualizado la institución, error: "+e);
        }
    }
    
    public String cargarVistaNuevo(){
        elDiv = 3;
        dependencia = new Dependencia();
        return "gestionDependencia";
    }
    
    public String cargarVistaEditar(){
        elDiv = 3;
        return "gestionDependencia";
    }
    
    public String visualizarDependencia(Dependencia item){
        elDiv = 2;
        //JsfUtil.addSuccessMessage(""+item.getNombre());
        dependencia = item;
        return "gestionDependencia";
    }

    public GestionInstitucionBean getGestionInstitucionBean() {
        return gestionInstitucionBean;
    }

    public void setGestionInstitucionBean(GestionInstitucionBean gestionInstitucionBean) {
        this.gestionInstitucionBean = gestionInstitucionBean;
    }
    
}
