package com.vur.mail.chat;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jesus
 */
public class Sala implements Serializable {

    private String sala;
    private List<Mensaje> mensajes;
    private List<UsChat> chates;

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public List<Mensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<Mensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public List<UsChat> getChates() {
        return chates;
    }

    public void setChates(List<UsChat> chates) {
        this.chates = chates;
    }

    
    public class Mensaje {

        private String mnsj;
        private long hora;
        private String nombre;

        public String getMnsj() {
            return mnsj;
        }

        public void setMnsj(String mnsj) {
            this.mnsj = mnsj;
        }

        public long getHora() {
            return hora;
        }

        public void setHora(long hora) {
            this.hora = hora;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
    }
}
