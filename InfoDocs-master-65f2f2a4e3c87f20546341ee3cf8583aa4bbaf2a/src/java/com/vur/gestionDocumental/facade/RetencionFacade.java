
package com.vur.gestionDocumental.facade;

import Modelo.Retencion;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class RetencionFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Retencion entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Retencion entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Retencion entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Retencion find(Object id) {
        return getEntityManager().find(Retencion.class, id);
    }

    public List<Retencion> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Retencion.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Retencion> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Retencion.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Retencion> rt = cq.from(Retencion.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
