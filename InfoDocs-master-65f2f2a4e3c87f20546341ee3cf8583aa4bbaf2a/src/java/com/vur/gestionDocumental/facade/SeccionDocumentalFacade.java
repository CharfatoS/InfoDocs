
package com.vur.gestionDocumental.facade;

import Modelo.Secciondocumental;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class SeccionDocumentalFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Secciondocumental entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Secciondocumental entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Secciondocumental entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Secciondocumental find(Object id) {
        return getEntityManager().find(Secciondocumental.class, id);
    }

    public List<Secciondocumental> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Secciondocumental.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Secciondocumental> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Secciondocumental.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Secciondocumental> rt = cq.from(Secciondocumental.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}