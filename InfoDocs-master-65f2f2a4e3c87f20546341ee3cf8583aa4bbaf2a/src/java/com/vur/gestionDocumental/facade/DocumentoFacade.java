
package com.vur.gestionDocumental.facade;

import Modelo.Documento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class DocumentoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Documento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Documento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Documento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Documento find(Object id) {
        return getEntityManager().find(Documento.class, id);
    }

    public List<Documento> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Documento.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Documento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Documento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Documento> rt = cq.from(Documento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
