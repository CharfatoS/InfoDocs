
package com.vur.gestionDocumental.facade;

import Modelo.Tipodocumento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class TipoDocumentoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Tipodocumento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Tipodocumento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Tipodocumento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Tipodocumento find(Object id) {
        return getEntityManager().find(Tipodocumento.class, id);
    }

    public List<Tipodocumento> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tipodocumento.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Tipodocumento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tipodocumento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Tipodocumento> rt = cq.from(Tipodocumento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
