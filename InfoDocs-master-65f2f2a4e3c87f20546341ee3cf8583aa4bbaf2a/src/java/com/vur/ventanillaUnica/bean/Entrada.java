
package com.vur.ventanillaUnica.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dsilva
 */
//@Entity
public class Entrada implements Serializable {
    //private static final long serialVersionUID = 1L;
    //@Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //@Column(columnDefinition="text")
    private String descripcion;
    //@ElementCollection
    private List<Adjunto> adjuntos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public void addAdjunto(Adjunto adjunto) {
        getAdjuntos().add(adjunto);
    }

    public List<Adjunto> getAdjuntos() {
        if (adjuntos == null) {
            adjuntos = new ArrayList<Adjunto>();
        }
        return adjuntos;
    }

    public void setAdjuntos(List<Adjunto> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entrada)) {
            return false;
        }
        Entrada other = (Entrada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + id + "";
    }

    public void quitarAdjunto(Adjunto adjunto) {
        getAdjuntos().remove(adjunto);
    }
}
