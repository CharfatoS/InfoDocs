package com.vur.ventanillaUnica.bean;

import com.vur.controller.util.JsfUtil;
import com.vur.institucional.controller.EmpleadoController;
import Modelo.Dependencia;
import Modelo.Empleado;
import com.vur.mail.GestorJavaMail;
import com.vur.usuario.bean.LoginBean;
import com.vur.usuario.bean.GestionUsuarioBean;
import com.vur.usuario.controller.UsuarioController;
import Modelo.Users;
import Modelo.Usuario;
import com.vur.util.VurUtil;
import Modelo.Archivo;
import Modelo.Asignado;
import Modelo.Estadorequerimiento;
import Modelo.Solicitud;
import com.vur.ventanillaUnica.facade.AsignadoFacade;
import com.vur.ventanillaUnica.facade.SolicitudFacade;
import Modelo.Respuesta;
import Modelo.Rol;
import Modelo.Tiposolicitud;
import com.vur.ventanillaUnica.facade.ArchivoFacade;
import com.vur.ventanillaUnica.facade.EjeTematicoFacade;
import com.vur.ventanillaUnica.facade.EstadoRequerimientoFacade;
import com.vur.ventanillaUnica.facade.TipoSolicitudFacade;
import com.vur.ventanillaUnica.faq.facade.RespuestaFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

/**
 *
 * tiempo Humano 1 hora 3600 segundos 1 dia	86400 segundos 1 semana	604800
 * segundos 1 mes (30.44 days) 2629743 seconds 1 año (365.24 days) 31556926
 * seconds
 *
 * @since 25-06-2015
 * @version 0.0.1
 * @author jesusmarin
 */
@ManagedBean(name = "solicitudBean")
@SessionScoped
public class SolicitudBean implements Serializable {

    private Usuario usuario;
    private Solicitud solicitud;
    private Asignado asignado;
    private Respuesta respuesta;
    private Dependencia dependencia;
    private int elDiv = 589;//2  formulario de usuario nuevo/ 1 formulario de nuevo requerimiento/3 ver la solicitud creada
    private long docid;
    private String idRadicado;
    private String mes;
    private String fechaReferencia;
    private Estadorequerimiento estado;

    @EJB
    private SolicitudFacade solicitudEjb;

    @EJB
    private ArchivoFacade archivoEjb;
    @EJB
    private AsignadoFacade asignadoEjb;
    @EJB
    private TipoSolicitudFacade TipoSolicitudEjb;
    @EJB
    private EstadoRequerimientoFacade estadoRequerimientoEjb;
    @EJB
    private RespuestaFacade respuestaEjb;
    @EJB
    private EjeTematicoFacade ejeTematicoEjb;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{usuarioController}")
    private UsuarioController usuarioController;
    @ManagedProperty(value = "#{gestionUsuarioBean}")
    private GestionUsuarioBean gestionUsuarioBean;
    @ManagedProperty(value = "#{subirArchivosBean}")
    private SubirArchivosBean subirArchivosBean;
    @ManagedProperty(value = "#{empleadoController}")
    private EmpleadoController empleadoController;

    public SolicitudBean() {
    }

    public String verRequerimientosCiuidadano() {
        elDiv = 0;
        return "listaRequerimientos";
    }

    public String verSolicitud(Solicitud item) {
        solicitud = item;
        respuesta = verRespuestaDeSolicitud(solicitud);
        elDiv = 1;
        return "listaRequerimientos";
    }

    public String verSolicitudEnBuscar(Solicitud item) {
        elDiv = 1;
        solicitud = item;
        try {
            respuesta = verRespuestaDeSolicitud(solicitud);
            JsfUtil.addSuccessMessage(" respuesta: " + respuesta.getRespuesta());
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("La solicitud  aun no tiene respuesta");
        }
        return "buscarSolicitud";
    }

    /*
     @return String
     */
    public String ingresarNuevoCiudadano() {
        Rol r = new Rol();
        r.setRol("CIUDADANO");
        elDiv = 2;
        //iniciar datos de nuevo usuario
        getGestionUsuarioBean().establecerComoCiudadano();
        getGestionUsuarioBean().getCurrentUsuario().setDocid(docid);
        getGestionUsuarioBean().getCurrentUsers().setPassword(docid);
        //getGestionUsuarioBean().getCurrentUsuario().setGrupo("CIUDADANO");
        getGestionUsuarioBean().getCurrentUsers().setNumaccesos(0);
        getGestionUsuarioBean().getCurrentUsers().setMail("a@mail.com");
        getGestionUsuarioBean().getCurrentUsers().setRol(r);
        getGestionUsuarioBean().getCurrentDireccion().setWeb("www.mipagina.com");
        usuario = getGestionUsuarioBean().getCurrentUsuario();
        JsfUtil.addSuccessMessage("Ingresar datos del nuevo ciudadano al sistema :)");
        return "nuevoRequerimiento";
    }

    public String buscarSolicitudDesdeFuncionario() {
        elDiv = 0;
        List<Solicitud> l = getSolicitudesBusqueda();
        return "buscarSolicitud";
    }

    /*
     @return String
     */
    public String verSolicitudesEnFuncionario() {
        // empleado = u.getIdEmpleado();  
        elDiv = 0;// muesta la lsita de solicitud del funcionario 
        Usuario u = getLoginBean().getUsuario();
        List<Solicitud> listaSolicitudes = getSolicitudesFuncionario();
        //JsfUtil.addSuccessMessage("Solcitudes asignadas a :" + u.getNombreCompleto());
        return "verSolicitudes";
    }

    /*PAra administrador
     @return String
     */
    public String verSolicitudesFuncionarioEnAdmin() {
        // empleado = u.getIdEmpleado();  
        elDiv = 2;// muesta la lsita de solicitud del funcionario 
        Usuario u = getLoginBean().getUsuario();
        try {
            List<Solicitud> listaSolicitudes = getSolicitudesFuncionario();
        } catch (Exception e) {
            //
        }

        JsfUtil.addSuccessMessage("elDiv :" + 2);
        return "gestionRequerimientos";
    }

    public String verSolicitudesFuncionarioMobil() {
        verSolicitudesFuncionarioEnAdmin();
        return "mb_responder";
    }

    /*
     @return String
     */
    public String verSolicitudesDependencia() {
        Rol rol = getLoginBean().getUsers().getRol();
        elDiv = 3;
        //elDiv = 0;
        if (rol.equals("SUPER")) {
            Empleado e = getLoginBean().getEmpleado();
            try {
                System.out.println(" es un super " + dependencia.toString());
            } catch (Exception ex) {
            }

        } else {
            List<Solicitud> l = getSolicitudesDependencia();
        }
        JsfUtil.addSuccessMessage("elDiv :" + elDiv);
        return "requerimientosDependencia";
    }

    public String verSolicitudesDependenciaEnSuper() {
        elDiv = 3;
        List<Solicitud> l = getSolicitudesDependenciaEnSuper();
        return "requerimientosDependencia";
    }

    /*
     @return String
     */
    public String verMisSolicitudesEnDependencia() {
//        elDiv = 0;
        elDiv = 6;
        List<Solicitud> l = getSolicitudesCiudadano();
        JsfUtil.addSuccessMessage("elDiv :" + elDiv);
        return "gestionRequerimientos";
    }

    /*
     @return String
     */
    public List<Solicitud> getSolicitudesDependenciaEnSuper() {
        try {
            //System.out.println("getSolicitudesDependenciaEnSuper " + dependencia.toString());
            return consultarSolicitudesDependencia(dependencia);
        } catch (Exception e) {
            System.out.println("getSolicitudesDependenciaEnSuper Dependencia catch");
        }
        return null;
    }

    /*
     @return String
     */
    public List<Solicitud> getSolicitudesDependencia() {
        Dependencia d = new Dependencia();

        d = getLoginBean().getEmpleado().getIdCargo().getIdDependencia();
        try {
            return consultarSolicitudesDependencia(d);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("Dependencia catch");
        }
        return null;
    }

    public List<Solicitud> getListaDependencia() {
        Dependencia d = new Dependencia();
        try {
            d = getLoginBean().getUsuario().getIdEmpleado().getIdCargo().getIdDependencia();
            return consultarSolicitudesDependencia(d);
        } catch (Exception e) {
        }
        return null;
    }

    /*
     @param Dependencia
     @return List
     */
    public List<Solicitud> consultarSolicitudesDependencia(Dependencia d) {
        try {
            return getSolicitudEjb().getSolicitudesDeDependencia(d);
        } catch (Exception e) {
        }
        return null;
    }

    /*
     @return List
     */
    public List<Solicitud> getSolicitudesFuncionario() {
        try {
            Usuario u = getLoginBean().getUsuario();
            return getSolicitudEjb().getlistaAsignadasFuncionario(u, "Asignado");//listaSolicitudes;
        } catch (Exception ex) {
            JsfUtil.addSuccessMessage("Ninguna solicitud Asignada a la fecha");
            return null;
        }
    }

    /*
     @return List
     */
    public List<Solicitud> getAllSolicitudesMesSegunEstado() {
        VurUtil v = new VurUtil();
        try {
            int mint = v.verIntMes(mes);
            long iml = v.getFechaInicioMes(mint);
            long fml = v.getFechaFinMes(mint);
            Usuario u = getLoginBean().getUsuario();
            return getSolicitudEjb().getListaSolicitudSegunEstado(estado, iml, fml, fechaReferencia);//listaSolicitudes;
        } catch (Exception ex) {
            JsfUtil.addSuccessMessage("Ninguna solicitud Asignada a la fecha");
            return null;
        }
    }

    /*
     @return List
     */
    public int getCountSolicitudesMesSegunEstado() {
        VurUtil v = new VurUtil();
        try {
            int mint = v.verIntMes(mes);
            long iml = v.getFechaInicioMes(mint);
            long fml = v.getFechaFinMes(mint);
            Usuario u = getLoginBean().getUsuario();
            return getSolicitudEjb().countSolicitudesSegunEstadoEnFechas(estado, iml, fml, fechaReferencia);// etListaSolicitudSegunEstado(estado, iml, fml);//listaSolicitudes;
        } catch (Exception ex) {
            JsfUtil.addSuccessMessage("Ninguna solicitud Asignada a la fecha");
            return 0;
        }
    }

    /*
     @param Solicitud
     @return String
     */
    public String visualizarSolicitudEnDependencia(Solicitud item) {
        elDiv = 7;
        solicitud = item;
        dependencia = verDependenciaDeSolicitud();
        getEmpleadoController().setDependencia(dependencia);
        try {
            //Asignado a = new Asignado();
            JsfUtil.addSuccessMessage("Asignado a :" + solicitud.getIdAsignado().toString());
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("Solicitud sin Asignar");
        }
        return "gestionRequerimientos";
    }

    private Dependencia verDependenciaDeSolicitud() {
        try {
            return solicitud.getIdejetematico().getIdservicio().getIddependencia();
        } catch (Exception e) {
            return null;
        }

    }

    public String asignarSolicitud() {
        boolean tiene = false;//false no tiene. true tiene
        tiene = verifiqueAsignadoDeSolicitud();

        JsfUtil.addSuccessMessage("***->" + tiene);
        if (tiene == false) {
            Asignado a = new Asignado();
            a.setIdsolicitud(solicitud);
            a.setHabilitado(true);
            Date d = new Date();
            //a.setFechaasignado(System.currentTimeMillis() / 1000);
            a.setFechaasignado(d);
            String es = verEstadoRequerimiento(solicitud);//solicitud.getIdEstadoRequerimiento().getEstado();
            a.setEstado(es);
            asignado = a;
            JsfUtil.addSuccessMessage("asitnado < " + a.toString());
            createUpdateAsignado();
            JsfUtil.addSuccessMessage("asitnado > " + a.toString());

        } else {
            asignado = buscarAsignado();
            asignado.setIdsolicitud(solicitud);
            JsfUtil.addSuccessMessage("Asignado a :" + asignado.getIdasignado().toString());
        }
        elDiv = 2;//muestra la asignacio de la solicitud
        return "requerimientosDependencia";
    }

    private String verEstadoRequerimiento(Solicitud s) {
        try {
            return s.getIdestadorequerimiento().getEstado();
        } catch (Exception e) {
            return "Radicado";
        }
    }

    public String actualizarAsignado() {
        Estadorequerimiento er = getEstadoRequerimientosAsignado(solicitud.getIdestadorequerimiento());
        solicitud.setIdestadorequerimiento(er);
        String s = er.getEstado();
        updateSolicitud();
        asignado.setEstado(s);
        createUpdateAsignado();
        JsfUtil.addSuccessMessage("Asignado en actualizar " + asignado.getEstado() + "/ Solicitud: " + solicitud.getIdestadorequerimiento().getEstado());
        elDiv = 3;
        return "requerimientosDependencia";
    }

    public String verEstadoDeSolicitud(Solicitud item) {
        try {
            return item.getIdestadorequerimiento().getEstado();
        } catch (Exception e) {
            return "?";
        }
    }

    private Estadorequerimiento getEstadoRequerimientosAsignado(Estadorequerimiento es) {
        try {
            System.out.println("ok fin estado requerimento Asignado");
            return getEstadoRequerimientoEjb().findEstado("Asignado");
        } catch (Exception e) {
            System.out.println("fallo estado requerimento Asignado");
            return es;
        }
    }

    public String verEmpleadoAsignado(Solicitud item) {
        String emp = "?";
        try {
            Empleado e = getAsignadoEjb().verAsignadoDeSolicitud(item).getIdEmpleado();
            emp = e.toString();
        } catch (Exception e) {
            emp = "?";
        }
        return emp;
    }

    private boolean verifiqueAsignadoDeSolicitud() {
        try {
            return getAsignadoEjb().verifiqueAsignadoDeSolicitud(solicitud);
        } catch (Exception e) {
            return false;
        }
    }

    private Asignado buscarAsignado() {
        try {
            return getAsignadoEjb().verAsignadoDeSolicitud(solicitud);
        } catch (NullPointerException ex) {
            return null;
        }
    }

    private void createUpdateAsignado() {
        String id = "" + getLoginBean().getUsuario();
        try {
//            id = id;//+ asignado.getLogAsignado();
        } catch (Exception e) {
        }
        //asignado.setLogAsignado(id);
        try {
            if (asignado.getIdasignado() < 0) {
                getAsignadoEjb().create(asignado);
            } else {
                getAsignadoEjb().edit(asignado);
            }
        } catch (Exception e) {
        }
    }

    /*
     @param Solicitud
     @return String
     */
    public String visualizarMiSolicitud(Solicitud item) {
        elDiv = 4;
        solicitud = item;
        asignado = buscarAsignado();
        respuesta = verRespuestaDeSolicitud(item);
        String tu = getLoginBean().getUsers().getRol().getRol();
        if (tu.equals("FUNCIONARIO")) {
            System.out.println("solicitudes en funcionario");
            return "verSolicitudes";
        } else {
            return "gestionRequerimientos";
        }
    }

    /*
     @param Solicitud
     @return String
     */
    public String visualizarSolicitudFuncionario(Solicitud item) {
        elDiv = 4;
        solicitud = item;
        asignado = buscarAsignado();
        String tu = getLoginBean().getUsers().getRol().getRol();
        if (tu.equals("FUNCIONARIO")) {
            System.out.println("solicitudes en funcionario");
            return "gestionRequerimientos";// return "verSolicitudes";
        } else {
            return "gestionRequerimientos";
        }
    }

    public String visualizarSolicitudMobil(Solicitud item) {
        visualizarSolicitudFuncionario(item);
        return "mb_responder";
    }

    private Respuesta iniciarRespuesta() {
        Date v = solicitud.getVence();
        Date i = solicitud.getCreado();
        String x = "" + (v.getTime() - i.getTime());
        respuesta = new Respuesta();
        respuesta.setRespuesta(new Date());
        //respuesta.setIdSolicitud(solicitud);
        respuesta.setIdUsuario(getLoginBean().getUsuario());
        respuesta.setTiemporespuesta(Integer.parseInt(x));
        return respuesta;
    }

    public boolean botonVerResponderSolicitud() {
        Usuario u = getLoginBean().getUsuario();
        Empleado e = getLoginBean().getEmpleado();
        Empleado ea = verEmpleadoDeAsignado();
        try {
            return ea.getIdempleado() != e.getIdempleado();
        } catch (Exception ex) {
            return true;
        }

    }

    private Empleado verEmpleadoDeAsignado() {
        try {
            return getEmpleadoController().getEmpleadoDeAsignado(asignado);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean botonVerRespuestaSolicitud() {
        Usuario u = getLoginBean().getUsuario();
        try {
            if ((solicitud.getUsuariodocid().getDocid() == u.getDocid()) && solicitud.getIdRespuesta() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /*
     @return String
     */
    public String crearRespuestaFuncionario() {
        Usuario u = getLoginBean().getUsuario();
        //verifique
        boolean existe = verSiExisteRespuesta(u, solicitud);
        if (existe) {
            respuesta = verRespuestaDeSolicitud(solicitud);
            elDiv = 8;//vista de respuesta
            JsfUtil.addSuccessMessage("Se encontro respuesta");
        } else {
            iniciarRespuesta();
            elDiv = 5;//formulario de respuuesta
        }
        return "responderSolicitud";
    }

    public String crearRespuestaMobil() {
        crearRespuestaFuncionario();
        return "mb_responder";
    }

    /*
     @return String
     */
    public String habilitarEdicion() {
        elDiv = 5;//formulario de respuuesta
        return "responderSolicitud";
    }

    /*
     @return String
     */
    public String cancelarRespuestaSolicitud() {
        elDiv = 0;//lista de solicitudes en funcioanrio
        getSubirArchivosBean().setEntradaActual(null);
        return "verSolicitudes";
    }

    /*
     @return String
     */
    public String cancelarAnexoArchivoEdicion() {
        elDiv = 4;//8;//formulario de respuuesta
        getSubirArchivosBean().setEntradaActual(null);
        return "gestionRequerimientos";
    }

    public String cancelarAnexoArchivoMobil() {
        cancelarAnexoArchivoEdicion();
        return "mb_responder";
    }

    /*
     @return String
     */
    public String agregarArchivoRespuesta() {
        elDiv = 0;//formulario de respuuesta
        return "responderSolicitud";
    }

    /*
     @return String
     */
    public String commitAnexarArchivo(String s) {
        gestionarArchivosAdjuntosRespuesta();
        elDiv = 8;//formulario de respuuesta
        getSubirArchivosBean().setEntradaActual(null);
        return s;
    }

    private boolean verSiExisteRespuesta(Usuario u, Solicitud s) {
        try {
            return getRespuestaEjb().verSiExisteRespuesta(u, s);
        } catch (Exception e) {
            return false;
        }
    }

    private Respuesta verRespuesta(Usuario u, Solicitud s) {
        try {
            return getRespuestaEjb().verRespuesta(u, s);
        } catch (Exception e) {
            return null;
        }
    }

    private Respuesta verRespuestaDeSolicitud(Solicitud s) {
        try {
            return getRespuestaEjb().verRespuesta(s);
        } catch (Exception e) {
            respuesta = new Respuesta();
            return null;
        }
    }

    /*
     @return String
     */
    public String commitRespuestaFuncionario() {
        elDiv = 8;//muestra la respuesta creada
        Estadorequerimiento er = getSegunEstado("Resuelto");
        //System.out.println("ER resuelto?" + er.getEstado());
        solicitud.setIdestadorequerimiento(er);
        if (respuesta.getIdrespuesta() < 0) {
            createRespuesta();
            updateSolicitud();
        } else {
            //cambiar el estadoa la solicitud 
            estado = getSegunEstado("Resuelto");//Radicado, Asignado, Resuelto, Entregado Archivado
            solicitud.setIdestadorequerimiento(estado);
            updateRespuesta();
            updateSolicitud();
        }
        return "responderSolicitud";
    }

    public String commitRespuestaMObil() {
        commitRespuestaFuncionario();
        return "mb_responder";
    }

    private void createRespuesta() {
        String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
                //id = id + "/" + respuesta.getSugerencia();
            } catch (Exception e) {
            }
            //respuesta.setSugerencia(id);
            getRespuestaEjb().create(respuesta);
            String x = "Solicitud respondido, " + respuesta.getIdSolicitud().getConcepto();
            enviarCorreo(x);
        } catch (Exception e) {
            //
        }

    }

    private void updateRespuesta() {
        String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
                // id = id + "/" + respuesta.getSugerencia();
            } catch (Exception e) {
            }
            //respuesta.setSugerencia(id);
            getRespuestaEjb().edit(respuesta);
            String x = " Se a agregado datos a la respuesta, / " + respuesta.getIdSolicitud().getConcepto();
            enviarCorreo(x);
        } catch (Exception e) {
            //
        }
    }

    private List<Asignado> getListaAsignado(Usuario u) {
        try {
            //Empleado e = u.getIdEmpleado();
            return getAsignadoEjb().verAsignadoDeUsuario(u);
        } catch (NullPointerException e) {
            return null;
        }
    }

    /*
     @return String
     */
    public String buscarCiudadano() {
        VurUtil vu = new VurUtil();
        docid = vu.arreglarNumeroString(docid);
        //JsfUtil.addSuccessMessage("Documento ingresado " + docid);
        usuario = buscarCiudadanoConDocId(docid);
        if (usuario == null) {
            elDiv = 10;//el ciudadano no existe y se habilita el boton para crear formulario
            JsfUtil.addErrorMessage("El documento  NO existe en el sistema");
        } else {
            elDiv = 11;
            JsfUtil.addSuccessMessage(" Ciudadano: " + usuario.getNombre() + " " + usuario.getApellido());
            JsfUtil.addSuccessMessage(" Documento de Identidad: " + usuario.getDocid());
            JsfUtil.addSuccessMessage(" El Div : " + elDiv);
        }
        return "nuevoRequerimiento";
    }

    private Usuario buscarCiudadanoConDocId(long di) {
        try {
            return getUsuarioController().buscarUsuarioConId(di);
        } catch (Exception e) {
            return null;
        }
    }

    /*guada ciudadano desde funcionario gestion busuario bean desde solicitud bean
     *@param String vista a donde llega
     @return String vista a donde llega
     */
    public String crearUsuarioCiudadano(String s) {
        boolean ok = getGestionUsuarioBean().crearUsuarioCiudadano();
        if (ok) {
            usuario = getGestionUsuarioBean().getCurrentUsuario();
            elDiv = 11;// quda listo para solicitar ingreso de solicitud
            JsfUtil.addSuccessMessage("Los datos del Ciudadano " + usuario.toString() + " se ingresaron correctamente");
        }
        return s;
    }

    /*
     @return String
     */
    public String nuevaSolicitud() {
        elDiv = 0;
        Usuario u = getLoginBean().getUsuario();
        long fecha = verFecha();
        solicitud = new Solicitud();
        solicitud.setDescripcion("");
        solicitud.setConcepto("");
        solicitud.setIdUsuario(u);//se asigna el usuario del login a la solicitud 
        solicitud.setCreado(new Date());
        solicitud.setVence(fechaVence(fecha, 864000));
        usuario = null;
        docid = 0;
        getSubirArchivosBean().setEntradaActual(null);
        return "nuevoRequerimiento";
    }

    public String verRespuestaDeSolicitud() {
        Users u = getLoginBean().getUsers();
        respuesta = verRespuestaDeSolicitud(solicitud);///solicitud.getIdRespuesta();//        
        elDiv = 8;
        if (u.getRol().equals("CIUDADANO")) {
            return "listaRequerimientos";
        } else {
            return "gestionRequerimientos";
        }
    }

    public String verRespuestaMobil() {
        verRespuestaDeSolicitud();
        return "mb_responder";
    }

    public boolean aunNoTieneRespuesta() {
        boolean r = false;
        respuesta = verRespuestaDeSolicitud(solicitud);
        try {
            if (respuesta.getIdrespuesta() > 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception ex) {
            return true;
        }
    }

    public List<Archivo> getListaArchivoDeSolicitud() {
        try {
            return getArchivoEjb().verListaArchivosDeSolicitud(solicitud);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Archivo> getListaArchivoDeRespuesta() {
        try {
            return getArchivoEjb().verListaArchivosDeSolicitud(respuesta);
        } catch (Exception e) {
            return null;
        }
    }

    public String linkDescargarArchivo(Archivo item) {
        String link = " ";
        if (item.getTipoarchivo().equals("application/pdf")) {
            //para pdf 
            link = "../../servletArchivo.pdf";
        } else if (item.getTipoarchivo().equals("application/zip")) {
            //para zip
            link = "../../servletArchivo.zip";
        } else if (item.getTipoarchivo().equals("application/vnd.ms-excel")) {
            //para  xls
            link = "../../servletArchivo.xls";
        } else if (item.getTipoarchivo().equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
            //para  xlsx
            link = "../../servletArchivo.xlsx";
        } else if (item.getTipoarchivo().equals("application/x-rar")) {
            //para  rar
            link = "../../servletArchivo.rar";
        } else if (item.getTipoarchivo().equals("image/jpeg")) {
            //para  jpg
            link = "../../servletArchivo.jpg";
        } else if (item.getTipoarchivo().equals("image/png")) {
            //para  png
            link = "../../servletArchivo.png";
        } else if (item.getTipoarchivo().equals("application/msword")) {
            //para  doc
            link = "../../servletArchivo.doc";
        } else if (item.getTipoarchivo().equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
            //para  docx
            link = "../../servletArchivo.docx";
        } else {
            link = "../../servletArchivo.pdf";
        }
        link = link + "?id=" + item.getIdarchivo();
        return link;
    }

    /*
     @return String
     */
    public String ingresarNuevaSolicitud() {
        //nuevaSolicitud();
        elDiv = 1;//formulario para ingreso de solicitud
        solicitud.setIdUsuario(usuario);//para formulario de ventanilla uica en funcionario
        solicitud.setCreado(new Date());
        JsfUtil.addSuccessMessage("Fecha creado " + solicitud.getCreado().getDate() + "/"
                + solicitud.getCreado().getMonth() + "/" + solicitud.getCreado().getYear());
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "nuevoRequerimiento";
    }

    public SelectItem[] getItemsEjeTematicoDedependencia() {
        return JsfUtil.getSelectItems(getEjeTematicoEjb().verEjeTematicoDeDependencia(dependencia), true);
    }

    private long verFecha() {
        return System.currentTimeMillis() / 1000;
    }

    private Date fechaVence(long s, int d) {
        Date da = new Date();
        da.setTime(s + d);
        return da;
    }

    /*
     @param Solicitud
     @return Date
     */
    public Date getFechaCreado(Solicitud item) {
        try {
            //return new Date(item.getCreado() * 1000);
            return new Date(item.getCreado().getTime() * 1000);
        } catch (Exception e) {
            return new Date();
        }
    }

    /*
     @param Solicitud
     @return Date
     */
    public Date getFechaVence(Solicitud item) {
        try {
//            return new Date(item.getVence() * 1000);
            return new Date(item.getVence().getTime() * 1000);
        } catch (Exception e) {
            return new Date();
        }
    }

    /*
     @param Long
     @return String
     */
    public String getTiempo(Long l) {
        return getLoginBean().getDias(l);
    }

    /*
     @param long
     @return Date
     */
    public Date getFecha(long item) {
        return new Date(item * 1000);
    }

    public int verCantidadArchivosAdjuntos(Solicitud item) {
        return getCantidadAdjuntos(item);
    }

    private int getCantidadAdjuntos(Solicitud item) {
        try {
            return getArchivoEjb().getCantidadAdjuntosDeSolicitud(item);
        } catch (Exception e) {
            return 0;
        }
    }

    /*verifica si id es mayor a 0 y actualiza y si es menor a 0 crea una nueva solicitud
     * @return the String nombre de la pagina
     */
    public String actualizarCrearSolicitud(String pg) {
        if (solicitud.getIdsolicitud() > 0) {
            updateSolicitud();
            //gestionar arcivos adjuntos
            gestionarArchivosAdjuntos();
        } else if (solicitud.getIdsolicitud() < 0) {
            int g = 10;
            try {
                g = solicitud.getIdejetematico().getTiemporespuesta();
                System.out.println("g tiempo respúesta " + g);
            } catch (Exception e) {
                //
            }
            g = g * 86400;
            //solicitud.setVence(fechaVence(solicitud.getCreado(), g));
            solicitud.setVence(fechaVence(solicitud.getCreado().getTime(), g));
            solicitud.setIdestadorequerimiento(getSegunEstado("Radicado"));
            solicitud.setIdtiposolicutd(getSegunTipo("Externa"));
            createSolicitud();
            List<Archivo> l = gestionarArchivosAdjuntos();
            solicitud.setArchivoList(l);
            getSubirArchivosBean().setEntradaActual(null);

        }
        if (pg.equals("gestionRequerimientos")) {
            elDiv = 6;//solicitudes creadas por empleado
        }
        JsfUtil.addSuccessMessage("El div " + elDiv);
        return pg;
    }

    private List<Archivo> gestionarArchivosAdjuntos() {
        List<Archivo> l = new ArrayList<Archivo>();
        Entrada entrada = getSubirArchivosBean().getEntradaActual();
        List<Adjunto> lista = entrada.getAdjuntos();
        for (Adjunto ad : lista) {
            System.out.println("getIdSolicitud en archivo " + solicitud.getIdsolicitud());
            Archivo a = new Archivo();
            a.setIdarchivo(-1);//
            a.setArchivo(ad.getArchivo());//bit[]
            a.setTipoarchivo(ad.getTipoContenido());//tipo de archivo
            a.setAdjunto(ad.getNombreAdjunto());// nombre del archivo
            a.setIdsolicitud(solicitud);
            a.setIddisposicionfinal(null);
            a.setIdrespuesta(null);
            createArchivo(a);
            l.add(a);
        }
        return l;
    }

    private List<Archivo> gestionarArchivosAdjuntosRespuesta() {
        List<Archivo> l = new ArrayList<Archivo>();
        Entrada entrada = getSubirArchivosBean().getEntradaActual();
        List<Adjunto> lista = entrada.getAdjuntos();
        for (Adjunto ad : lista) {
            Archivo a = new Archivo();
            a.setIdarchivo(-1);//
            a.setArchivo(ad.getArchivo());//bit[]
            a.setTipoarchivo(ad.getTipoContenido());//tipo de archivo
            a.setAdjunto(ad.getNombreAdjunto());// nombre del archivo
            a.setIdrespuesta(respuesta);
            a.setIddisposicionfinal(null);
            a.setIdsolicitud(null);
            System.out.println("idSolicitud en Archivo objeto " + a.getIdsolicitud());
            createArchivo(a);
            l.add(a);
        }
        return l;
    }

    public int countSolicitudesAnioSegunEstado(String s) {
        VurUtil v = new VurUtil();
        Usuario u = getLoginBean().getUsuario();
        long f = v.getFechaInicioAnioActual();
        //System.out.println("getfechaInicioMes "+f);
        try {
            return getSolicitudEjb().countSolicitudFuncionarioFechaActual(u, s, f);
        } catch (Exception e) {
            return 0;
        }
    }

    public int countSolicitudesMesSegunEstado(String s) {
        VurUtil v = new VurUtil();
        Usuario u = getLoginBean().getUsuario();
        long f = v.getFechaInicioMesActual();
        //System.out.println("getfechaInicioMes "+f);
        try {
            return getSolicitudEjb().countSolicitudFuncionarioFechaActual(u, s, f);
            //return getSolicitudEjb().countSolicitudFechaActual(s, f);
        } catch (Exception e) {
            return 0;
        }
    }

    public int countSolicitudesDiaSegunEstado(String s) {
        VurUtil v = new VurUtil();
        Usuario u = getLoginBean().getUsuario();
        long f = v.getFechaInicioDiaActual();
        //System.out.println("getfechaInicioMes "+f);
        try {
            return getSolicitudEjb().countSolicitudFuncionarioFechaActual(u, s, f);
        } catch (Exception e) {
            return 0;
        }
    }

    public int countSolicitudesSegunEstado(String s) {
        Usuario u = getLoginBean().getUsuario();
        try {
            return getSolicitudEjb().countAsignadasFuncionario(u, s);
        } catch (Exception e) {
            return 0;
        }
    }

    public int countSolicitudesFuncionarioVencidos(String s) {
        Usuario u = getLoginBean().getUsuario();
        try {
            return getSolicitudEjb().countAsignadasFuncionarioVencidas(u, "Asignado");//x;
        } catch (Exception e) {
            return -1;
        }
    }

    public int countSolicitudesFuncionarioPorVencerse(String s) {
        Usuario u = getLoginBean().getUsuario();
        try {
            return getSolicitudEjb().countAsignadasFuncionarioPorVencerse(u, "Asignado");//x;
        } catch (Exception e) {
            return -1;
        }
    }

    private void createArchivo(Archivo a) {
        //String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
                //id = id + "/" + a.getAdjunto();
            } catch (Exception e) {
            }
            //a.setAdjunto(id);
            if (a.getIdarchivo() < 0) {
                JsfUtil.addSuccessMessage("casi Creo el archivo " + a.toString());
                getArchivoEjb().create(a);
            } else {
                JsfUtil.addSuccessMessage("id Archvo mayor a 0");
            }
        } catch (Exception e) {
            //JsfUtil.addSuccessMessage("xxxx errror  Creo el archivo " + a.toString());
        }
    }

    private void updateSolicitud() {
        // String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
                //id = id + "/" + solicitud.getDescripcion();
            } catch (Exception e) {
            }
            //solicitud.setDescripcion(id);
            getSolicitudEjb().edit(solicitud);
//            String x = " Se a vuelto a editado, / " + respuesta.getIdSolicitud().getConcepto();
//            enviarCorreo(x);
        } catch (Exception e) {
            //
        }
    }

    private void createSolicitud() {
        // String id = getLoginBean().getIdUsuarioSession();
        try {
            try {
                //id = id + "/" + solicitud.getDescripcion();
            } catch (Exception e) {
            }
            //solicitud.setDescripcion(id);
            getSolicitudEjb().create(solicitud);
            String x = " " + solicitud.getConcepto();
            enviarCorreo(x);
            //System.out.println("La solicitud se agregó correctamente");
            JsfUtil.addSuccessMessage("La solicitud se agregó correctamente");
        } catch (Exception e) {
            //System.out.println("error en  createSolicitud");
            JsfUtil.addSuccessMessage("error en  createSolicitud");
        }
    }

    private void enviarCorreo(String d) {
        GestorJavaMail gjm = new GestorJavaMail();
        String correo = solicitud.getIdUsuario().getMail();
        gjm.sendGmail("viacreativa@gmail.com", "j35usMarin", correo, d);
        //gjm.sendGit("contacto@gitinnova.com", mes, correo);
    }

    private Estadorequerimiento getSegunEstado(String s) {
        try {
            //JsfUtil.addSuccessMessage("Creo la solicitud con EstadoRequerimiento ");
            return getEstadoRequerimientoEjb().findEstado(s);
        } catch (Exception ex) {
            JsfUtil.addSuccessMessage(" xxx  Creo la solicitud con EstadoRequerimiento ");
            return null; //
        }
    }

    private Tiposolicitud getSegunTipo(String s) {
        try {
            //JsfUtil.addSuccessMessage("Creo la solicitud con TipoSolicitud ");            
            return getTipoSolicitudEjb().findTipo(s);
        } catch (Exception e) {
            JsfUtil.addSuccessMessage("xxx xCreo la solicitud con TipoSolicitud ");
            return null; //
        }
    }

    public String buscarSolicitud() {
        elDiv = 2;
        idRadicado = "";
        docid = 0;
        return "buscarSolicitud";
    }

    public List<Solicitud> getSolicitudesBusqueda() {
        List<Solicitud> l = new ArrayList<Solicitud>();
        if (elDiv == 0) {
            if (!idRadicado.equals("") && docid == 0) {
                //JsfUtil.addSuccessMessage("busca por  idRadicado ");
                l = buscarSolicitudesIdRadicado();
            } else if (docid != 0 && idRadicado.equals("")) {
                //JsfUtil.addSuccessMessage("busca por doc id ");
                l = buscarSolicitudesDocIdUsuario();
            } else if (docid != 0 && !idRadicado.equals("")) {
                //JsfUtil.addSuccessMessage("busca por los dos id ");
                l = buscarSolicitudesIdRadicadoConIdDoc();
            }
        } else if (elDiv == 1) {
            l = new ArrayList<Solicitud>();
        } else {
            l = new ArrayList<Solicitud>();
        }

        return l;
    }

    public List<Solicitud> buscarSolicitudesIdRadicado() {
        List<Solicitud> l = new ArrayList<Solicitud>();
        try {
            int id = Integer.parseInt(idRadicado);
            Solicitud s = getSolicitudEjb().find(id);
            l.add(s);
            return l;
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> buscarSolicitudesDocIdUsuario() {
        try {
            return getSolicitudEjb().buscarSolicitud(docid);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Solicitud> buscarSolicitudesIdRadicadoConIdDoc() {
        try {
            int id = Integer.parseInt(idRadicado);
            return getSolicitudEjb().buscarSolicitud(id, docid);
        } catch (Exception e) {
            return null;
        }
    }

    /*
     @return List
     */
    public List<Solicitud> getSolicitudesCiudadano() {
        try {
            Usuario u = getLoginBean().getUsuario();
            //JsfUtil.addSuccessMessage("Ciudadano Funcionario "+u.getIdUsuario());
            return getSolicitudesDeUsuario(u);
        } catch (Exception e) {
            return null;
        }
        // return JsfUtil.getSelectItems(getSolicitudEjb().getSolicitudesUsuario(u), false);
    }

    /*
     @ return UsuarioController
     */
    public UsuarioController getUsuarioController() {
        return usuarioController;
    }

    /*
     @param UsuarioController
     */
    public void setUsuarioController(UsuarioController usuarioController) {
        this.usuarioController = usuarioController;
    }

    /**
     * Get the value of solcitud
     *
     *
     * @param Usuario u
     * @return the value of List<Solicitud> de suario
     */
    private List<Solicitud> getSolicitudesDeUsuario(Usuario u) {
        try {
            return getSolicitudEjb().getSolicitudesUsuario(u);
        } catch (Exception e) {
            return null;
        }
        // return JsfUtil.getSelectItems(getSolicitudEjb().getSolicitudesUsuario(u), false);
    }

    private AsignadoFacade getAsignadoEjb() {
        return asignadoEjb;
    }

    private SolicitudFacade getSolicitudEjb() {
        return solicitudEjb;
    }

    private RespuestaFacade getRespuestaEjb() {
        return respuestaEjb;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    private EjeTematicoFacade getEjeTematicoEjb() {
        return ejeTematicoEjb;
    }

    public EmpleadoController getEmpleadoController() {
        return empleadoController;
    }

    public void setEmpleadoController(EmpleadoController empleadoController) {
        this.empleadoController = empleadoController;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    private ArchivoFacade getArchivoEjb() {
        return archivoEjb;
    }

    private TipoSolicitudFacade getTipoSolicitudEjb() {
        return TipoSolicitudEjb;
    }

    private EstadoRequerimientoFacade getEstadoRequerimientoEjb() {
        return estadoRequerimientoEjb;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public SubirArchivosBean getSubirArchivosBean() {
        return subirArchivosBean;
    }

    public void setSubirArchivosBean(SubirArchivosBean subirArchivosBean) {
        this.subirArchivosBean = subirArchivosBean;
    }

    public String getFechaReferencia() {
        return fechaReferencia;
    }

    public void setFechaReferencia(String fechaReferencia) {
        this.fechaReferencia = fechaReferencia;
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    public long getDocid() {
        return docid;
    }

    public void setDocid(long docid) {
        this.docid = docid;
    }

    public Usuario getCiudadano() {
        return usuario;
    }

    public void setCiudadano(Usuario ciudadano) {
        this.usuario = ciudadano;
    }

    public GestionUsuarioBean getGestionUsuarioBean() {
        return gestionUsuarioBean;
    }

    public void setGestionUsuarioBean(GestionUsuarioBean gestionUsuarioBean) {
        this.gestionUsuarioBean = gestionUsuarioBean;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public Dependencia getDependencia() {
        return dependencia;
    }

    public void setDependencia(Dependencia dependencia) {
        this.dependencia = dependencia;
    }

    public Asignado getAsignado() {
        return asignado;
    }

    public void setAsignado(Asignado asignado) {
        this.asignado = asignado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getIdRadicado() {
        return idRadicado;
    }

    public void setIdRadicado(String idRadicado) {
        this.idRadicado = idRadicado;
    }

    public Estadorequerimiento getEstado() {
        return estado;
    }

    public void setEstado(Estadorequerimiento estado) {
        this.estado = estado;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public boolean visualizarBotonRespondido(Solicitud item) {
        return estado.getEstado().equals("Resuelto") || estado.getEstado().equals("Entregado");
    }

    public String verSolicitudSolicitud(Solicitud item) {
        solicitud = item;
//        elDiv = 1;Respondidos
//        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario " + elDiv);
        return "gestionRequerimientos";
    }

    public String vistaRespondidos() {
        elDiv = 0;
        return "Respondidos";
    }

    public String verSolicitudRespondido(Solicitud item) {
        elDiv = 1;
        respuesta = verRespuestaDeSolicitud(item);

        solicitud = item;
        if (estado.getEstado().equals("Resuelto")) {
            elDiv = 1;//se gestiona de resuelto a entregado
            respuesta.setEntregado(new Date());
        } else if (estado.getEstado().equals("Entregado")) {
            elDiv = 2;// se gestiona d eentregado a archivado
            solicitud.setIdestadorequerimiento(getEstadoRequerimientoEjb().findEstado("Archivado"));
            respuesta.setArchivado(new Date());
        }
        return "Respondidos";
    }

    public String actualizarRespuestaSolicitud() {
        Date h = new Date();
        solicitud.setUltimaactualizacion(h);
        solicitud.setIdestadorequerimiento(getEstadoRequerimientoEjb().findEstado("Entregado"));
        respuesta.setEntregado(h);
        elDiv = 1;
        updateRespuesta();
        updateSolicitud();
        return "Respondidos";
    }

    public String archivarRespuestaSolicitud() {
        Date h = new Date();
        solicitud.setUltimaactualizacion(h);
        //solicitud.setIdEstadoRequerimiento(getEstadoRequerimientoEjb().findEstado("Archivado"));
        respuesta.setArchivado(h);
        elDiv = 2;
        updateRespuesta();
        updateSolicitud();
        return "Respondidos";
    }

    public String visualizarNuevaSolicitud() {
        String s = nuevaSolicitud();
        elDiv = 1;
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario " + elDiv);
        return "gestionRequerimientos";
    }

    public String visualizarMisSolicitudes() {
        elDiv = 2;
        solicitud = new Solicitud();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionRequerimientos";
    }

    public String visualizarVerSolicitudes() {
        elDiv = 3;
        solicitud = new Solicitud();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionRequerimientos";
    }

}
