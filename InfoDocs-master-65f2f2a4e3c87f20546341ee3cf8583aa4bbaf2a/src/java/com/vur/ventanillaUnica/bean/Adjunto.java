
package com.vur.ventanillaUnica.bean;

import java.io.Serializable;

/**
 *
 * @author dsilva
 */
//@Embeddable
public class Adjunto implements Serializable {

    //@Lob
    //@Column(columnDefinition = "blob")
    private byte[] archivo;//archivo--> Contenido
    //@Column
    private String tipoContenido;//tipo archivo
    //@Column
    private String nombreAdjunto;//adjunto->nombre

    public Adjunto() {
    }

    public Adjunto(byte[] archivo, String tipoContenido, String nombreAdjunto) {
        this.archivo = archivo;
        this.tipoContenido = tipoContenido;
        this.nombreAdjunto = nombreAdjunto;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public String getNombreAdjunto() {
        return nombreAdjunto;
    }

    public void setNombreAdjunto(String nombreAdjunto) {
        this.nombreAdjunto = nombreAdjunto;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public void setTipoContenido(String tipoContenido) {
        this.tipoContenido = tipoContenido;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adjunto other = (Adjunto) obj;
        if ((this.nombreAdjunto == null) ? (other.nombreAdjunto != null) : !this.nombreAdjunto.equals(other.nombreAdjunto)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.nombreAdjunto != null ? this.nombreAdjunto.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "" + nombreAdjunto+"." + tipoContenido;
    }

    
}
