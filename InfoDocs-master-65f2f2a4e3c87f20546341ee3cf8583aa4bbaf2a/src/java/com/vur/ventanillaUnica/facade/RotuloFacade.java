
package com.vur.ventanillaUnica.facade;

import Modelo.Rotulo;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class RotuloFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Rotulo entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Rotulo entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Rotulo entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Rotulo find(Object id) {
        return getEntityManager().find(Rotulo.class, id);
    }

    public List<Rotulo> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Rotulo.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Rotulo> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Rotulo.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Rotulo> rt = cq.from(Rotulo.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
