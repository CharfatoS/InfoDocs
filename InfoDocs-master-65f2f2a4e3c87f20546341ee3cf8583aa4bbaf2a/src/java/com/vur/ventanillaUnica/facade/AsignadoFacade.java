package com.vur.ventanillaUnica.facade;

import Modelo.Usuario;
import Modelo.Asignado;
import Modelo.Solicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class AsignadoFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Asignado entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Asignado entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Asignado entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Asignado find(Object id) {
        return getEntityManager().find(Asignado.class, id);
    }

    public List<Asignado> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Asignado.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Asignado> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Asignado.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Asignado> rt = cq.from(Asignado.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Asignado> verAsignadoDeUsuario(Usuario u) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            Root<Asignado> rt = cq.from(Asignado.class);            
            cq.select(rt)
                    .where(cb.equal(rt.get("idEmpleado").get("idUsuario"), u)
                    );  
             cq.orderBy(cb.asc(rt.get("idSolicitud").get("idEstadoRequerimiento").get("estado")));
            Query q = getEntityManager().createQuery(cq);            
            return q.getResultList();
        } catch (Exception e) {
            //System.out.println("null error  en facade verAsignadoDeUsuario");
            return null;
        }
    }

    public boolean verifiqueAsignadoDeSolicitud(Solicitud s) {
         try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            Root<Asignado> rt = cq.from(Asignado.class);            
            cq.select(rt)
                    .where(cb.equal(rt.get("idSolicitud"), s)
                    );            
            Query q = getEntityManager().createQuery(cq); 
            List<Asignado> a =  q.getResultList();
             System.out.println("prueba verificacion asignado "+(a.size() > 0));
            return (a.size() > 0);
        } catch (Exception e) {
           // System.out.println("null error  en facade verifiqueAsignadoDeSolicitud");
            return false;
        }
    }

    public Asignado verAsignadoDeSolicitud(Solicitud s) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            Root<Asignado> rt = cq.from(Asignado.class);            
            cq.select(rt)
                    .where(cb.equal(rt.get("idSolicitud"), s)
                    );            
            Query q = getEntityManager().createQuery(cq); 
            List<Asignado> a =  q.getResultList();
            Asignado u = a.get(0);
            //System.out.println("facade AsignadoDeSolicitud "+u.toString());
            return u;
        } catch (Exception e) {
            //System.out.println("null error  en facade verifiqueAsignadoDeSolicitud");
            return null;
        }
    }
    public List<Asignado> verAsignadosListaDeSolicitud(Solicitud s) {
       try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            Root<Asignado> rt = cq.from(Asignado.class);            
            cq.select(rt)
                    .where(cb.equal(rt.get("idSolicitud"), s)
                    );            
            Query q = getEntityManager().createQuery(cq);            
            return q.getResultList();
        } catch (Exception e) {
            //System.out.println("null error  en facade verAsignadosListaDeSolicitud");
            return null;
        }
    }
}
