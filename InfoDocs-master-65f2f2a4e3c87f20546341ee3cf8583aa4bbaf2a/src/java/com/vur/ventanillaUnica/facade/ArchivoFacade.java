package com.vur.ventanillaUnica.facade;

import Modelo.Archivo;
import Modelo.Respuesta;
import Modelo.Solicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class ArchivoFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Archivo entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Archivo entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Archivo entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Archivo find(Object id) {
        return getEntityManager().find(Archivo.class, id);
    }

    public List<Archivo> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Archivo.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Archivo> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Archivo.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Archivo> rt = cq.from(Archivo.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public int getCantidadAdjuntosDeSolicitud(Solicitud item) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Archivo> rt = cq.from(Archivo.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt))
                .where(
                        getEntityManager().getCriteriaBuilder().equal(rt.get("idSolicitud"), item)
                );
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Archivo> verListaArchivosDeSolicitud(Solicitud s) {
         CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Archivo> rt = cq.from(Archivo.class);
        cq.select(rt)
                .where(
                        getEntityManager().getCriteriaBuilder().equal(rt.get("idSolicitud"), s)
                );
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
     public List<Archivo> verListaArchivosDeSolicitud(Respuesta s) {
         CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Archivo> rt = cq.from(Archivo.class);
        cq.select(rt)
                .where(
                        getEntityManager().getCriteriaBuilder().equal(rt.get("idRespuesta"), s)
                );
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
}
