
package com.vur.ventanillaUnica.facade;

import Modelo.Disposicionfinal;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class DisposicionFinalFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Disposicionfinal entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Disposicionfinal entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Disposicionfinal entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Disposicionfinal find(Object id) {
        return getEntityManager().find(Disposicionfinal.class, id);
    }

    public List<Disposicionfinal> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Disposicionfinal.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Disposicionfinal> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Disposicionfinal.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Disposicionfinal> rt = cq.from(Disposicionfinal.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}

