
package com.vur.ventanillaUnica.facade;

import Modelo.Tiporequerimiento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class TipoRequerimientoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Tiporequerimiento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Tiporequerimiento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Tiporequerimiento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Tiporequerimiento find(Object id) {
        return getEntityManager().find(Tiporequerimiento.class, id);
    }

    public List<Tiporequerimiento> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tiporequerimiento.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Tiporequerimiento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tiporequerimiento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Tiporequerimiento> rt = cq.from(Tiporequerimiento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
