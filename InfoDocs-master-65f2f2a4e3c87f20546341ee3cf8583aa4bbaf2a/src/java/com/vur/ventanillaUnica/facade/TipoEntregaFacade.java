
package com.vur.ventanillaUnica.facade;

import Modelo.TipoEntrega;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesus
 */
@Stateless
public class TipoEntregaFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(TipoEntrega entity) {
        getEntityManager().persist(entity);
    }

    public void edit(TipoEntrega entity) {
        getEntityManager().merge(entity);
    }

    public void remove(TipoEntrega entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public TipoEntrega find(Object id) {
        return getEntityManager().find(TipoEntrega.class, id);
    }

    public List<TipoEntrega> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(TipoEntrega.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<TipoEntrega> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(TipoEntrega.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<TipoEntrega> rt = cq.from(TipoEntrega.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
