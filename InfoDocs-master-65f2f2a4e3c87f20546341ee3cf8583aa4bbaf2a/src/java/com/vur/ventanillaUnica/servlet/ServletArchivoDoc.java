package com.vur.ventanillaUnica.servlet;

import Modelo.Archivo;
import com.vur.ventanillaUnica.facade.ArchivoFacade;
import java.io.BufferedOutputStream;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jesus
 */
@WebServlet(urlPatterns = {"/servletArchivo.doc"})
public class ServletArchivoDoc extends HttpServlet  {

   
    @EJB
    private ArchivoFacade archivoEjb;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.setContentType("application/msword");
        String ids = request.getParameter("id");
        int id = Integer.parseInt(ids);
        Archivo archivo = getElArchivo(id);

        try {
            if (archivo.getIdarchivo() > 0) {
                int lg = archivo.getArchivo().length;
                byte[] ba1 = new byte[lg];
                ba1 = archivo.getArchivo();
                BufferedOutputStream fos1 = new BufferedOutputStream(response.getOutputStream());
                fos1.write(ba1);
                fos1.flush();
                fos1.close();
            } else {
//                e.add(new Chunk("Este Recuro no esta Disponibñe ", f8));
//                d.add(e);
            }

        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "/servletArchivo.Doc";
    }// </editor-fold>

    private Archivo getElArchivo(int id) {
        try {
            return getArchivoEjb().find(id);
        } catch (Exception e) {
            return null;
        }
    }

    private ArchivoFacade getArchivoEjb() {
        return archivoEjb;
    }

}
