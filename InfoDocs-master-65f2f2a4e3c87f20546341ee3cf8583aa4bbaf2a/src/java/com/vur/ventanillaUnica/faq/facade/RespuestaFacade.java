package com.vur.ventanillaUnica.faq.facade;

import Modelo.Usuario;
import Modelo.Respuesta;
import Modelo.Solicitud;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class RespuestaFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    public void create(Respuesta entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Respuesta entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Respuesta entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Respuesta find(Object id) {
        return getEntityManager().find(Respuesta.class, id);
    }

    public List<Respuesta> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Respuesta.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Respuesta> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Respuesta.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Respuesta> rt = cq.from(Respuesta.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public boolean verSiExisteRespuesta(Usuario u, Solicitud s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Respuesta> consulta = cb.createQuery(Respuesta.class);
            Root<Respuesta> resp = consulta.from(Respuesta.class);
            consulta.select(resp)
                    .where(cb.equal(resp.get("idUsuario"), u),
                            cb.equal(resp.get("idSolicitud"), s)
                    );
            Query q = getEntityManager().createQuery(consulta);
            List<Respuesta> r = q.getResultList();
            int rx = r.size();
            if (rx > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /*
    *@param Usuario usuarioa funcionario que estan en el login
    *@param Solicitud la solicitud a la que se le va  a hacer la consulta de respuesta
   * @return respuesta , de la lista la primera encontrada
    */
    public Respuesta verRespuesta(Usuario u, Solicitud s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Respuesta> consulta = cb.createQuery(Respuesta.class);
            Root<Respuesta> resp = consulta.from(Respuesta.class);
            consulta.select(resp)
                    .where(cb.equal(resp.get("idUsuario"), u),
                            cb.equal(resp.get("idSolicitud"), s)
                    );
            Query q = getEntityManager().createQuery(consulta);
            List<Respuesta> r = q.getResultList();
            int rx = r.size();
            return r.get(0);
        } catch (Exception ex) {
            return null;
        }

    }
    /*   
    *@param Solicitud la solicitud a la que se le va  a hacer la consulta de respuesta
   * @return respuesta , de la lista la primera encontrada
    */
    public Respuesta verRespuesta( Solicitud s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Respuesta> consulta = cb.createQuery(Respuesta.class);
            Root<Respuesta> resp = consulta.from(Respuesta.class);
            consulta.select(resp)
                    .where(
                            cb.equal(resp.get("idSolicitud"), s)
                    );
            Query q = getEntityManager().createQuery(consulta);
            List<Respuesta> r = q.getResultList();
            int rx = r.size();
            return r.get(0);
        } catch (Exception ex) {
            return null;
        }

    }
}
