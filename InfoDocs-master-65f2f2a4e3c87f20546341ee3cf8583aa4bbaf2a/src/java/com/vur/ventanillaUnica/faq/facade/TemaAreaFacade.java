
package com.vur.ventanillaUnica.faq.facade;

import Modelo.Temaarea;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class TemaAreaFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Temaarea entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Temaarea entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Temaarea entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Temaarea find(Object id) {
        return getEntityManager().find(Temaarea.class, id);
    }

    public List<Temaarea> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Temaarea.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Temaarea> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Temaarea.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Temaarea> rt = cq.from(Temaarea.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
