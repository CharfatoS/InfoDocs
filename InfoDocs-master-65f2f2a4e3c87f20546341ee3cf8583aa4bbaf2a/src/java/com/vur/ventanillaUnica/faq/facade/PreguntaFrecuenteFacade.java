
package com.vur.ventanillaUnica.faq.facade;

import Modelo.Preguntafrecuente;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class PreguntaFrecuenteFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Preguntafrecuente entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Preguntafrecuente entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Preguntafrecuente entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Preguntafrecuente find(Object id) {
        return getEntityManager().find(Preguntafrecuente.class, id);
    }

    public List<Preguntafrecuente> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Preguntafrecuente.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Preguntafrecuente> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Preguntafrecuente.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Preguntafrecuente> rt = cq.from(Preguntafrecuente.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}

