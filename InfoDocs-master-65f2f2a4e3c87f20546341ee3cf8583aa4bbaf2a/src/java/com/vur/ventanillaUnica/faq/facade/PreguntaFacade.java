
package com.vur.ventanillaUnica.faq.facade;

import Modelo.Pregunta;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class PreguntaFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Pregunta entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Pregunta entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Pregunta entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Pregunta find(Object id) {
        return getEntityManager().find(Pregunta.class, id);
    }

    public List<Pregunta> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Pregunta.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Pregunta> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Pregunta.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Pregunta> rt = cq.from(Pregunta.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}

