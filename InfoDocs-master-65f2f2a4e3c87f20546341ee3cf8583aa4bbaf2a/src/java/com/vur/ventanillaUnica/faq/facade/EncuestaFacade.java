
package com.vur.ventanillaUnica.faq.facade;

import Modelo.Encuesta;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class EncuestaFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Encuesta entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Encuesta entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Encuesta entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Encuesta find(Object id) {
        return getEntityManager().find(Encuesta.class, id);
    }

    public List<Encuesta> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Encuesta.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Encuesta> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Encuesta.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Encuesta> rt = cq.from(Encuesta.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}

