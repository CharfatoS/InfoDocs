
package com.vur.usuario.facade;

import Modelo.Empjuridico;
import Modelo.Oficina;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesus
 */
@Stateless
public class OficinaFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    private CriteriaBuilder cb() {
        return getEntityManager().getCriteriaBuilder();
    }

    public void create(Oficina entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Oficina entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Oficina entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Oficina find(Object id) {
        return getEntityManager().find(Oficina.class, id);
    }

    public List<Oficina> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Oficina.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Oficina> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Oficina.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Oficina> rt = cq.from(Oficina.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Oficina> verOficinasEmpJuridico(Empjuridico s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Oficina> consulta = cb.createQuery(Oficina.class);
            Root<Oficina> userW = consulta.from(Oficina.class);
            consulta.select(userW)
                    .where(cb.equal(userW.get("idEmpJuridico"), s)
                    );
            Query q = getEntityManager().createQuery(consulta);            
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
       
    }

    
}
