
package com.vur.usuario.facade;

import Modelo.Empjuridico;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesus
 */
@Stateless
public class EmpJuridicoFacade implements Serializable {

    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }

    private CriteriaBuilder cb() {
        return getEntityManager().getCriteriaBuilder();
    }

    public void create(Empjuridico entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Empjuridico entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Empjuridico entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Empjuridico find(Object id) {
        return getEntityManager().find(Empjuridico.class, id);
    }

    public List<Empjuridico> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Empjuridico.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Empjuridico> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Empjuridico.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Empjuridico> rt = cq.from(Empjuridico.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public boolean verifiqueEmpresaConNit(String s) {
         try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Empjuridico> consulta = cb.createQuery(Empjuridico.class);
            Root<Empjuridico> userW = consulta.from(Empjuridico.class);
            consulta.select(userW).where(cb.equal(userW.get("nit"), s));
            Query q = getEntityManager().createQuery(consulta);
            Empjuridico u = (Empjuridico) q.getSingleResult();
            return (u != null);
        } catch (Exception e) {
            return false;
        }
    }

    public Empjuridico getEmpresaConNit(String s) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Empjuridico> consulta = cb.createQuery(Empjuridico.class);
            Root<Empjuridico> userW = consulta.from(Empjuridico.class);
            consulta.select(userW).where(cb.equal(userW.get("nit"), s));
            Query q = getEntityManager().createQuery(consulta);
            Empjuridico u = (Empjuridico) q.getSingleResult();
            return u;
        } catch (Exception e) {
            return null;
        }
    }

    
}
