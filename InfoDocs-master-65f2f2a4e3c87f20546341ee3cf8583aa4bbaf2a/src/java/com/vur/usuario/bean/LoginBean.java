package com.vur.usuario.bean;

import Modelo.Empleado;
import Modelo.Users;
import Modelo.Usuario;
import com.vur.usuario.facade.UsersFacade;
import com.vur.util.VurUtil;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author jesusmarin
 */
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private String username;
    private Users users;
    private Usuario usuario;
    private String vur;
    @EJB
    private UsersFacade userEjb;

    /*
     *@return String url  de vista de rol
     */
    public String getUrl() {
        String url = "inicio";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        vur = request.getContextPath();
        try {
            username = request.getUserPrincipal().getName();
            users = getUserEjb().verUsersPorUsername(username);
            usuario = users.getIdusuario();
            if (users.getRol().equals("CIUDADANO")) {
                url = "" + vur + "/app/ciudadano/ciudadano.xhtml";
            }
            if (users.getRol().equals("EMPLEADO")) {
                url = "" + vur + "/app/empleado/admin.xhtml";
            }
            if (users.getRol().equals("ADMIN")) {
                url = "" + vur + "/app/admin/admin.xhtml";
            }
            if (users.getRol().equals("SUPER")) {
                url = "" + vur + "/app/super/super.xhtml";
            }
            if (users.getRol().equals("FUNCIONARIO")) {
                url = "" + vur + "/app/funcionario/funcionario.xhtml";
            }
            if (users.getRol().equals("SINACCESO")) {
                url = "" + vur + "/app/sinacceso";
            }
            //actualizarAcceso();
            return url;
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "El Registro de Usuario Fallo!", null));
            return "inicio";
        }
    }

    private void actualizarAcceso() {
        Date d = new Date();
        int x = 0;
        try {
            x = users.getNumaccesos();
        } catch (NullPointerException e) {
            //
        }
        users.setUltimoacceso(d);
        users.setNumaccesos(x + 1);
        updateUsers();
    }

  
    public String getDias(Long tiempo) {
        String ulting = "";
        long residuo = 0;
        if (tiempo > 86400) {
            // 86400 dia
            long dia = tiempo / 86400;
            residuo = tiempo - (86400 * dia);
            if (dia > 1) {
                ulting = ulting + " " + dia + " dias ";
            } else {
                ulting = ulting + " " + dia + " dia ";
            }
            tiempo = residuo;
        }
        return ulting;
    }

    public String ultimoIngreso() {
        int years, months, days, hours, minutes, seconds;
        String result = "";
        Date date = new Date();
        date.setYear(2015);
        date.setMonth(1);
        date.setDate(27);
        date.setHours(11);
        date.setMinutes(42);
        date.setSeconds(30);

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(users.getUltimoacceso());
        cal2.set(cal2.getTime().getYear() + 3800, cal2.getTime().getMonth() + 1, cal2.getTime().getDate());

        years = cal2.getTime().getYear() - cal1.getTime().getYear();
        months = cal2.getTime().getMonth() - cal1.getTime().getMonth();
        days = cal2.getTime().getDate() - cal1.getTime().getDate();
        hours = cal2.getTime().getHours() - cal1.getTime().getHours();
        minutes = cal2.getTime().getMinutes() - cal1.getTime().getMinutes();
        seconds = cal2.getTime().getSeconds() - cal1.getTime().getSeconds();

        result += years > 0 ? years + " años, " : "";
        result += months > 0 ? months + " meses, " : "";
        result += days > 0 ? days + " días, " : "";
        result += hours > 0 ? hours + " horas, " : "";
        result += minutes > 0 ? minutes + " minutos, " : "";
        result += seconds > 0 ? seconds + " segundos." : "";
        return "Hace " + result;
    }
    
//    public String ultimoIngreso() {
//        Date d1 = new Date();
//        Date d2 = new Date();
//        
//        
//        
//        String ulting = "";
//        long hoy = System.currentTimeMillis() / 1000;
//        long ultimo;
//        try {
//            ultimo = users.getUltimoacceso();
//        } catch (Exception e) {
//            ultimo = hoy;
//        }
//        long tiempo = hoy - ultimo;
////        try {
////            tiempo = hoy - ultimo;
////        } catch (Exception e) {
////            //
////        }
//        long residuo = 0;
//        //System.out.println("en hoy " + hoy + "/" + "en ultimo " + ultimo + " / " + " tiempo " + tiempo);
//        if (tiempo > 2629743) {
//            // 2629743 mes
//            long mes = tiempo / 2629743;
//            residuo = tiempo - (2629743 * mes);
//            //System.out.println("en residuo = tiempo % 2629743; /tiempo " + tiempo + " / residuo " + residuo + " / " + mes);
//
//            if (mes > 1) {
//                ulting = ulting + " " + mes + " meses, ";
//            } else {
//                ulting = ulting + " " + mes + " mes, ";
//            }
//            tiempo = residuo;
//        }
//        if (tiempo > 604800) {
//            // 604800 semana
//            long sem = tiempo / 604800;
//            residuo = tiempo - (604800 * sem);
//            //System.out.println("en semana " + tiempo);           
//            if (sem > 1) {
//                ulting = ulting + " " + sem + " semanas, ";
//            } else {
//                ulting = ulting + " " + sem + " semana, ";
//            }
//            tiempo = residuo;
//        }
//        if (tiempo > 86400) {
//            // 86400 dia
//            long dia = tiempo / 86400;
//            residuo = tiempo - (86400 * dia);
//            if (dia > 1) {
//                ulting = ulting + " " + dia + " dias, ";
//            } else {
//                ulting = ulting + " " + dia + " dia, ";
//            }
//            tiempo = residuo;
//        }
//        if (tiempo > 3600) {
//            // 3600 hora
//            long hora = tiempo / 3600;
//            residuo = tiempo - (3600 * hora);
//            if (hora > 1) {
//                ulting = ulting + " " + hora + " horas, ";
//            } else {
//                ulting = ulting + " " + hora + " hora, ";
//            }
//            tiempo = residuo;
//        }
//        if (tiempo > 60) {
//            // 60 minuto
//            long min = tiempo / 60;
//            residuo = tiempo - (60 * min);
//            if (tiempo > 1) {
//                ulting = ulting + " " + min + " minutos, ";
//            } else {
//                ulting = ulting + " " + min + " minuto, ";
//            }
//            tiempo = residuo;
//        }
//        if (tiempo > 1) {
//            ulting = ulting + " " + tiempo + " segundos ";
//        } else {
//            ulting = ulting + " " + tiempo + " segundo ";
//        }
//        return "" + ulting;
//    }

    public Empleado getEmpleado() {
        try {
            //return usuario.getIdEmpleado();
            return new Empleado();
        } catch (Exception e) {
            Empleado em = new Empleado();
            em.setIdempleado(-10);
            return em;
        }
    }

    public String getIdUsuarioSession() {
        try {
            return "" + usuario.getDocid()+ "/";
        } catch (Exception e) {
            Empleado em = new Empleado();
            em.setIdempleado(-10);
            return "-1/";
        }
    }

    /*
     @return String
     */
    public String getPath() {
        String url = "";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        url = ("vur: " + vur + " / path: " + request.getContextPath());
        return url;
    }
    /*
     @return String
     */

    public String logOut() throws ServletException {
        actualizarAcceso();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        request.logout();
        return "inicio";
    }

    /*
     @return  boolean
     */
    public boolean enSession() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            String c = request.getUserPrincipal().getName();
            return !c.equals(null);  //true hay una session/ false no hay una session
        } catch (Exception e) {
            return false;
        }
    }
    /*
     @return String    
     */

    public String getNombreUsuario() {
        String nombre = " ";
        try {
            nombre = users.getIdusuario().getNombre();
            return nombre;
        } catch (Exception e) {
            return " ";
        }
    }

    private void updateUsers() {
        try {
            getUserEjb().edit(users);
        } catch (Exception e) {
            //
        }
    }

    /*
     @retun Users
     */
    private UsersFacade getUserEjb() {
        return userEjb;
    }

    /*
     @return Users
     */
    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    /*
     @return String
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /*
     @return String
     */
    public String getVur() {
        return vur;
    }

    public void setVur(String vur) {
        this.vur = vur;
    }

    /*
     @return Usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
