package com.vur.usuario.bean;

//import com.hash.app.hashSHA256;
//import com.sun.org.apache.bcel.internal.generic.Select;
import com.vur.controller.util.JsfUtil;
import Modelo.Ciudad;
import Modelo.Lugar;
import Modelo.Empleado;
import com.vur.institucional.facade.EmpleadoFacade;
import Modelo.Users;
import com.vur.usuario.facade.UsersFacade;
import Modelo.Usuario;
import com.vur.usuario.facade.UsuarioFacade;
import Modelo.Direccion;
import Modelo.Empjuridico;
import Modelo.Oficina;
import Modelo.Cargoempleadooficina;
import Modelo.Direccionusuario;
import Modelo.Estadolaboral;
import Modelo.Rol;
import com.vur.usuario.facade.DireccionFacade;
import com.vur.usuario.facade.EmpJuridicoFacade;
import com.vur.usuario.facade.OficinaFacade;
import com.vur.usuario.facade.UsuarioOficinaFacade;
import com.vur.util.VurUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@ManagedBean(name = "gestionUsuarioBean")
@SessionScoped
public class GestionUsuarioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private FacesContext fc;
    private HttpServletRequest hsr;
//    private String url = "inicio";
    private String username = null;
    private String rol = null;
    private Users currentUsers;
    private Usuario currentUsuario;
    private Empleado empleado;
    private Empjuridico empJuridico;
    private Cargoempleadooficina cargoempleadooficinaoficina;
    private Oficina oficina;
    private Direccion currentDireccion;
    private Direccionusuario currentDireccionUsuario;
    private Ciudad ciudad;
    private Lugar lugar;
    private int elDiv;
    private long docId;
    private boolean juridica;
    // private hashSHA256 codificador;

    @EJB
    private UsersFacade ejbUsers;
    @EJB
    private UsuarioFacade ejbUsuario;
    @EJB
    private EmpleadoFacade ejbEmpleado;
    @EJB
    private EmpJuridicoFacade ejbEmpJuridico;
    @EJB
    private UsuarioOficinaFacade ejbUsuarioOficina;
    @EJB
    private OficinaFacade ejbOficina;
    @EJB
    private DireccionFacade ejbDireccion;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;

    public GestionUsuarioBean() {
    }

    public String verifiqueUsuarioCiudadanoDocId() {
//         elDiv =1;
//         JsfUtil.addErrorMessage("huy si etro al metodo");
        verificarDocIdentidadCiudadano();
        return "crearUsuario";
    }

    public String verificarDocIdentidadCiudadano() {
        System.out.println("doc id  " + docId);
        boolean existe = consulteDocId(docId);
        elDiv = (existe) ? 20 : 1;
        //JsfUtil.addSuccessMessage("que paso? " + existe);
        if (existe == false) {
            establecerComoCiudadano();
            currentUsuario.setDocid(docId);
            currentUsers.setPassword(docId);
            currentUsers.setMail("a@mail.com");
        } else {
            currentUsuario = consulteUsuarioDocId(docId);
        }
        return "crearUsuario";
    }

    public String verJuridicoNatural() {
        String g = "";
        if (juridica == true) {
            g = "Jurídica";
        } else {
            g = "Natural";
        }
        return g;
    }

    public boolean desplegarFormularioCiudadano() {
        if (juridica == true) {
            return false;
        } else {
            return true;
        }
    }

    public void establecerComoCiudadano() {
        vaciarUsuario();
        inicializar();
        Rol rol = new Rol();
        rol.setRol("CIUDADANO");
        currentUsers.setRol(rol);
    }

    public String visualizarPanelUsuarios() {
        elDiv = 0;
        currentUsuario = new Usuario();
        //JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionUsuarios";
    }

    public String visualizarCrearNuevoUsuario() {
        elDiv = 10;//para crear
        docId = 0;
        currentUsuario = new Usuario();
        JsfUtil.addSuccessMessage("Ingrese el dato de busqueda");
        return "gestionUsuarios";
    }

    public String consulteEmpleadoPorDocId() {
        if (consulteDocId(docId)) {
            elDiv = 31;//12
            currentUsuario = consulteUsuarioDocId(docId);
            asigneDatos();
            JsfUtil.addSuccessMessage("El Empleado tiene registro de usuario en el sistema");
        } else {

        }
        return "gestionUsuarios";
    }

    public String consulteUsuarioDocId() {
        if (consulteDocId(docId)) {
            elDiv = 21;//12
            currentUsuario = consulteUsuarioDocId(docId);
            asigneDatos();
            JsfUtil.addSuccessMessage("El Empleado tiene registro de usuario en el sistema");
        } else {
            elDiv = 11;
            currentUsuario = new Usuario();
            //Empleado ?
            vaciarUsuario();
            inicializar();
            currentUsuario.setDocid(docId);
            currentUsers.setMail("a@mail.com");
            currentDireccion.setWeb("n/a");
            JsfUtil.addSuccessMessage("Ingrese el dato de busqueda");
        }
        return "gestionUsuarios";
    }

    private void asigneDatos() {
        verUsersDelUsuario();
        verEmpleadoDelUsuario();
        verDireccionDelUsuario();
    }

    private void verDireccionDelUsuario() {
        try {
            currentDireccion = getEjbDireccion().verDireccionDeUsuario(currentUsuario); //currentUsuario.getIdDireccion();
        } catch (Exception e) {
        }

    }

    private void verEmpleadoDelUsuario() {
        try {
            empleado = getEjbEmpleado().verEmpleadoDeUsuario(currentUsuario);//currentUsuario.getIdEmpleado();
        } catch (Exception e) {
            empleado = null;
        }
    }

    private void verUsersDelUsuario() {
        try {
            currentUsers = getEjbUsers().verUsersPorUsuario(currentUsuario);//currentUsuario.getIdUsers();
        } catch (Exception e) {
        }
    }

    public String editarDatosEmpleado() {
        editUsuario();
        currentUsers.setIdusuario(currentUsuario);
        Rol r = new Rol();
        r.setRol(currentUsers.getRol().getRol());
        currentUsers.setRol(r);

        editUsers();
        currentDireccion.setIdlugar(lugar);
        editDireccion();
        if (empleado != null) {
            createUpdateEmpleado();
        }
        elDiv = 21;
        return "gestionUsuarios";
    }

    public String commitUsuarioFuncioNarioAdmin() {
        boolean c = crearFuncionario();
        if (c) {
            elDiv = 12;
            JsfUtil.addSuccessMessage("guardó datos correctamente");
        } else {
            elDiv = 11;
            JsfUtil.addErrorMessage("Error el guardar datos");
        }
        return "gestionUsuarios";
    }

    public boolean verBotonAgregarCargo() {
        try {
            if (empleado.getIdempleado() > 0) {
                System.out.println("true");
                return true;//si tiene empleado  se muestra Editar
            } else {
                System.out.println("false");
                return false;
            }
        } catch (Exception e) {
            System.out.println("catch false");
            return false;
        }
    }

    public String habilitarCrearEmpleado() {
        elDiv = 13;//habilita formulario para crear emplado
        iniciarEmpleado();
        return "gestionUsuarios";
    }

    public String habilitarEditarUsuario() {
        lugar = getCurrentDireccion().getIdlugar();
        elDiv = 22;//habilita formulario para editar empleado        
        return "gestionUsuarios";
    }

    public String habilitarEditarEmpleado() {
        elDiv = 32;//habilita formulario para editar empleado        
        return "gestionUsuarios";
    }

    public String crearEditarEmpleado() {
        elDiv = 12;//crea actualiza emplado
        if (empleado.getIdempleado() > 0) {
            elDiv = 31;
        }
        createUpdateEmpleado();
        return "gestionUsuarios";
    }

    private void iniciarEmpleado() {
        long fecha = System.currentTimeMillis() / 1000;
        empleado = new Empleado();
        empleado.setDocidusuario(currentUsuario);
        Estadolaboral estado = new Estadolaboral();
        estado.setEstado(empleado.getIdestadolaboral().getEstado());
        empleado.setIdestadolaboral(estado);
        Date f = new Date();
        f.setTime(fecha);
        empleado.setFecharegistro(f);
    }

    public String visualizarEditarNuevoUsuario() {
        elDiv = 20;//para editar
        currentUsuario = new Usuario();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionUsuarios";
    }

    public String visualizarEditarEmpleado() {
        elDiv = 30;//para editar
        currentUsuario = new Usuario();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionUsuarios";
    }

    private void inicializar() {
        getSelectedCurrentDireccion();
        getSelectedCurrentUsuario();
        //getSelectedCurrentPreferencias();
        getSelectedCurrentUsers();
    }

    public void vaciarUsuario() {
        setCurrentDireccion(null);
        setCurrentUsuario(null);
        //setCurrentPreferencias(null);
        setCurrentUsers(null);
    }

    private UsersFacade getEjbUsers() {
        return ejbUsers;
    }

    private UsuarioFacade getEjbUsuario() {
        return ejbUsuario;
    }

//    private PreferenciasFacade getEjbPreferencias() {
//        return ejbPreferencias;
//    }
    private DireccionFacade getEjbDireccion() {
        return ejbDireccion;
    }

    public Direccion getSelectedCurrentDireccion() {
        if (currentDireccion == null) {
            currentDireccion = new Direccion();
        }
        return currentDireccion;
    }

//    public Preferencias getSelectedCurrentPreferencias() {
//        if (currentPreferencias == null) {
//            currentPreferencias = new Preferencias();
//        }
//        return currentPreferencias;
//    }
    public Users getSelectedCurrentUsers() {
        if (currentUsers == null) {
            currentUsers = new Users();
        }
        return currentUsers;
    }

    public Usuario getSelectedCurrentUsuario() {
        if (currentUsuario == null) {
            currentUsuario = new Usuario();
        }
        return currentUsuario;
    }

    public void setCurrentDireccion(Direccion currentDireccion) {
        this.currentDireccion = currentDireccion;
    }

//    public void setCurrentPreferencias(Preferencias currentPreferencias) {
//        this.currentPreferencias = currentPreferencias;
//    }
    public List<Usuario> consultarTodos() {
        inicializar();
        return ejbUsuario.findAll();
    }

    public List<Usuario> consultarParametro(int tipo, String parametro) {
        inicializar();
        switch (tipo) {
            case 0:

                break;
        }
        return null;
    }

    public void Consultar(int id) {
        inicializar();
        currentUsuario = ejbUsuario.find(id);
        currentDireccion = ejbDireccion.find(id);
        currentUsers = ejbUsers.find(id);
    }

    private Empjuridico getLaEmpJuridica(String s) {
        try {
            return getEjbEmpJuridico().getEmpresaConNit(s);
        } catch (Exception e) {
            return null;
        }
    }

    private void createUpdateEmpJuridico() {
        try {
            if (empJuridico.getNit() == null) {
                getEjbEmpJuridico().create(empJuridico);
            } else {
                getEjbEmpJuridico().edit(empJuridico);
            }
        } catch (Exception e) {
            //
        }
    }

    private boolean consulteNit(String s) {
        try {
            return getEjbEmpJuridico().verifiqueEmpresaConNit(s);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean consulteDocId(long s) {
        try {
            System.out.println("consulteDocId  ");
            return getEjbUsuario().verifiqueUsuarioConId(s);
        } catch (Exception e) {
            System.out.println("consulteDocId  catch");
            return false;
        }
    }

    private Usuario consulteUsuarioDocId(long s) {
        try {
            return getEjbUsuario().findUsuarioConId(s);
        } catch (Exception e) {
            return null;
        }
    }

    public String Crear() {
        fc = FacesContext.getCurrentInstance();
        hsr = (HttpServletRequest) fc.getExternalContext().getRequest();
        //codificador = new hashSHA256();

        if (hsr.getUserPrincipal() != null) {
            username = hsr.getUserPrincipal().toString();
            rol = ejbUsers.buscarRolPorUsername(username).getRol();
        }
        return "inicio";
    }

    public String irFormularioCrearUsuario() {
        docId = 0;
        establecerComoCiudadano();
        elDiv = 0;
        return "crearUsuario";
    }

    public String creacionCiudadanoJuridico() throws ServletException {
        elDiv = 2;
        String d = creacionCiudadano();
        docId = 0;
        return "crearUsuario";
    }

    public String habilitarOficinaCiudadanoJuridico() throws ServletException {
        elDiv = 5;
        return "crearUsuario";
    }

    public String verificarNitJuridico() {
        boolean nitEx = consulteNit(String.valueOf(docId));
        elDiv = (nitEx) ? 4 : 3;
        if (nitEx) {
            // busca la empresa juridica
            empJuridico = getLaEmpJuridica(String.valueOf(docId));
        } else {
            //inicia EmpJuridico
            empJuridico = new Empjuridico();
            empJuridico.setNit(String.valueOf(docId));
            empJuridico.setMail("a@mail.com");
            empJuridico.setWeb("www.miweb.com");
        }

        return "crearUsuario";
    }

    public String crearEmpJuridico() {
        createUpdateEmpJuridico();
        currentUsuario.setIdempjuridico(empJuridico);
        editUsuario();
        elDiv = 4;
        return "crearUsuario";
    }

    public String creacionCiudadano() throws ServletException {
        boolean existe = consulteDocId(currentUsuario.getDocid());
        if (existe) {
            JsfUtil.addErrorMessage("Es documento ya existe en el sistema");
            return "crearUsuario";
        } else {
            boolean x = crearCiudadano();
            if (x) {
                fc = FacesContext.getCurrentInstance();
                hsr = (HttpServletRequest) fc.getExternalContext().getRequest();
                //codificador = new hashSHA256();
                hsr.logout();

                hsr.login(currentUsers.getMail(), currentUsers.getPassword());
                if (hsr.getUserPrincipal() != null) {
                    username = hsr.getUserPrincipal().toString();
                    rol = getEjbUsers().buscarRolPorUsername(username).getRol();
                }
                return "inicio";
            } else {
                JsfUtil.addErrorMessage("Error en la creacion de datos");
                return "crearUsuario";
            }
        }

    }

    public String crearCiudadanoDesdeFuncionario(String pagina) {

        boolean x = crearCiudadano();
        return pagina;
    }

    public boolean crearUsuarioCiudadano() {
        // JsfUtil.addSuccessMessage("En crear Nuevo requerimiento vista Funcionario "+c);
        boolean x = crearCiudadano();
        return x;
    }

    private boolean crearCiudadano() {
        boolean creado = false;
        try {
            //guarda ciudadano
            if (currentUsuario.getDocid() < 0) {
                createUsuario();
                // JsfUtil.addSuccessMessage(currentUsuario.toString());
                //guarda users
                if (currentUsuario.getDocid() > 0) {
                    currentUsers.setIdusuario(currentUsuario);
                    currentUsers.setNumaccesos(1);

                    //currentUsers.setMail(currentUsuario.getDocId());
                    //currentUsers.setPassword(currentUsuario.getDocId());
                    Rol r = new Rol();
                    r.setRol("CIUDADANO");
                    currentUsers.setRol(r);
                    Date d = new Date();
                    d.setTime(System.currentTimeMillis() / 100);
                    currentUsers.setUltimoacceso(d);
                    createUsers();
                    //JsfUtil.addSuccessMessage(currentUsers.toString());
                    // guarda direccion currentDireccion

                    //currentDireccion.setIddireccion();
                    currentDireccion.setIdciudad(ciudad);
                    currentDireccion.setIdlugar(lugar);
                    
                    
                    //createDireccion();
                    getCurrentDireccionUsuario().setIddireccion(currentDireccion);
                    //JsfUtil.addSuccessMessage(currentDireccion.toString());
                    if (currentUsuario.getDocid() > 0 && currentUsers.getIdusuario().getDocid() > 0 && currentDireccion.getIddireccion() > 0) {
                        creado = true;
                        JsfUtil.addSuccessMessage(" Los datos se agregaron Corectamente");
                    }
                }
            }

            return creado;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean crearFuncionario() {
        try {
            long fch = System.currentTimeMillis() / 1000;
            currentUsers.setNumaccesos(0);
            //currentUsers.setUsername(currentUsuario());
            //currentUsers.setPassword(currentUsuario.getDocId());

            Rol r = new Rol();
            r.setRol(currentUsers.getRol().getRol());
            currentUsers.setRol(r);

            Date d = new Date();
            d.setTime(fch);
            currentUsers.setUltimoacceso(d);
            currentUsers.setNumaccesos(1);
            currentUsers.setIdusuario(currentUsuario);
            // currentUsers.setPassword(codificador.EncriptarSHA256hex(currentUsuario.getDocId()));
            createUsuario();
            createUsers();
           // currentDireccion.setIdUsuario(currentUsuario);
            currentDireccion.setIdlugar(lugar);
            currentDireccion.setIdciudad(ciudad);
            createDireccion();
            empleado = new Empleado();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void createUsuario() {
        Usuario u = getLoginBean().getUsuario();
        VurUtil vu = new VurUtil();
        try {
            //String g = currentUsuario.getGrupo();
            //g = "" + u.getIdUsuario() + "/" + g;
            //currentUsuario.setGrupo(g);
            currentUsuario = vu.arreglarNombreEnMayuscula(currentUsuario);
            getEjbUsuario().create(currentUsuario);
            JsfUtil.addSuccessMessage("Guardo correctamente la Persona.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar la persona.");
        }
    }

    private void createUsers() {
        Usuario u = getLoginBean().getUsuario();
        try {
            //currentUsers.setLogusers("" + u.getIdUsuario());
            getEjbUsers().create(currentUsers);
            //JsfUtil.addSuccessMessage("Guardo correctamente el usuario.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar el createUsers.");
        }
    }

    private void createDireccion() {
        Usuario u = getLoginBean().getUsuario();
        //String id = "" + u.getIdUsuario();
        try {
            //id = id + "/" + currentDireccion.getWeb();
            //currentDireccion.setWeb(id);
            //agregarTipoLugarADireccion();
            getEjbDireccion().create(currentDireccion);
            // JsfUtil.addSuccessMessage("Guardo correctamente la direccion.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar la direccion.");
        }
    }

    private void agregarTipoLugarADireccion() {
        try {
            String tl = currentDireccion.getIdlugar().getNombre();
            String barr = tl + "/" + currentDireccion.getBarrio();
            currentDireccion.setBarrio(barr);
        } catch (Exception e) {
        }
    }

    public SelectItem[] getItemsOficiasdeEmpJuridico() {
        return JsfUtil.getSelectItems(getEjbOficina().verOficinasEmpJuridico(empJuridico), true);
    }

    public void Editar() {
        if (!currentUsuario.equals(
                getEjbUsuario().find(
                        currentUsuario.getDocid()))) {
            editUsuario();
        }
        if (!currentUsers.equals(
                getEjbUsers().find(
                        currentUsers.getIdusuario()))) {
            editUsers();
        }
        if (!currentDireccion.equals(
                getEjbDireccion().find(
                        currentDireccion.getIddireccion()))) {
            editDireccion();
        }
    }

    private void createUpdateEmpleado() {
        try {
            if (empleado.getIdempleado()< 0) {
                // JsfUtil.addSuccessMessage("id empleado create "+empleado.getIdEmpleado());
                getEjbEmpleado().create(empleado);
            } else {
                // JsfUtil.addSuccessMessage("id empleado en edit "+empleado.getIdEmpleado());
                getEjbEmpleado().edit(empleado);
            }
        } catch (Exception e) {
            //JsfUtil.addSuccessMessage("id empleado en catch "+empleado.getIdEmpleado());
        }
    }

    private void editUsuario() {
        VurUtil v = new VurUtil();
        try {
            currentUsuario = v.arreglarNombreEnMayuscula(currentUsuario);
            getEjbUsuario().edit(currentUsuario);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar la persona.");
        }
    }

    private void editUsers() {
        //Usuario u = getLoginBean().getUsuario();
        try {
            //currentUsers.setLogusers("" + u.getIdUsers());
            getEjbUsers().edit(currentUsers);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar el usuario.");
        }
    }

    public void editDireccion() {
        try {
            getEjbDireccion().edit(currentDireccion);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar la direccion.");
        }
    }

    public FacesContext getFc() {
        return fc;
    }

    public String[] getTipoDocumento() {
        String[] l = {"---", "Cédula", "Pasaporte", "Cédula de extranjería", "Tarjeta de Identidad", "Otro"};
        return l;
    }

    private EmpleadoFacade getEjbEmpleado() {
        return ejbEmpleado;
    }

    private EmpJuridicoFacade getEjbEmpJuridico() {
        return ejbEmpJuridico;
    }

    private UsuarioOficinaFacade getEjbUsuarioOficina() {
        return ejbUsuarioOficina;
    }

    private OficinaFacade getEjbOficina() {
        return ejbOficina;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;
    }

    public void setFc(FacesContext fc) {
        this.fc = fc;
    }

    public HttpServletRequest getHsr() {
        return hsr;
    }

    public void setHsr(HttpServletRequest hsr) {
        this.hsr = hsr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Direccion getCurrentDireccion() {
        return currentDireccion;
    }

//    public Preferencias getCurrentPreferencias() {
//        return currentPreferencias;
//    }
//
//    public void setEjbPreferencias(PreferenciasFacade ejbPreferencias) {
//        this.ejbPreferencias = ejbPreferencias;
//    }
    public void setEjbDireccion(DireccionFacade ejbDireccion) {
        this.ejbDireccion = ejbDireccion;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Users getCurrentUsers() {
        return currentUsers;
    }

    public void setCurrentUsers(Users currentUsers) {
        this.currentUsers = currentUsers;
    }

    public Usuario getCurrentUsuario() {
        return currentUsuario;
    }

    public void setCurrentUsuario(Usuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    public boolean isJuridica() {
        return juridica;
    }

    public void setJuridica(boolean juridica) {
        this.juridica = juridica;
    }

    public long getDocId() {
        return docId;
    }

    public void setDocId(long docId) {
        this.docId = docId;
    }

    public Empjuridico getEmpJuridico() {
        return empJuridico;
    }

    public void setEmpJuridico(Empjuridico empJuridico) {
        this.empJuridico = empJuridico;
    }

//    public UsuarioOficina getUsuarioOficina() {
//        return usuarioOficina;
//    }
//
//    public void setUsuarioOficina(UsuarioOficina usuarioOficina) {
//        this.usuarioOficina = usuarioOficina;
//    }
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public Direccionusuario getCurrentDireccionUsuario() {
        return currentDireccionUsuario != null ? currentDireccionUsuario : new Direccionusuario();
    }

    public void setCurrentDireccionUsuario(Direccionusuario currentDireccionUsuario) {
        this.currentDireccionUsuario = currentDireccionUsuario;
    }

}
