package com.vur.usuario.controller;

//import com.hash.app.hashSHA256;
import com.vur.controller.util.JsfUtil;
import Modelo.Users;
import com.vur.usuario.facade.UsersFacade;
import Modelo.Usuario;
import com.vur.usuario.facade.UsuarioFacade;
import Modelo.Direccion;
import Modelo.Rol;
import com.vur.usuario.facade.DireccionFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean(name = "gestionUsuarioController")
@SessionScoped
public class GestionUsuarioController implements Serializable {

    private static final long serialVersionUID = 1L;
    private FacesContext fc;
    private HttpServletRequest hsr;
    private String url = "inicio";
    private String username = null;
    private Rol rol = null;
    private Users currentUsers;
    private Usuario currentUsuario;
    private Direccion currentDireccion;
    private int elDiv;
    // private hashSHA256 codificador;

    @EJB
    private com.vur.usuario.facade.UsersFacade ejbUsers;
    @EJB
    private com.vur.usuario.facade.UsuarioFacade ejbUsuario;
    @EJB
    private com.vur.usuario.facade.DireccionFacade ejbDireccion;

    public GestionUsuarioController() {
    }

    public void establecerComoCiudadano() {
        inicializar();
        //currentUsuario.setGrupo("CIUDADANO");
        Rol r = new Rol();
        r.setRol("CIUDADANO");
        currentUsers.setRol(r);
    }

    private void inicializar() {
        getSelectedCurrentDireccion();
        getSelectedCurrentUsuario();
        getSelectedCurrentUsuario();
    }

    public void vaciarUsuario() {
        setCurrentDireccion(null);
        setCurrentUsuario(null);
        setCurrentUsers(null);
    }

    private UsersFacade getEjbUsuario() {
        return ejbUsers;
    }

    private UsuarioFacade getEjbPersona() {
        return ejbUsuario;
    }

    private DireccionFacade getEjbDireccion() {
        return ejbDireccion;
    }

    public Direccion getSelectedCurrentDireccion() {
        if (currentDireccion == null) {
            currentDireccion = new Direccion();
        }
        return currentDireccion;
    }

    public Users getSelectedCurrentUsers() {
        if (currentUsers == null) {
            currentUsers = new Users();
        }
        return currentUsers;
    }

    public Usuario getSelectedCurrentUsuario() {
        if (currentUsuario == null) {
            currentUsuario = new Usuario();
        }
        return currentUsuario;
    }

    public void setCurrentDireccion(Direccion currentDireccion) {
        this.currentDireccion = currentDireccion;
    }

    public List<Usuario> consultarTodos() {
        inicializar();
        return ejbUsuario.findAll();
    }

    public List<Usuario> consultarParametro(int tipo, String parametro) {
        inicializar();
        switch (tipo) {
            case 0:

                break;
        }
        return null;
    }

    public void Consultar(int id) {
        inicializar();
        currentUsuario = ejbUsuario.find(id);
        currentDireccion = ejbDireccion.find(id);
        currentUsers = ejbUsers.find(id);
    }

    public String creacionCiudadano() {
        boolean existe = consulteDocId(currentUsuario.getDocid());
        if (existe) {
            JsfUtil.addErrorMessage("Es documento ya existe en el sistema");
            return "crearUsuario";
        } else {
            boolean x = crearCiudadano();
            if (x) {
                fc = FacesContext.getCurrentInstance();
                hsr = (HttpServletRequest) fc.getExternalContext().getRequest();
                //codificador = new hashSHA256();

                if (hsr.getUserPrincipal() != null) {
                    username = hsr.getUserPrincipal().toString();
                    rol = ejbUsers.buscarRolPorUsername(username);
                }
                return "ciudadano";
            } else {
                JsfUtil.addErrorMessage("Error en la creacio de datos");
                return "crearUsuario";
            }
        }

    }

    private boolean consulteDocId(long s) {
        try {
            return getEjbPersona().verifiqueUsuarioConId(s);
        } catch (Exception e) {
            return false;
        }
    }

    public String Crear() {
        fc = FacesContext.getCurrentInstance();
        hsr = (HttpServletRequest) fc.getExternalContext().getRequest();
        //codificador = new hashSHA256();

        if (hsr.getUserPrincipal() != null) {
            username = hsr.getUserPrincipal().toString();
            rol = ejbUsers.buscarRolPorUsername(username);
        }

        try {
            if ("SUPER".equals(rol)) {
                if (!ejbUsuario.BuscarPersonaNueva(currentUsuario)) {
                    if (crearFuncionario()) {
                        url = "app/super/usuario";
                    } else {
                        url = "error";
                    }
                }
            } else if ("FUNCIONARIO".equals(rol)) {
                if (!ejbUsuario.BuscarPersonaNueva(currentUsuario)) {
                    if (crearCiudadano()) {
                        url = "app/funcionario/usuario";
                    } else {
                        url = "error";
                    }
                }
            } else {
                if (!ejbUsuario.BuscarPersonaNueva(currentUsuario)) {
                    if (crearCiudadano()) {
                        url = "app/ciudadano/usuario";
                    } else {
                        url = "error";
                    }
                }
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al validar el rol de usuario.");
            url = "error";
            return url;
        }
        return url;
    }

    public String crearUsuarioCiudadano(String pagina) {
        // JsfUtil.addSuccessMessage("En crear Nuevo requerimiento vista Funcionario "+c);
        boolean x = crearCiudadano();
        return pagina;
    }

    private boolean crearCiudadano() {
        Rol r = new Rol();
        r.setRol("CIUDADANO");
        try {
            currentUsers.setNumaccesos(0);
            currentUsers.setUsername(currentUsuario.getDocid());
            currentUsers.setPassword(codificador.EncriptarSHA256hex(currentUsuario.getDocId()));
//            currentUsers.setPassword(currentUsuario.getDocid());
            currentUsers.setIdusuario(currentUsuario);
            currentUsers.setRol(r);
            
            currentDireccion.setIdciudad(null);
            currentDireccion.setIdlugar(null);
            currentDireccion.setIdusuario(null);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean crearFuncionario() {
        try {
            currentUsers.setNumaccesos(0);
            currentUsers.setIdusuario(currentUsuario);
            // currentUsers.setPassword(codificador.EncriptarSHA256hex(currentUsuario.getDocId()));

            createPersona();
            createUsuario();
            createDireccion();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void createPersona() {
        try {
            getEjbPersona().create(currentUsuario);
            JsfUtil.addSuccessMessage("Guardo correctamente la persona.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar la persona.");
        }
    }

    public void createUsuario() {
        try {
            getEjbUsuario().create(currentUsers);
            JsfUtil.addSuccessMessage("Guardo correctamente el usuario.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar el usuario.");
        }
    }

    public void createDireccion() {
        try {
            getEjbDireccion().create(currentDireccion);
            JsfUtil.addSuccessMessage("Guardo correctamente la direccion.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al guardar la direccion.");
        }
    }

    public void Editar() {
        if (!currentUsuario.equals(
                getEjbPersona().find(
                        currentUsuario.getDocid()))) {
            editPersona();
        }
        if (!currentUsers.equals(
                getEjbUsuario().find(
                        currentUsers.getMail()))) {
            editUsuario();
        }
        if (!currentDireccion.equals(
                getEjbDireccion().find(
                        currentDireccion.getIddireccion()))) {
            editDireccion();
        }
    }

    public void editPersona() {
        try {
            getEjbPersona().edit(currentUsuario);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar la persona.");
        }
    }

    public void editUsuario() {
        try {
            getEjbUsuario().edit(currentUsers);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar el usuario.");
        }
    }

    public void editDireccion() {
        try {
            getEjbDireccion().edit(currentDireccion);
            JsfUtil.addSuccessMessage("Cambios realizados correctamente.");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al editar la direccion.");
        }
    }

    public FacesContext getFc() {
        return fc;
    }

    public void setFc(FacesContext fc) {
        this.fc = fc;
    }

    public HttpServletRequest getHsr() {
        return hsr;
    }

    public void setHsr(HttpServletRequest hsr) {
        this.hsr = hsr;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    private UsersFacade getEjbUsers() {
        return ejbUsers;
    }
    

    public Direccion getCurrentDireccion() {
        return currentDireccion;
    }

    public String visualizarCrearNuevoUsuario(){
        elDiv = 1;
        currentUsuario = new Usuario();
        JsfUtil.addSuccessMessage("Ingrese los datos en el Formulario");
        return "gestionUsuarios";
    }

    public int getElDiv() {
        return elDiv;
    }

    public void setElDiv(int elDiv) {
        this.elDiv = elDiv;
    }

    public Users getCurrentUsers() {
        return currentUsers;
    }

    public void setCurrentUsers(Users currentUsers) {
        this.currentUsers = currentUsers;
    }

    public Usuario getCurrentUsuario() {
        return currentUsuario;
    }

    public void setCurrentUsuario(Usuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }

    
    
}
