
package com.vur.geo.facade;

import Modelo.Ciudad;
import Modelo.Departamento;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class CiudadFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Ciudad entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Ciudad entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Ciudad entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Ciudad find(Object id) {
        return getEntityManager().find(Ciudad.class, id);
    }

    public List<Ciudad> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ciudad.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Ciudad> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ciudad.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Ciudad> rt = cq.from(Ciudad.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Ciudad> getCiudadDeDepartamento(Departamento d) {
         try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Ciudad> consulta = cb.createQuery(Ciudad.class);
            Root<Ciudad> userW = consulta.from(Ciudad.class);
            consulta.select(userW).where(cb.equal(userW.get("idDepartamento"), d));
            //consulta.orderBy(cb.asc(userW.get("nombre")));
            Query q = getEntityManager().createQuery(consulta);            
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
       
    }
}
