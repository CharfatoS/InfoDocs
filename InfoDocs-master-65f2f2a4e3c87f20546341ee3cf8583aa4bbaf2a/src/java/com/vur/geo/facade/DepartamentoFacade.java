
package com.vur.geo.facade;

import Modelo.Departamento;
import Modelo.Pais;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class DepartamentoFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Departamento entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Departamento entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Departamento entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Departamento find(Object id) {
        return getEntityManager().find(Departamento.class, id);
    }

    public List<Departamento> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Departamento.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Departamento> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Departamento.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Departamento> rt = cq.from(Departamento.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<Departamento> getDepartamentosDePais(Pais p) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Departamento> consulta = cb.createQuery(Departamento.class);
            Root<Departamento> dep = consulta.from(Departamento.class);
            consulta.select(dep)
                    .where(cb.equal(dep.get("idPais"), p)
                    );
            consulta.orderBy(cb.asc(dep.get("nombre")));
            Query q = getEntityManager().createQuery(consulta); 
            System.out.println("getDepartamentosDePais "+p.getNombre());
            return q.getResultList();
        } catch (Exception e) {
             System.out.println("en catch getDepartamentosDePais null");
            return null;
        }
    }
}
