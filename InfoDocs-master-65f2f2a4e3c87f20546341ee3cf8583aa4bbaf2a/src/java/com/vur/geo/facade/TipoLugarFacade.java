
package com.vur.geo.facade;

import Modelo.Tipolugar;
;import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jesusmarin
 */
@Stateless
public class TipoLugarFacade implements Serializable {
    @PersistenceContext
    private EntityManager em;

    private EntityManager getEntityManager() {
        return em;
    }
    
    public void create(Tipolugar entity) {
        getEntityManager().persist(entity);
    }

    public void edit(Tipolugar entity) {
        getEntityManager().merge(entity);
    }

    public void remove(Tipolugar entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public Tipolugar find(Object id) {
        return getEntityManager().find(Tipolugar.class, id);
    }

    public List<Tipolugar> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tipolugar.class));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    public List<Tipolugar> findRange(int[] range) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(Tipolugar.class));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<Tipolugar> rt = cq.from(Tipolugar.class);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
}
