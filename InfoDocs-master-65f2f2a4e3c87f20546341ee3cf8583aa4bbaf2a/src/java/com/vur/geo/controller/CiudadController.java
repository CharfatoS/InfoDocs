package com.vur.geo.controller;

import Modelo.Ciudad;
import com.vur.controller.util.JsfUtil;
import com.vur.controller.util.PaginationHelper;
import Modelo.Departamento;
import Modelo.Pais;
import com.vur.geo.facade.CiudadFacade;
import com.vur.geo.facade.DepartamentoFacade;
import com.vur.geo.facade.LugarFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "ciudadController")
@SessionScoped
public class CiudadController implements Serializable {

    private Ciudad ciudad;
    private Departamento departamento;
    private Pais pais;
    private DataModel items = null;
    @EJB
    private com.vur.geo.facade.CiudadFacade ejbFacade;
    @EJB
    private DepartamentoFacade ejbDepartamento;
    @EJB
    private LugarFacade ejbLugar;
    
    
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public CiudadController() {
    }

    public Ciudad getSelected() {
        if (ciudad == null) {
            ciudad = new Ciudad();
            selectedItemIndex = -1;
        }
        return ciudad;
    }

    private CiudadFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        ciudad = (Ciudad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        ciudad = new Ciudad();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(ciudad);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("CiudadCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        ciudad = (Ciudad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(ciudad);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("CiudadUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        ciudad = (Ciudad) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(ciudad);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/com/vur/properties/vu").getString("CiudadDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/com/vur/properties/vu").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            ciudad = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }
    public SelectItem[] getItemsDepartamentoDePais() {
    try {
            return JsfUtil.getSelectItems(getEjbDepartamento().getDepartamentosDePais(getPais()), true);
        } catch (NullPointerException ex) {
            return null;
        }
    }
    public SelectItem[] getItemsCiudadDeDepartamento() {
        try {
            return JsfUtil.getSelectItems(getEjbFacade().getCiudadDeDepartamento(getDepartamento()), true);
        } catch (NullPointerException ex) {
            return null;
        }

    }
    
     public SelectItem[] getItemsLugarDeCiudad() {
        try {
            return JsfUtil.getSelectItems(getEjbLugar().getLugarDeCiudad(ciudad), true);
        } catch (NullPointerException ex) {
            return null;
        }

    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public LugarFacade getEjbLugar() {
        return ejbLugar;
    }

    
    
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public CiudadFacade getEjbFacade() {
        return ejbFacade;
    }

    public DepartamentoFacade getEjbDepartamento() {
        return ejbDepartamento;
    }
    
    
    

    @FacesConverter(forClass = Ciudad.class)
    public static class CiudadControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CiudadController controller = (CiudadController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ciudadController");
            return controller.ejbFacade.find(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Ciudad) {
                Ciudad o = (Ciudad) object;
                return getStringKey(o.getIdciudad());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Ciudad.class.getName());
            }
        }

    }

}
