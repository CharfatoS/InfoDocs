package com.vur.util;

import Modelo.Users;
import Modelo.Usuario;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 ** tiempo Humano 1 hora 3600 segundos 1 dia	86400 segundos 1 semana	604800
 * segundos 1 mes (30.44 days) 2629743 seconds 1 año (365.24 days) 31556926
 * seconds
 * @author jesus
 */
@ManagedBean(name = "vurUtilBean")
@SessionScoped
public class VurUtil implements Serializable {

    public VurUtil() {
    }

    public Usuario arreglarNombreEnMayuscula(Usuario u) {
        String nombre = u.getNombre();
        nombre = nombre.toUpperCase();
        nombre = nombre.replace(" ", "");
        String otroNombre = u.getOtronombre();
        otroNombre = otroNombre.toUpperCase();
        u.setNombre(nombre);
        u.setOtronombre(otroNombre);
        String apellido = u.getApellido();
        apellido = apellido.toUpperCase();
        apellido = apellido.replaceAll(" ", "");
        String otroApellido = u.getOtroapellido();
        otroApellido = otroApellido.toUpperCase();
        u.setApellido(apellido);
        u.setOtroapellido(otroApellido);
        return u;
    }

    public String arreglarTextoEnMayuscula(String x) {
        x = x.toUpperCase();
        x = x.replaceAll(" ", "");
        return x;
    }

    public long arreglarNumeroString(long x) {
        System.out.println("inicia " + x);
//        x = x.replaceAll(" ", "");
//        x = x.replace(".", "");
//        x = x.replaceAll(",", "");
//        x = x.replaceAll("-", "");
//        x = x.replaceAll("/", "");
//        x = x.replace("*", "");
//        x = x.replace("+", "");
        System.out.println("Finaliza " + x);
        return x;
    }

    public Date verFechaDeLong(long l) {
        return new Date(l * 1000);
    }

    public String verFechaStringDeLong(long l) {
        Date fecha = new Date(l * 1000);
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        return formato.format(fecha);
    }

    public long getLongInicioMes() {
        long f = 0;
        Date h = new Date();
        h.setTime(f);
        return f;
    }

    public long getFechaInicioAnioActual() {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        calendar.set(anio, 0, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        return h;
    }

    public long getFechaFinAnioActual() {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR) + 1;
        calendar.set(anio, 0, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = (date.getTime() / 1000) - 1;
        return h;
    }

    public long getFechaInicioMesActual() {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        return getFechaInicioMes(mes, anio);
    }

    public long getFechaFinMesActual() {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        return getFechaFinMes(mes, anio);
    }

    public long getFechaInicioMes(int mes, int anio) {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        calendar.set(anio, mes, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        return h;
    }

    public long getFechaInicioMes(int mes) {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        calendar.set(anio, mes, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        return h;
    }

    public long getFechaFinMes(int mes, int anio) {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        mes = mes + 1;
        calendar.set(anio, mes, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        h = h - 1;
        return h;
    }

    public long getFechaFinMes(int mes) {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        mes = mes + 1;
        int anio = calendar.get(Calendar.YEAR);
        calendar.set(anio, mes, 1, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        h = h - 1;
        return h;
    }

    public long getFechaInicioDiaActual() {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        return getFechaInicioDia(dia, mes, anio);
    }

    public long getFechaInicioDia(int dia, int mes, int anio) {
        Calendar calendar = new GregorianCalendar();
        Date d = new Date();
        calendar.setTime(d);
        calendar.set(anio, mes, dia, 00, 00, 00);
        Date date = calendar.getTime();
        long h = date.getTime() / 1000;
        return h;
    }

    public long getFechaFinDiaActual() {
        Date d = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(d);
        int anio = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);//Suma 1 para mostrar el siguiente dia
        return getFechaFinDia(dia, mes, anio);
    }

    public long getFechaFinDia(int dia, int mes, int anio) {
        Date d = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(d);
        dia++;//Suma 1 para mostrar el siguiente dia      
        calendar.set(anio, mes, dia, 00, 00, 00);
        Date date = calendar.getTime();
        long h = (date.getTime() / 1000) - 1;//resta 1 segundo para volver al fimal del dia anterior
        return h;
    }

    public String[] getItemMeses() {
        String[] m = {"---", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        return m;
    }

    public String[] getItemFechaReferencia() {
        String[] m = {"vence", "creado"};
        return m;
    }

    public int verIntMes(String mes) {
        try {
            if (mes.equals("---") || mes == null) {
                return 13;
            }
            if (mes.equals("Enero")) {
                return 0;
            }
            if (mes.equals("Febrero")) {
                return 1;
            }
            if (mes.equals("Marzo")) {
                return 2;
            }
            if (mes.equals("Abril")) {
                return 3;
            }
            if (mes.equals("Mayo")) {
                return 4;
            }
            if (mes.equals("Junio")) {
                return 5;
            }
            if (mes.equals("Julio")) {
                return 6;
            }
            if (mes.equals("Agosto")) {
                return 7;
            }
            if (mes.equals("Septiembre")) {
                return 8;
            }
            if (mes.equals("Octubre")) {
                return 9;
            }
            if (mes.equals("Noviembre")) {
                return 10;
            }
            if (mes.equals("Diciembre")) {
                return 11;
            } else {
                return 11;
            }
        } catch (NullPointerException nu) {
            return 11;
        }
    }

    public String ultimoIngreso(Users u) {
        String ulting = "";
        long hoy = System.currentTimeMillis() / 1000;
        long ultimo = (System.currentTimeMillis() / 1000) + 1;
        try {
            ultimo = u.getUltimoacceso();
        } catch (Exception e) {
            ultimo = System.currentTimeMillis() / 1000;
        }
        long tiempo = hoy - ultimo;
        long residuo = 0;
        // System.out.println("en hoy " + hoy + "/" + "en ultimo " + ultimo + " / " + " tiempo " + tiempo);
        if (tiempo > 2629743) {
            // 2629743 mes
            long mes = tiempo / 2629743;
            residuo = tiempo - (2629743 * mes);
            //System.out.println("en residuo = tiempo % 2629743; /tiempo " + tiempo + " / residuo " + residuo + " / " + mes);

            if (mes > 1) {
                ulting = ulting + " " + mes + " meses, ";
            } else {
                ulting = ulting + " " + mes + " mes, ";
            }
            tiempo = residuo;
        }
        if (tiempo > 604800) {
            // 604800 semana
            long sem = tiempo / 604800;
            residuo = tiempo - (604800 * sem);
            //System.out.println("en semana " + tiempo);           
            if (sem > 1) {
                ulting = ulting + " " + sem + " semanas, ";
            } else {
                ulting = ulting + " " + sem + " semana, ";
            }
            tiempo = residuo;
        }
        if (tiempo > 86400) {
            // 86400 dia
            long dia = tiempo / 86400;
            residuo = tiempo - (86400 * dia);
            if (dia > 1) {
                ulting = ulting + " " + dia + " dias, ";
            } else {
                ulting = ulting + " " + dia + " dia, ";
            }
            tiempo = residuo;
        }
        if (tiempo > 3600) {
            // 3600 hora
            long hora = tiempo / 3600;
            residuo = tiempo - (3600 * hora);
            if (hora > 1) {
                ulting = ulting + " " + hora + " horas, ";
            } else {
                ulting = ulting + " " + hora + " hora, ";
            }
            tiempo = residuo;
        }
        if (tiempo > 60) {
            // 60 minuto
            long min = tiempo / 60;
            residuo = tiempo - (60 * min);
            if (tiempo > 1) {
                ulting = ulting + " " + min + " minutos, ";
            } else {
                ulting = ulting + " " + min + " minuto, ";
            }
            tiempo = residuo;
        }
        if (tiempo > 1) {
            ulting = ulting + " " + tiempo + " segundos ";
        } else {
            ulting = ulting + " " + tiempo + " segundo ";
        }
        return "" + ulting;
    }

}
