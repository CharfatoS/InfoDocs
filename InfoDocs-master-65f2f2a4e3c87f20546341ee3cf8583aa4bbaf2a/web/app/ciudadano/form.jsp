<%-- 
    Document   : form
    Created on : 08/03/2011, 05:17:13 PM
    Author     : dsilva
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cargar archivo</title>
        <style type="text/css">
            body {
                background-color:#eff5fa;
                padding: 10px 20px 10px 25px;
                font-family: arial;
                color: #4e6a71;
            }
            .button{
                padding: 10px 26px 10px 26px;
                color: #fff;
                background-color: #018706;
                border: 0px;
                border-radius: 0px 0px;
                -webkit-border-radius: 0px 0px;  /* Safari  */
                -moz-border-radius: 0px 0px;
            }
            .button:hover{
                box-shadow: 2px 2px 2px #003602;
            }
            input{
                border: 0px;
                border-radius: 0px 0px;
            }
        </style>
    </head>
    <body>
        <h1>Cargar archivo</h1>
        <form action="<c:url value='/servlet/upload'/>" method="POST" enctype="multipart/form-data">
            Archivo:&nbsp;&nbsp;
            <input  type="file" name="archivo" value="" /><br/><br/>
            <button id="cargar" class="button" type="submit">Cargar</button>
        </form>
    </body>
</html>
