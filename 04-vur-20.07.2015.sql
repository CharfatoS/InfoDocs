--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-07-20 15:29:38 COT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 249 (class 3079 OID 11935)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 249
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 17593)
-- Name: archivo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE archivo (
    idarchivo integer NOT NULL,
    adjunto character varying(255),
    archivo bytea,
    tipoarchivo character varying(255),
    iddisposicionfinal integer,
    idrespuesta integer,
    idsolicitud integer
);


ALTER TABLE archivo OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 17599)
-- Name: archivo_idarchivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE archivo_idarchivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE archivo_idarchivo_seq OWNER TO postgres;

--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 173
-- Name: archivo_idarchivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE archivo_idarchivo_seq OWNED BY archivo.idarchivo;


--
-- TOC entry 174 (class 1259 OID 17601)
-- Name: asignado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE asignado (
    idasignado integer NOT NULL,
    estado character varying(255),
    fechaasignado timestamp without time zone,
    habilitado boolean,
    idcargo integer,
    idsolicitud integer,
    idempleado bigint
);


ALTER TABLE asignado OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 17604)
-- Name: asignado_idasignado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE asignado_idasignado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE asignado_idasignado_seq OWNER TO postgres;

--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 175
-- Name: asignado_idasignado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE asignado_idasignado_seq OWNED BY asignado.idasignado;


--
-- TOC entry 176 (class 1259 OID 17606)
-- Name: cargo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cargo (
    idcargo integer NOT NULL,
    activo boolean,
    nombre character varying(255),
    iddependencia integer
);


ALTER TABLE cargo OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 17609)
-- Name: cargo_idcargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cargo_idcargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cargo_idcargo_seq OWNER TO postgres;

--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 177
-- Name: cargo_idcargo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cargo_idcargo_seq OWNED BY cargo.idcargo;


--
-- TOC entry 178 (class 1259 OID 17611)
-- Name: categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoria (
    idcategoria integer NOT NULL,
    descripcion text,
    nombre character varying(255)
);


ALTER TABLE categoria OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 17617)
-- Name: categoria_idcategoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoria_idcategoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoria_idcategoria_seq OWNER TO postgres;

--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 179
-- Name: categoria_idcategoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoria_idcategoria_seq OWNED BY categoria.idcategoria;


--
-- TOC entry 180 (class 1259 OID 17619)
-- Name: categoriadocumental; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoriadocumental (
    idcategoriadocumental integer NOT NULL,
    categoria character varying(255),
    idcodigo character varying(255),
    idsecciondocumental integer
);


ALTER TABLE categoriadocumental OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 17625)
-- Name: categoriadocumental_idcategoriadocumental_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoriadocumental_idcategoriadocumental_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoriadocumental_idcategoriadocumental_seq OWNER TO postgres;

--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 181
-- Name: categoriadocumental_idcategoriadocumental_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoriadocumental_idcategoriadocumental_seq OWNED BY categoriadocumental.idcategoriadocumental;


--
-- TOC entry 182 (class 1259 OID 17627)
-- Name: categoriatipodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categoriatipodocumento (
    idcategoriatipodocumento integer NOT NULL,
    idcategoriadocumental integer,
    idtipodocumento integer
);


ALTER TABLE categoriatipodocumento OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 17630)
-- Name: categoriatipodocumento_idcategoriatipodocumento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categoriatipodocumento_idcategoriatipodocumento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categoriatipodocumento_idcategoriatipodocumento_seq OWNER TO postgres;

--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 183
-- Name: categoriatipodocumento_idcategoriatipodocumento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categoriatipodocumento_idcategoriatipodocumento_seq OWNED BY categoriatipodocumento.idcategoriatipodocumento;


--
-- TOC entry 184 (class 1259 OID 17632)
-- Name: ciudad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ciudad (
    idciudad integer NOT NULL,
    codigo character varying(255),
    nombre character varying(255),
    prefijo character varying(255),
    iddepartamento integer
);


ALTER TABLE ciudad OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 17638)
-- Name: ciudad_idciudad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ciudad_idciudad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ciudad_idciudad_seq OWNER TO postgres;

--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 185
-- Name: ciudad_idciudad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ciudad_idciudad_seq OWNED BY ciudad.idciudad;


--
-- TOC entry 186 (class 1259 OID 17640)
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE departamento (
    iddepartamento integer NOT NULL,
    codigo character varying(255),
    nombre character varying(255),
    prefijo character varying(255),
    idpais integer
);


ALTER TABLE departamento OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 17646)
-- Name: departamento_iddepartamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE departamento_iddepartamento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE departamento_iddepartamento_seq OWNER TO postgres;

--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 187
-- Name: departamento_iddepartamento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE departamento_iddepartamento_seq OWNED BY departamento.iddepartamento;


--
-- TOC entry 188 (class 1259 OID 17648)
-- Name: dependencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dependencia (
    iddependencia integer NOT NULL,
    mision text,
    nombre character varying(255),
    idinstitucion integer
);


ALTER TABLE dependencia OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 17654)
-- Name: dependencia_iddependencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dependencia_iddependencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dependencia_iddependencia_seq OWNER TO postgres;

--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 189
-- Name: dependencia_iddependencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dependencia_iddependencia_seq OWNED BY dependencia.iddependencia;


--
-- TOC entry 190 (class 1259 OID 17656)
-- Name: direccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE direccion (
    iddireccion integer NOT NULL,
    barrio character varying(255),
    cel character varying(255),
    direccion character varying(255),
    telcasa character varying(255),
    teloficina character varying(255),
    web character varying(255),
    idciudad integer,
    idtipolugar integer,
    idusuario bigint
);


ALTER TABLE direccion OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 17662)
-- Name: direccion_iddireccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE direccion_iddireccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE direccion_iddireccion_seq OWNER TO postgres;

--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 191
-- Name: direccion_iddireccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE direccion_iddireccion_seq OWNED BY direccion.iddireccion;


--
-- TOC entry 192 (class 1259 OID 17664)
-- Name: disposicionfinal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE disposicionfinal (
    iddisposicionfinal integer NOT NULL,
    disposicion character varying(255)
);


ALTER TABLE disposicionfinal OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 17667)
-- Name: disposicionfinal_iddisposicionfinal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disposicionfinal_iddisposicionfinal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE disposicionfinal_iddisposicionfinal_seq OWNER TO postgres;

--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 193
-- Name: disposicionfinal_iddisposicionfinal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disposicionfinal_iddisposicionfinal_seq OWNED BY disposicionfinal.iddisposicionfinal;


--
-- TOC entry 194 (class 1259 OID 17669)
-- Name: documento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documento (
    iddocumento integer NOT NULL,
    asunto character varying(255),
    codigo character varying(255),
    fechaarchivo bigint,
    idcategoriadocumental integer,
    iddisposicionfinal integer,
    idretencion integer,
    idtipodocumento integer
);


ALTER TABLE documento OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 17675)
-- Name: documento_iddocumento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documento_iddocumento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE documento_iddocumento_seq OWNER TO postgres;

--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 195
-- Name: documento_iddocumento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documento_iddocumento_seq OWNED BY documento.iddocumento;


--
-- TOC entry 196 (class 1259 OID 17677)
-- Name: ejetematico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ejetematico (
    idejetematico integer NOT NULL,
    descripcion text,
    tema character varying(255),
    tiemporespuesta integer,
    idservicio integer
);


ALTER TABLE ejetematico OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17683)
-- Name: ejetematico_idejetematico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ejetematico_idejetematico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ejetematico_idejetematico_seq OWNER TO postgres;

--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 197
-- Name: ejetematico_idejetematico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ejetematico_idejetematico_seq OWNED BY ejetematico.idejetematico;


--
-- TOC entry 198 (class 1259 OID 17685)
-- Name: empleado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empleado (
    idempleado integer NOT NULL,
    activo boolean,
    cesantias character varying(255),
    eps character varying(255),
    fecharegistro text,
    pension character varying(255),
    idcargo integer,
    idestadolaboral integer,
    idusuario bigint
);


ALTER TABLE empleado OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 17691)
-- Name: empleado_idempleado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empleado_idempleado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE empleado_idempleado_seq OWNER TO postgres;

--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 199
-- Name: empleado_idempleado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empleado_idempleado_seq OWNED BY empleado.idempleado;


--
-- TOC entry 200 (class 1259 OID 17693)
-- Name: encuenta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE encuenta (
    idencuenta integer NOT NULL,
    activa boolean,
    descripcion character varying(255),
    nombre character varying(255),
    iddependencia integer,
    idtemaarea integer
);


ALTER TABLE encuenta OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 17699)
-- Name: encuenta_idencuenta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE encuenta_idencuenta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE encuenta_idencuenta_seq OWNER TO postgres;

--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 201
-- Name: encuenta_idencuenta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE encuenta_idencuenta_seq OWNED BY encuenta.idencuenta;


--
-- TOC entry 202 (class 1259 OID 17701)
-- Name: estadolaboral; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estadolaboral (
    idestadolaboral integer NOT NULL,
    descripcion character varying(255),
    estado character varying(255)
);


ALTER TABLE estadolaboral OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 17707)
-- Name: estadolaboral_idestadolaboral_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estadolaboral_idestadolaboral_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estadolaboral_idestadolaboral_seq OWNER TO postgres;

--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 203
-- Name: estadolaboral_idestadolaboral_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estadolaboral_idestadolaboral_seq OWNED BY estadolaboral.idestadolaboral;


--
-- TOC entry 204 (class 1259 OID 17709)
-- Name: estadorequerimiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estadorequerimiento (
    idestadorequerimiento integer NOT NULL,
    estado character varying(255)
);


ALTER TABLE estadorequerimiento OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 17712)
-- Name: estadorequerimiento_idestadorequerimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estadorequerimiento_idestadorequerimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estadorequerimiento_idestadorequerimiento_seq OWNER TO postgres;

--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 205
-- Name: estadorequerimiento_idestadorequerimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estadorequerimiento_idestadorequerimiento_seq OWNED BY estadorequerimiento.idestadorequerimiento;


--
-- TOC entry 206 (class 1259 OID 17714)
-- Name: institucion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE institucion (
    idinstitucion integer NOT NULL,
    caracter character varying(255),
    direccion character varying(255),
    fax character varying(255),
    linkvur character varying(255),
    nombre character varying(255),
    telefono character varying(255),
    tipo character varying(255),
    web character varying(255),
    idciudad integer
);


ALTER TABLE institucion OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 17720)
-- Name: institucion_idinstitucion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE institucion_idinstitucion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE institucion_idinstitucion_seq OWNER TO postgres;

--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 207
-- Name: institucion_idinstitucion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE institucion_idinstitucion_seq OWNED BY institucion.idinstitucion;


--
-- TOC entry 208 (class 1259 OID 17722)
-- Name: link; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE link (
    idlink integer NOT NULL,
    img character varying(255),
    nombre character varying(255),
    url character varying(255)
);


ALTER TABLE link OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 17728)
-- Name: link_idlink_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE link_idlink_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE link_idlink_seq OWNER TO postgres;

--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 209
-- Name: link_idlink_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE link_idlink_seq OWNED BY link.idlink;


--
-- TOC entry 210 (class 1259 OID 17730)
-- Name: log_solicitud; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE log_solicitud (
    idlog_solicitud integer NOT NULL,
    accion character varying(255),
    fecha bigint,
    tablaaccion character varying(255),
    idsolicitud integer,
    idusuario bigint
);


ALTER TABLE log_solicitud OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 17736)
-- Name: log_solicitud_idlog_solicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE log_solicitud_idlog_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE log_solicitud_idlog_solicitud_seq OWNER TO postgres;

--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 211
-- Name: log_solicitud_idlog_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE log_solicitud_idlog_solicitud_seq OWNED BY log_solicitud.idlog_solicitud;


--
-- TOC entry 212 (class 1259 OID 17738)
-- Name: pais; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pais (
    idpais integer NOT NULL,
    codigo character varying(255),
    nombre character varying(255),
    prefijo character varying(255)
);


ALTER TABLE pais OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 17744)
-- Name: pais_idpais_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pais_idpais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pais_idpais_seq OWNER TO postgres;

--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 213
-- Name: pais_idpais_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pais_idpais_seq OWNED BY pais.idpais;


--
-- TOC entry 214 (class 1259 OID 17746)
-- Name: preferencias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE preferencias (
    idpreferencias integer NOT NULL,
    css character varying(255),
    nombre character varying(255),
    idusuario bigint
);


ALTER TABLE preferencias OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 17752)
-- Name: preferencias_idpreferencias_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE preferencias_idpreferencias_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE preferencias_idpreferencias_seq OWNER TO postgres;

--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 215
-- Name: preferencias_idpreferencias_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE preferencias_idpreferencias_seq OWNED BY preferencias.idpreferencias;


--
-- TOC entry 216 (class 1259 OID 17754)
-- Name: pregunta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pregunta (
    idpregunta integer NOT NULL,
    numrespuestas integer,
    pregunta character varying(255),
    idencuenta integer,
    idtiporespuesta integer
);


ALTER TABLE pregunta OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 17757)
-- Name: pregunta_idpregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pregunta_idpregunta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pregunta_idpregunta_seq OWNER TO postgres;

--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 217
-- Name: pregunta_idpregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pregunta_idpregunta_seq OWNED BY pregunta.idpregunta;


--
-- TOC entry 218 (class 1259 OID 17759)
-- Name: preguntafrecuente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE preguntafrecuente (
    idpregunafrecuente integer NOT NULL,
    activo character varying(255),
    descripcion character varying(255),
    pregunta character varying(255),
    respuesta text,
    idservicio integer,
    idtemaarea integer
);


ALTER TABLE preguntafrecuente OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 17765)
-- Name: preguntafrecuente_idpregunafrecuente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE preguntafrecuente_idpregunafrecuente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE preguntafrecuente_idpregunafrecuente_seq OWNER TO postgres;

--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 219
-- Name: preguntafrecuente_idpregunafrecuente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE preguntafrecuente_idpregunafrecuente_seq OWNED BY preguntafrecuente.idpregunafrecuente;


--
-- TOC entry 220 (class 1259 OID 17767)
-- Name: respuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE respuesta (
    idrespuesta integer NOT NULL,
    respuesta text,
    sugerencia text,
    tiemporespuesta integer,
    idcargo integer,
    idsolicitud integer,
    fecha bigint,
    idusuario bigint
);


ALTER TABLE respuesta OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 22757)
-- Name: respuestaFAQ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "respuestaFAQ" (
    idrespuestafaq integer NOT NULL,
    correcta boolean,
    numerica integer,
    texto text,
    idpregunta integer,
    idusuario bigint
);


ALTER TABLE "respuestaFAQ" OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 17773)
-- Name: respuesta_idrespuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE respuesta_idrespuesta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE respuesta_idrespuesta_seq OWNER TO postgres;

--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 221
-- Name: respuesta_idrespuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE respuesta_idrespuesta_seq OWNED BY respuesta.idrespuesta;


--
-- TOC entry 222 (class 1259 OID 17775)
-- Name: retencion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retencion (
    idretencion integer NOT NULL,
    retencion character varying(255)
);


ALTER TABLE retencion OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 17778)
-- Name: retencion_idretencion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retencion_idretencion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE retencion_idretencion_seq OWNER TO postgres;

--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 223
-- Name: retencion_idretencion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE retencion_idretencion_seq OWNED BY retencion.idretencion;


--
-- TOC entry 224 (class 1259 OID 17780)
-- Name: rotulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE rotulo (
    idrotulo integer NOT NULL,
    barras character varying(255),
    codigo character varying(255),
    fecha text,
    rotulo character varying(255),
    idsolicitud integer
);


ALTER TABLE rotulo OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 17786)
-- Name: rotulo_idrotulo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rotulo_idrotulo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rotulo_idrotulo_seq OWNER TO postgres;

--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 225
-- Name: rotulo_idrotulo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE rotulo_idrotulo_seq OWNED BY rotulo.idrotulo;


--
-- TOC entry 226 (class 1259 OID 17788)
-- Name: secciondocumental; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE secciondocumental (
    idsecciondocumental integer NOT NULL,
    codigo character varying(255),
    procedimiento character varying(255),
    seccion character varying(255)
);


ALTER TABLE secciondocumental OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 17794)
-- Name: secciondocumental_idsecciondocumental_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE secciondocumental_idsecciondocumental_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE secciondocumental_idsecciondocumental_seq OWNER TO postgres;

--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 227
-- Name: secciondocumental_idsecciondocumental_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE secciondocumental_idsecciondocumental_seq OWNED BY secciondocumental.idsecciondocumental;


--
-- TOC entry 228 (class 1259 OID 17796)
-- Name: servicio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE servicio (
    idservicio integer NOT NULL,
    nombre character varying(255),
    obligatorio character varying(255),
    tiemporespuesta integer,
    idcategoria integer,
    iddependencia integer
);


ALTER TABLE servicio OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 17802)
-- Name: servicio_idservicio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE servicio_idservicio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE servicio_idservicio_seq OWNER TO postgres;

--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 229
-- Name: servicio_idservicio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE servicio_idservicio_seq OWNED BY servicio.idservicio;


--
-- TOC entry 230 (class 1259 OID 17804)
-- Name: solicitud; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE solicitud (
    idsolicitud integer NOT NULL,
    concepto character varying(255),
    creado bigint,
    descripcion text,
    vence bigint,
    idejetematico integer,
    idestadorequerimiento integer,
    idtiporequerimiento integer,
    idtiposolicutd integer,
    idusuario bigint
);


ALTER TABLE solicitud OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 17810)
-- Name: solicitud_idsolicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE solicitud_idsolicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE solicitud_idsolicitud_seq OWNER TO postgres;

--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 231
-- Name: solicitud_idsolicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE solicitud_idsolicitud_seq OWNED BY solicitud.idsolicitud;


--
-- TOC entry 232 (class 1259 OID 17812)
-- Name: temaarea; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE temaarea (
    idtemaarea integer NOT NULL,
    activo boolean,
    nombre character varying(255)
);


ALTER TABLE temaarea OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 17815)
-- Name: temaarea_idtemaarea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE temaarea_idtemaarea_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE temaarea_idtemaarea_seq OWNER TO postgres;

--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 233
-- Name: temaarea_idtemaarea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE temaarea_idtemaarea_seq OWNED BY temaarea.idtemaarea;


--
-- TOC entry 234 (class 1259 OID 17817)
-- Name: tipodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipodocumento (
    idtipodocumento integer NOT NULL,
    tipodoc character varying(255)
);


ALTER TABLE tipodocumento OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 17820)
-- Name: tipodocumento_idtipodocumento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipodocumento_idtipodocumento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipodocumento_idtipodocumento_seq OWNER TO postgres;

--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 235
-- Name: tipodocumento_idtipodocumento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipodocumento_idtipodocumento_seq OWNED BY tipodocumento.idtipodocumento;


--
-- TOC entry 236 (class 1259 OID 17822)
-- Name: tipolugar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipolugar (
    idtipolugar integer NOT NULL,
    cod character varying(255),
    nombre character varying(255)
);


ALTER TABLE tipolugar OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 17828)
-- Name: tipolugar_idtipolugar_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipolugar_idtipolugar_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipolugar_idtipolugar_seq OWNER TO postgres;

--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 237
-- Name: tipolugar_idtipolugar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipolugar_idtipolugar_seq OWNED BY tipolugar.idtipolugar;


--
-- TOC entry 238 (class 1259 OID 17830)
-- Name: tiporequerimiento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiporequerimiento (
    idtiporequerimiento integer NOT NULL,
    tipo character varying(255)
);


ALTER TABLE tiporequerimiento OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 17833)
-- Name: tiporequerimiento_idtiporequerimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tiporequerimiento_idtiporequerimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tiporequerimiento_idtiporequerimiento_seq OWNER TO postgres;

--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 239
-- Name: tiporequerimiento_idtiporequerimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tiporequerimiento_idtiporequerimiento_seq OWNED BY tiporequerimiento.idtiporequerimiento;


--
-- TOC entry 240 (class 1259 OID 17835)
-- Name: tiporespuesta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiporespuesta (
    idtiporespuesta integer NOT NULL,
    tipo character varying(255)
);


ALTER TABLE tiporespuesta OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 17838)
-- Name: tiporespuesta_idtiporespuesta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tiporespuesta_idtiporespuesta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tiporespuesta_idtiporespuesta_seq OWNER TO postgres;

--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 241
-- Name: tiporespuesta_idtiporespuesta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tiporespuesta_idtiporespuesta_seq OWNED BY tiporespuesta.idtiporespuesta;


--
-- TOC entry 242 (class 1259 OID 17840)
-- Name: tiposolicitud; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiposolicitud (
    idtiposolicitud integer NOT NULL,
    tipo character varying(255)
);


ALTER TABLE tiposolicitud OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 17843)
-- Name: tiposolicitud_idtiposolicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tiposolicitud_idtiposolicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tiposolicitud_idtiposolicitud_seq OWNER TO postgres;

--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 243
-- Name: tiposolicitud_idtiposolicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tiposolicitud_idtiposolicitud_seq OWNED BY tiposolicitud.idtiposolicitud;


--
-- TOC entry 244 (class 1259 OID 17845)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    idusers integer NOT NULL,
    mail character varying(255),
    numaccesos integer,
    password character varying(255),
    rol character varying(255),
    username character varying(255),
    idusuario bigint,
    ultimoacceso bigint
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 17851)
-- Name: users_idusers_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_idusers_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_idusers_seq OWNER TO postgres;

--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 245
-- Name: users_idusers_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_idusers_seq OWNED BY users.idusers;


--
-- TOC entry 246 (class 1259 OID 17853)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    idusuario integer NOT NULL,
    apellido character varying(255),
    docid character varying(255),
    fechanacimiento character varying(255),
    grupo character varying(255),
    nombre character varying(255),
    otroapellido character varying(255),
    otronombre character varying(255),
    tipodoc character varying(255)
);


ALTER TABLE usuario OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 17859)
-- Name: usuario_idusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_idusuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuario_idusuario_seq OWNER TO postgres;

--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 247
-- Name: usuario_idusuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_idusuario_seq OWNED BY usuario.idusuario;


--
-- TOC entry 2212 (class 2604 OID 17861)
-- Name: idarchivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY archivo ALTER COLUMN idarchivo SET DEFAULT nextval('archivo_idarchivo_seq'::regclass);


--
-- TOC entry 2213 (class 2604 OID 17862)
-- Name: idasignado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asignado ALTER COLUMN idasignado SET DEFAULT nextval('asignado_idasignado_seq'::regclass);


--
-- TOC entry 2214 (class 2604 OID 17863)
-- Name: idcargo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargo ALTER COLUMN idcargo SET DEFAULT nextval('cargo_idcargo_seq'::regclass);


--
-- TOC entry 2215 (class 2604 OID 17864)
-- Name: idcategoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoria ALTER COLUMN idcategoria SET DEFAULT nextval('categoria_idcategoria_seq'::regclass);


--
-- TOC entry 2216 (class 2604 OID 17865)
-- Name: idcategoriadocumental; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoriadocumental ALTER COLUMN idcategoriadocumental SET DEFAULT nextval('categoriadocumental_idcategoriadocumental_seq'::regclass);


--
-- TOC entry 2217 (class 2604 OID 17866)
-- Name: idcategoriatipodocumento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoriatipodocumento ALTER COLUMN idcategoriatipodocumento SET DEFAULT nextval('categoriatipodocumento_idcategoriatipodocumento_seq'::regclass);


--
-- TOC entry 2218 (class 2604 OID 17867)
-- Name: idciudad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ciudad ALTER COLUMN idciudad SET DEFAULT nextval('ciudad_idciudad_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 17868)
-- Name: iddepartamento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY departamento ALTER COLUMN iddepartamento SET DEFAULT nextval('departamento_iddepartamento_seq'::regclass);


--
-- TOC entry 2220 (class 2604 OID 17869)
-- Name: iddependencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dependencia ALTER COLUMN iddependencia SET DEFAULT nextval('dependencia_iddependencia_seq'::regclass);


--
-- TOC entry 2221 (class 2604 OID 17870)
-- Name: iddireccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion ALTER COLUMN iddireccion SET DEFAULT nextval('direccion_iddireccion_seq'::regclass);


--
-- TOC entry 2222 (class 2604 OID 17871)
-- Name: iddisposicionfinal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disposicionfinal ALTER COLUMN iddisposicionfinal SET DEFAULT nextval('disposicionfinal_iddisposicionfinal_seq'::regclass);


--
-- TOC entry 2223 (class 2604 OID 17872)
-- Name: iddocumento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento ALTER COLUMN iddocumento SET DEFAULT nextval('documento_iddocumento_seq'::regclass);


--
-- TOC entry 2224 (class 2604 OID 17873)
-- Name: idejetematico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ejetematico ALTER COLUMN idejetematico SET DEFAULT nextval('ejetematico_idejetematico_seq'::regclass);


--
-- TOC entry 2225 (class 2604 OID 17874)
-- Name: idempleado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado ALTER COLUMN idempleado SET DEFAULT nextval('empleado_idempleado_seq'::regclass);


--
-- TOC entry 2226 (class 2604 OID 17875)
-- Name: idencuenta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuenta ALTER COLUMN idencuenta SET DEFAULT nextval('encuenta_idencuenta_seq'::regclass);


--
-- TOC entry 2227 (class 2604 OID 17876)
-- Name: idestadolaboral; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estadolaboral ALTER COLUMN idestadolaboral SET DEFAULT nextval('estadolaboral_idestadolaboral_seq'::regclass);


--
-- TOC entry 2228 (class 2604 OID 17877)
-- Name: idestadorequerimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estadorequerimiento ALTER COLUMN idestadorequerimiento SET DEFAULT nextval('estadorequerimiento_idestadorequerimiento_seq'::regclass);


--
-- TOC entry 2229 (class 2604 OID 17878)
-- Name: idinstitucion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institucion ALTER COLUMN idinstitucion SET DEFAULT nextval('institucion_idinstitucion_seq'::regclass);


--
-- TOC entry 2230 (class 2604 OID 17879)
-- Name: idlink; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY link ALTER COLUMN idlink SET DEFAULT nextval('link_idlink_seq'::regclass);


--
-- TOC entry 2231 (class 2604 OID 17880)
-- Name: idlog_solicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_solicitud ALTER COLUMN idlog_solicitud SET DEFAULT nextval('log_solicitud_idlog_solicitud_seq'::regclass);


--
-- TOC entry 2232 (class 2604 OID 17881)
-- Name: idpais; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pais ALTER COLUMN idpais SET DEFAULT nextval('pais_idpais_seq'::regclass);


--
-- TOC entry 2233 (class 2604 OID 17882)
-- Name: idpreferencias; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preferencias ALTER COLUMN idpreferencias SET DEFAULT nextval('preferencias_idpreferencias_seq'::regclass);


--
-- TOC entry 2234 (class 2604 OID 17883)
-- Name: idpregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta ALTER COLUMN idpregunta SET DEFAULT nextval('pregunta_idpregunta_seq'::regclass);


--
-- TOC entry 2235 (class 2604 OID 17884)
-- Name: idpregunafrecuente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preguntafrecuente ALTER COLUMN idpregunafrecuente SET DEFAULT nextval('preguntafrecuente_idpregunafrecuente_seq'::regclass);


--
-- TOC entry 2236 (class 2604 OID 17885)
-- Name: idrespuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta ALTER COLUMN idrespuesta SET DEFAULT nextval('respuesta_idrespuesta_seq'::regclass);


--
-- TOC entry 2237 (class 2604 OID 17886)
-- Name: idretencion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY retencion ALTER COLUMN idretencion SET DEFAULT nextval('retencion_idretencion_seq'::regclass);


--
-- TOC entry 2238 (class 2604 OID 17887)
-- Name: idrotulo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rotulo ALTER COLUMN idrotulo SET DEFAULT nextval('rotulo_idrotulo_seq'::regclass);


--
-- TOC entry 2239 (class 2604 OID 17888)
-- Name: idsecciondocumental; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secciondocumental ALTER COLUMN idsecciondocumental SET DEFAULT nextval('secciondocumental_idsecciondocumental_seq'::regclass);


--
-- TOC entry 2240 (class 2604 OID 17889)
-- Name: idservicio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY servicio ALTER COLUMN idservicio SET DEFAULT nextval('servicio_idservicio_seq'::regclass);


--
-- TOC entry 2241 (class 2604 OID 17890)
-- Name: idsolicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud ALTER COLUMN idsolicitud SET DEFAULT nextval('solicitud_idsolicitud_seq'::regclass);


--
-- TOC entry 2242 (class 2604 OID 17891)
-- Name: idtemaarea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY temaarea ALTER COLUMN idtemaarea SET DEFAULT nextval('temaarea_idtemaarea_seq'::regclass);


--
-- TOC entry 2243 (class 2604 OID 17892)
-- Name: idtipodocumento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipodocumento ALTER COLUMN idtipodocumento SET DEFAULT nextval('tipodocumento_idtipodocumento_seq'::regclass);


--
-- TOC entry 2244 (class 2604 OID 17893)
-- Name: idtipolugar; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipolugar ALTER COLUMN idtipolugar SET DEFAULT nextval('tipolugar_idtipolugar_seq'::regclass);


--
-- TOC entry 2245 (class 2604 OID 17894)
-- Name: idtiporequerimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiporequerimiento ALTER COLUMN idtiporequerimiento SET DEFAULT nextval('tiporequerimiento_idtiporequerimiento_seq'::regclass);


--
-- TOC entry 2246 (class 2604 OID 17895)
-- Name: idtiporespuesta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiporespuesta ALTER COLUMN idtiporespuesta SET DEFAULT nextval('tiporespuesta_idtiporespuesta_seq'::regclass);


--
-- TOC entry 2247 (class 2604 OID 17896)
-- Name: idtiposolicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiposolicitud ALTER COLUMN idtiposolicitud SET DEFAULT nextval('tiposolicitud_idtiposolicitud_seq'::regclass);


--
-- TOC entry 2248 (class 2604 OID 17897)
-- Name: idusers; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN idusers SET DEFAULT nextval('users_idusers_seq'::regclass);


--
-- TOC entry 2249 (class 2604 OID 17898)
-- Name: idusuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN idusuario SET DEFAULT nextval('usuario_idusuario_seq'::regclass);


--
-- TOC entry 2491 (class 0 OID 17593)
-- Dependencies: 172
-- Data for Name: archivo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 173
-- Name: archivo_idarchivo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('archivo_idarchivo_seq', 1, false);


--
-- TOC entry 2493 (class 0 OID 17601)
-- Dependencies: 174
-- Data for Name: asignado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO asignado (idasignado, estado, fechaasignado, habilitado, idcargo, idsolicitud, idempleado) VALUES (2, 'Abierto', '2015-05-05 00:00:00', true, 4, 4, 1);
INSERT INTO asignado (idasignado, estado, fechaasignado, habilitado, idcargo, idsolicitud, idempleado) VALUES (1, 'Abierto', '2015-07-05 00:00:00', true, 4, 5, 2);


--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 175
-- Name: asignado_idasignado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('asignado_idasignado_seq', 1, false);


--
-- TOC entry 2495 (class 0 OID 17606)
-- Dependencies: 176
-- Data for Name: cargo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (2, true, 'Profesional Castastro', 1);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (1, true, 'Secretaria', 1);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (3, true, 'Secretaria', 2);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (4, true, 'Profesional Agronomo', 2);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (5, true, 'Secretaria', 3);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (6, true, 'Profesional Contable', 3);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (7, true, 'Asistente de Salud', 4);
INSERT INTO cargo (idcargo, activo, nombre, iddependencia) VALUES (8, true, 'Medico Veterinario', 4);


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 177
-- Name: cargo_idcargo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cargo_idcargo_seq', 1, false);


--
-- TOC entry 2497 (class 0 OID 17611)
-- Dependencies: 178
-- Data for Name: categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO categoria (idcategoria, descripcion, nombre) VALUES (1, NULL, 'salud');
INSERT INTO categoria (idcategoria, descripcion, nombre) VALUES (2, NULL, 'Predial');
INSERT INTO categoria (idcategoria, descripcion, nombre) VALUES (3, NULL, 'Campo');
INSERT INTO categoria (idcategoria, descripcion, nombre) VALUES (4, NULL, 'Social');


--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 179
-- Name: categoria_idcategoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoria_idcategoria_seq', 1, false);


--
-- TOC entry 2499 (class 0 OID 17619)
-- Dependencies: 180
-- Data for Name: categoriadocumental; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 181
-- Name: categoriadocumental_idcategoriadocumental_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoriadocumental_idcategoriadocumental_seq', 1, false);


--
-- TOC entry 2501 (class 0 OID 17627)
-- Dependencies: 182
-- Data for Name: categoriatipodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 183
-- Name: categoriatipodocumento_idcategoriatipodocumento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categoriatipodocumento_idcategoriatipodocumento_seq', 1, false);


--
-- TOC entry 2503 (class 0 OID 17632)
-- Dependencies: 184
-- Data for Name: ciudad; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ciudad (idciudad, codigo, nombre, prefijo, iddepartamento) VALUES (1, '011', 'Florencia', 'Flo', 1);
INSERT INTO ciudad (idciudad, codigo, nombre, prefijo, iddepartamento) VALUES (2, '021', 'Neiva', 'Nei', 2);
INSERT INTO ciudad (idciudad, codigo, nombre, prefijo, iddepartamento) VALUES (3, '031', 'Mocoa', 'Moc', 3);


--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 185
-- Name: ciudad_idciudad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ciudad_idciudad_seq', 1, false);


--
-- TOC entry 2505 (class 0 OID 17640)
-- Dependencies: 186
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO departamento (iddepartamento, codigo, nombre, prefijo, idpais) VALUES (1, '1', 'Caquetá', 'ca', 1);
INSERT INTO departamento (iddepartamento, codigo, nombre, prefijo, idpais) VALUES (2, '2', 'Huila', 'hu', 1);
INSERT INTO departamento (iddepartamento, codigo, nombre, prefijo, idpais) VALUES (3, '3', 'Putumayo', 'pu', 1);


--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 187
-- Name: departamento_iddepartamento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('departamento_iddepartamento_seq', 2, true);


--
-- TOC entry 2507 (class 0 OID 17648)
-- Dependencies: 188
-- Data for Name: dependencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dependencia (iddependencia, mision, nombre, idinstitucion) VALUES (1, NULL, 'Planeacion', 1);
INSERT INTO dependencia (iddependencia, mision, nombre, idinstitucion) VALUES (2, NULL, 'Secretaria de Agricultura', 1);
INSERT INTO dependencia (iddependencia, mision, nombre, idinstitucion) VALUES (3, NULL, 'Secretaria de Gobierno', 1);
INSERT INTO dependencia (iddependencia, mision, nombre, idinstitucion) VALUES (4, NULL, 'Secretaria de Salud', 1);


--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 189
-- Name: dependencia_iddependencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dependencia_iddependencia_seq', 1, false);


--
-- TOC entry 2509 (class 0 OID 17656)
-- Dependencies: 190
-- Data for Name: direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (1, NULL, '3218529632', 'las cachas', '432', '656598', 'www.mipagina.com', 3, 3, 11);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (2, NULL, '3203203214', 'la cazuca', '4326598', '43569854', 'www.mipagina.com', 2, 3, 12);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (3, NULL, '3203203214', 'Calle 45 N 12-12', '4326598', '4359898', 'www.mipagina.com', 2, 3, 13);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (4, NULL, '3114568965', 'Calle 45 N 12-12', '4355555', '4359898', 'www.mipagina.com', 2, 3, 14);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (5, NULL, '3162523654', 'norte', '4326598', '43569854', 'www.mipagina.com', 2, 2, 15);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (6, NULL, '', '', '', '', 'www.mipagina.com', NULL, NULL, 16);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (7, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 17);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (8, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 18);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (9, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 19);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (10, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 20);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (11, 'acolsure', '3183271707', 'calle 28 n19-25', '', '', '', NULL, NULL, 21);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (12, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 22);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (13, 'caceres', '3201478523', 'calle 12  12-12', '43656987', '43565698', 'www.mipagina.com', NULL, NULL, 23);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (14, 'carcara', '3202556854', 'calle 12 12/12', '4325698', '4326598', '', NULL, NULL, 24);
INSERT INTO direccion (iddireccion, barrio, cel, direccion, telcasa, teloficina, web, idciudad, idtipolugar, idusuario) VALUES (15, 'carcara', '3202556854', 'calle 12 12/12', '4325698', '4326598', '', NULL, NULL, 25);


--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 191
-- Name: direccion_iddireccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('direccion_iddireccion_seq', 15, true);


--
-- TOC entry 2511 (class 0 OID 17664)
-- Dependencies: 192
-- Data for Name: disposicionfinal; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 193
-- Name: disposicionfinal_iddisposicionfinal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('disposicionfinal_iddisposicionfinal_seq', 1, false);


--
-- TOC entry 2513 (class 0 OID 17669)
-- Dependencies: 194
-- Data for Name: documento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 195
-- Name: documento_iddocumento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('documento_iddocumento_seq', 1, false);


--
-- TOC entry 2515 (class 0 OID 17677)
-- Dependencies: 196
-- Data for Name: ejetematico; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ejetematico (idejetematico, descripcion, tema, tiemporespuesta, idservicio) VALUES (2, 'Apoyo Economico', 'Subsidio', 6, 2);
INSERT INTO ejetematico (idejetematico, descripcion, tema, tiemporespuesta, idservicio) VALUES (3, 'Formacion pecuaria', 'Formacion pecuaria', 5, 3);
INSERT INTO ejetematico (idejetematico, descripcion, tema, tiemporespuesta, idservicio) VALUES (4, 'Impuestos', 'Impuestos', 6, 4);
INSERT INTO ejetematico (idejetematico, descripcion, tema, tiemporespuesta, idservicio) VALUES (1, 'solicitudes  para fumigacion', 'Control Mosquitos', 10, 1);


--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 197
-- Name: ejetematico_idejetematico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ejetematico_idejetematico_seq', 1, false);


--
-- TOC entry 2517 (class 0 OID 17685)
-- Dependencies: 198
-- Data for Name: empleado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO empleado (idempleado, activo, cesantias, eps, fecharegistro, pension, idcargo, idestadolaboral, idusuario) VALUES (1, true, 'Provenir', 'Coomeva', '09/07/2015', 'Porvenir', 2, 2, 2);
INSERT INTO empleado (idempleado, activo, cesantias, eps, fecharegistro, pension, idcargo, idestadolaboral, idusuario) VALUES (2, true, 'Porvenir', 'Coomeva', '14/07/2015', 'Porvenir', 4, 2, 3);
INSERT INTO empleado (idempleado, activo, cesantias, eps, fecharegistro, pension, idcargo, idestadolaboral, idusuario) VALUES (3, true, 'Porvenir', 'Coomeva', '14/07/2015', 'Porvenir', 5, 2, 4);


--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 199
-- Name: empleado_idempleado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empleado_idempleado_seq', 1, false);


--
-- TOC entry 2519 (class 0 OID 17693)
-- Dependencies: 200
-- Data for Name: encuenta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 201
-- Name: encuenta_idencuenta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('encuenta_idencuenta_seq', 1, false);


--
-- TOC entry 2521 (class 0 OID 17701)
-- Dependencies: 202
-- Data for Name: estadolaboral; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estadolaboral (idestadolaboral, descripcion, estado) VALUES (3, 'Ausente', 'Ausente');
INSERT INTO estadolaboral (idestadolaboral, descripcion, estado) VALUES (4, 'Otro', 'Otro');
INSERT INTO estadolaboral (idestadolaboral, descripcion, estado) VALUES (1, 'inactivo', 'inactivo');
INSERT INTO estadolaboral (idestadolaboral, descripcion, estado) VALUES (2, 'Activo', 'Activo');


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 203
-- Name: estadolaboral_idestadolaboral_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estadolaboral_idestadolaboral_seq', 1, false);


--
-- TOC entry 2523 (class 0 OID 17709)
-- Dependencies: 204
-- Data for Name: estadorequerimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estadorequerimiento (idestadorequerimiento, estado) VALUES (2, 'Asignado');
INSERT INTO estadorequerimiento (idestadorequerimiento, estado) VALUES (3, 'En Tramite');
INSERT INTO estadorequerimiento (idestadorequerimiento, estado) VALUES (1, 'Radicado');
INSERT INTO estadorequerimiento (idestadorequerimiento, estado) VALUES (4, 'Resuelto');


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 205
-- Name: estadorequerimiento_idestadorequerimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estadorequerimiento_idestadorequerimiento_seq', 1, false);


--
-- TOC entry 2525 (class 0 OID 17714)
-- Dependencies: 206
-- Data for Name: institucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO institucion (idinstitucion, caracter, direccion, fax, linkvur, nombre, telefono, tipo, web, idciudad) VALUES (1, 'Departamental', 'Centro', '4356565', 'localhost/vurSuma', 'Gobernacion del Caqueta', '4358989', 'Publica', 'wwwcaqueta.gov.co', NULL);


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 207
-- Name: institucion_idinstitucion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('institucion_idinstitucion_seq', 1, false);


--
-- TOC entry 2527 (class 0 OID 17722)
-- Dependencies: 208
-- Data for Name: link; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 209
-- Name: link_idlink_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('link_idlink_seq', 1, false);


--
-- TOC entry 2529 (class 0 OID 17730)
-- Dependencies: 210
-- Data for Name: log_solicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 211
-- Name: log_solicitud_idlog_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('log_solicitud_idlog_solicitud_seq', 1, false);


--
-- TOC entry 2531 (class 0 OID 17738)
-- Dependencies: 212
-- Data for Name: pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO pais (idpais, codigo, nombre, prefijo) VALUES (1, '1', 'COLOMBIA', 'COL');


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 213
-- Name: pais_idpais_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pais_idpais_seq', 1, false);


--
-- TOC entry 2533 (class 0 OID 17746)
-- Dependencies: 214
-- Data for Name: preferencias; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 215
-- Name: preferencias_idpreferencias_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('preferencias_idpreferencias_seq', 1, false);


--
-- TOC entry 2535 (class 0 OID 17754)
-- Dependencies: 216
-- Data for Name: pregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 217
-- Name: pregunta_idpregunta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pregunta_idpregunta_seq', 1, false);


--
-- TOC entry 2537 (class 0 OID 17759)
-- Dependencies: 218
-- Data for Name: preguntafrecuente; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 219
-- Name: preguntafrecuente_idpregunafrecuente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('preguntafrecuente_idpregunafrecuente_seq', 1, false);


--
-- TOC entry 2539 (class 0 OID 17767)
-- Dependencies: 220
-- Data for Name: respuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO respuesta (idrespuesta, respuesta, sugerencia, tiemporespuesta, idcargo, idsolicitud, fecha, idusuario) VALUES (1, 'Quiero responderle a guillermo prontamente', 'le sugiero que lea la respuesta completamente ya editada', 518400, NULL, 5, 1436567760, 2);


--
-- TOC entry 2567 (class 0 OID 22757)
-- Dependencies: 248
-- Data for Name: respuestaFAQ; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 221
-- Name: respuesta_idrespuesta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('respuesta_idrespuesta_seq', 1, true);


--
-- TOC entry 2541 (class 0 OID 17775)
-- Dependencies: 222
-- Data for Name: retencion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 223
-- Name: retencion_idretencion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('retencion_idretencion_seq', 1, false);


--
-- TOC entry 2543 (class 0 OID 17780)
-- Dependencies: 224
-- Data for Name: rotulo; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 225
-- Name: rotulo_idrotulo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('rotulo_idrotulo_seq', 1, false);


--
-- TOC entry 2545 (class 0 OID 17788)
-- Dependencies: 226
-- Data for Name: secciondocumental; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 227
-- Name: secciondocumental_idsecciondocumental_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('secciondocumental_idsecciondocumental_seq', 1, false);


--
-- TOC entry 2547 (class 0 OID 17796)
-- Dependencies: 228
-- Data for Name: servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO servicio (idservicio, nombre, obligatorio, tiemporespuesta, idcategoria, iddependencia) VALUES (3, 'Capacitacion  manejo ganadero', NULL, 5, 3, 2);
INSERT INTO servicio (idservicio, nombre, obligatorio, tiemporespuesta, idcategoria, iddependencia) VALUES (1, 'Visita Fumigacion', NULL, 10, 1, 2);
INSERT INTO servicio (idservicio, nombre, obligatorio, tiemporespuesta, idcategoria, iddependencia) VALUES (2, 'Subsidio alimentario', NULL, 8, 4, 2);
INSERT INTO servicio (idservicio, nombre, obligatorio, tiemporespuesta, idcategoria, iddependencia) VALUES (4, 'Impuestos', NULL, 6, 2, 2);
INSERT INTO servicio (idservicio, nombre, obligatorio, tiemporespuesta, idcategoria, iddependencia) VALUES (5, 'Visitas domiciliarias Predial', NULL, 10, 2, 2);


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 229
-- Name: servicio_idservicio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('servicio_idservicio_seq', 1, false);


--
-- TOC entry 2549 (class 0 OID 17804)
-- Dependencies: 230
-- Data for Name: solicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (1, 'Nueva Solicitud', 1435716910, 'Nueva Solicitud para salud', 1436580910, 1, 1, 1, 2, 1);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (2, 'Peticion Primera de zancudos', 1436461878, 'Es solo una prueba de peticion a nombre de carmen', 1437325878, 1, 1, 3, 2, 2);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (3, 'Solcitid de Poor', 1436463315, 'Es una prueba de doliitud de porrongolo', 1436981715, 2, 1, 4, 2, 2);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (4, 'Solicitud de lola', 1436464131, 'Es una prueba para lola', 1436982531, 4, 1, 4, 2, 15);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (5, 'Solcitud de Guiilermo', 1436473776, 'Guillermo quiere contrao den tro de la gobernacion', 1436992176, 2, 1, 3, 2, 16);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (6, 'Solicitud 1', 1436490813, 'Pruebna de dsolicitudfd', 1437354813, 1, 1, 6, 2, 1);
INSERT INTO solicitud (idsolicitud, concepto, creado, descripcion, vence, idejetematico, idestadorequerimiento, idtiporequerimiento, idtiposolicutd, idusuario) VALUES (7, 'solicitud de don roque', 1436899874, 'solicitud de don roque par ael contorl de mosquitos', 1437763874, 1, 1, 1, 2, 17);


--
-- TOC entry 2643 (class 0 OID 0)
-- Dependencies: 231
-- Name: solicitud_idsolicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('solicitud_idsolicitud_seq', 7, true);


--
-- TOC entry 2551 (class 0 OID 17812)
-- Dependencies: 232
-- Data for Name: temaarea; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2644 (class 0 OID 0)
-- Dependencies: 233
-- Name: temaarea_idtemaarea_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('temaarea_idtemaarea_seq', 1, false);


--
-- TOC entry 2553 (class 0 OID 17817)
-- Dependencies: 234
-- Data for Name: tipodocumento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2645 (class 0 OID 0)
-- Dependencies: 235
-- Name: tipodocumento_idtipodocumento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipodocumento_idtipodocumento_seq', 1, false);


--
-- TOC entry 2555 (class 0 OID 17822)
-- Dependencies: 236
-- Data for Name: tipolugar; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tipolugar (idtipolugar, cod, nombre) VALUES (1, 've', 'Vereda');
INSERT INTO tipolugar (idtipolugar, cod, nombre) VALUES (2, 'com', 'Comuna');
INSERT INTO tipolugar (idtipolugar, cod, nombre) VALUES (3, 'Cor', 'Corregimiento');
INSERT INTO tipolugar (idtipolugar, cod, nombre) VALUES (4, 'Ba', 'Barrio');


--
-- TOC entry 2646 (class 0 OID 0)
-- Dependencies: 237
-- Name: tipolugar_idtipolugar_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipolugar_idtipolugar_seq', 1, false);


--
-- TOC entry 2557 (class 0 OID 17830)
-- Dependencies: 238
-- Data for Name: tiporequerimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (1, 'Consulta');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (2, 'Sugerencia');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (3, 'Tramite');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (4, 'Felicitaciones');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (5, 'Queja');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (6, 'Reclamo');
INSERT INTO tiporequerimiento (idtiporequerimiento, tipo) VALUES (7, 'Invitacion');


--
-- TOC entry 2647 (class 0 OID 0)
-- Dependencies: 239
-- Name: tiporequerimiento_idtiporequerimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tiporequerimiento_idtiporequerimiento_seq', 1, false);


--
-- TOC entry 2559 (class 0 OID 17835)
-- Dependencies: 240
-- Data for Name: tiporespuesta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2648 (class 0 OID 0)
-- Dependencies: 241
-- Name: tiporespuesta_idtiporespuesta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tiporespuesta_idtiporespuesta_seq', 1, false);


--
-- TOC entry 2561 (class 0 OID 17840)
-- Dependencies: 242
-- Data for Name: tiposolicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tiposolicitud (idtiposolicitud, tipo) VALUES (1, 'Interna');
INSERT INTO tiposolicitud (idtiposolicitud, tipo) VALUES (2, 'Externa');
INSERT INTO tiposolicitud (idtiposolicitud, tipo) VALUES (3, 'Respuesta');


--
-- TOC entry 2649 (class 0 OID 0)
-- Dependencies: 243
-- Name: tiposolicitud_idtiposolicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tiposolicitud_idtiposolicitud_seq', 1, false);


--
-- TOC entry 2563 (class 0 OID 17845)
-- Dependencies: 244
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (5, 'a@mail.com', 0, '7778', 'CIUDADANO', '7778', 9, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (6, 'a@mail.com', 0, '8881', 'CIUDADANO', '8881', 10, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (7, 'a@mail.com', 0, '88818', 'CIUDADANO', '88818', 11, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (8, 'azuca@gmail.com', 0, '12345', 'CIUDADANO', '12345', 12, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (9, 'a@mail.com', 0, '123455', 'CIUDADANO', '123455', 13, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (10, 'a@mail.com', 0, '123453', 'CIUDADANO', '123453', 14, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (11, 'a@mail.com', 0, '65478', 'CIUDADANO', '65478', 15, NULL);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (12, 'corea@gmail.com', 0, '145214', 'CIUDADANO', '145214', 16, 14364738254);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (14, 'mial@mail.com', 0, '124578', 'CIUDADANO', '124578', 18, 14368973319);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (13, 'mial@mail.com', 2, '989898', 'CIUDADANO', '989898', 17, 1436897697);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (15, 'ma@mial.com', 1, '787878', 'CIUDADANO', '787878', 19, 14368977507);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (16, 'mial@mail.com', 2, '323232', 'CIUDADANO', '323232', 20, 1436898088);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (17, 'diana@mail.com', 2, '1117', 'CIUDADANO', '1117', 21, 1436918654);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (18, 'me@mail.com', 2, '326598', 'CIUDADANO', '326598', 22, 1436986779);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (19, 'na@mail.com', 2, '22255544', 'CIUDADANO', '22255544', 23, 1436986949);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (20, 'n@mail.com', 2, '454545', 'CIUDADANO', '454545', 24, 1436987571);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (21, 'mail@mail·¢om', 3, '17647761', 'CIUDADANO', '17647761', 25, 1437004105);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (1, 'ca@ca.con', 1462, '123', 'CIUDADANO', 'ciudadano', 1, 1437160074);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (2, 'fu@fu.com', 234, '1234', 'FUNCIONARIO', 'funcionario', 2, 1437160213);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (3, 'ad@ad.com', 341, '12345', 'ADMIN', 'admin', 3, 1437160314);
INSERT INTO users (idusers, mail, numaccesos, password, rol, username, idusuario, ultimoacceso) VALUES (4, 'sup@sup.com', 445, '123456', 'SUPER', 'super', 4, 1437160355);


--
-- TOC entry 2650 (class 0 OID 0)
-- Dependencies: 245
-- Name: users_idusers_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_idusers_seq', 21, true);


--
-- TOC entry 2565 (class 0 OID 17853)
-- Dependencies: 246
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (3, 'ADMIN', '333', '12/12/1975', 'ADMIN', 'USUARIO', 'TRES', '-', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (2, 'FUNCIONARIO', '222', '12/12/1985', 'FUNCIONARIO', 'USUARIO', 'DOS', '-', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (4, 'SUPER', '444', '12/12/1965', 'SUPER', 'USUARIO', 'CUATRO', '-', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (1, 'CIUIDADANO', '111', '28/05/1968', 'CIUDADANO', 'USUARIO', 'UNO', '-', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (5, 'Care', '42544', '12/12/1986', 'CIUDADANO', 'Qoque', 'Garces', 'Lore', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (6, 'apellido', '142', '10/10/1978', 'CIUDADANO', 'primer', 'despues', 'segundo', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (7, 'apellqqer', '41425', '12/04/1987', 'CIUDADANO', 'nuevo', 'terder', 'Userq', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (8, 'perez', '777', '04/12/1974', 'CIUDADANO', 'pepito', 'ca', 'pe', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (9, 'LOZADA', '7778', '12/03/1994', 'CIUDADANO', 'MAREIAL', 'SERA', 'LOLA', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (10, 'trilla', '8881', '02/02/1999', 'CIUDADANO', 'Marttha', 'cosa', 'pesar', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (11, 'lerico', '88818', '12/12/1996', 'CIUDADANO', 'lucia', 'toledo', 'rojas', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (12, 'zo', '12345', '12/12/1985', 'CIUDADANO', 'Luis', 'solito', 'Cardo', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (13, 'castro', '123455', '12/12/1978', 'CIUDADANO', 'Carmen', 'castro', 'rocio', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (14, 'Castro ', '123453', '12/12/1985', 'CIUDADANO', 'Porro', 'aicedo', 'rongolo', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (15, 'Na', '65478', '12/12/1985', 'CIUDADANO', 'Lola', 'denada', 'Lao', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (16, 'OSSA', '145214', '12/12/1987', 'CIUDADANO', 'GUILLE', 'PASERA', 'ERMO', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (17, 'ROJAS', '989898', '12/12/1987', NULL, 'JOSE', 'ROJAS', 'ALIRIOQ', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (18, 'LITA', '124578', '12/12/1987', NULL, 'LOLA', 'MERA', 'LAOL', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (19, 'PAREDES', '787878', '12/12/1963', NULL, 'MARTHA', 'PAREDES', 'LILILANA', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (20, 'PEÑA', '323232', '12/12/1963', NULL, 'MARTHA', 'NIETO', 'PATRCIA', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (21, 'SANABRIA', '1117', '23/11/1992', NULL, 'DIANA', 'DE MARIN', 'LISSETH', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (22, 'SAN', '326598', '12/12/1987', NULL, 'JULIO', 'SARABANDA', 'CESAR', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (23, 'SOLA', '22255544', '12/12/1975', NULL, 'ZOILA', '', '', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (24, 'PAILA', '454545', '12/12/1987', 'CIUDADANO', 'LUCAS', 'PAPA', 'TANEDA', 'Cédula');
INSERT INTO usuario (idusuario, apellido, docid, fechanacimiento, grupo, nombre, otroapellido, otronombre, tipodoc) VALUES (25, 'MOLINA', '17647761', '12/12/1987', 'CIUDADANO', 'LUIS', 'PAREDES', 'FERNANDO', 'Cédula');


--
-- TOC entry 2651 (class 0 OID 0)
-- Dependencies: 247
-- Name: usuario_idusuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_idusuario_seq', 25, true);


--
-- TOC entry 2251 (class 2606 OID 17900)
-- Name: archivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT archivo_pkey PRIMARY KEY (idarchivo);


--
-- TOC entry 2253 (class 2606 OID 17902)
-- Name: asignado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY asignado
    ADD CONSTRAINT asignado_pkey PRIMARY KEY (idasignado);


--
-- TOC entry 2256 (class 2606 OID 17904)
-- Name: cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY (idcargo);


--
-- TOC entry 2258 (class 2606 OID 17906)
-- Name: categoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoria
    ADD CONSTRAINT categoria_pkey PRIMARY KEY (idcategoria);


--
-- TOC entry 2260 (class 2606 OID 17908)
-- Name: categoriadocumental_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoriadocumental
    ADD CONSTRAINT categoriadocumental_pkey PRIMARY KEY (idcategoriadocumental);


--
-- TOC entry 2262 (class 2606 OID 17910)
-- Name: categoriatipodocumento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categoriatipodocumento
    ADD CONSTRAINT categoriatipodocumento_pkey PRIMARY KEY (idcategoriatipodocumento);


--
-- TOC entry 2264 (class 2606 OID 17912)
-- Name: ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT ciudad_pkey PRIMARY KEY (idciudad);


--
-- TOC entry 2266 (class 2606 OID 17914)
-- Name: departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (iddepartamento);


--
-- TOC entry 2268 (class 2606 OID 17916)
-- Name: dependencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dependencia
    ADD CONSTRAINT dependencia_pkey PRIMARY KEY (iddependencia);


--
-- TOC entry 2270 (class 2606 OID 17918)
-- Name: direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT direccion_pkey PRIMARY KEY (iddireccion);


--
-- TOC entry 2272 (class 2606 OID 17920)
-- Name: disposicionfinal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY disposicionfinal
    ADD CONSTRAINT disposicionfinal_pkey PRIMARY KEY (iddisposicionfinal);


--
-- TOC entry 2327 (class 2606 OID 22784)
-- Name: docid; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT docid UNIQUE (docid);


--
-- TOC entry 2274 (class 2606 OID 17922)
-- Name: documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (iddocumento);


--
-- TOC entry 2276 (class 2606 OID 17924)
-- Name: ejetematico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ejetematico
    ADD CONSTRAINT ejetematico_pkey PRIMARY KEY (idejetematico);


--
-- TOC entry 2278 (class 2606 OID 17926)
-- Name: empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT empleado_pkey PRIMARY KEY (idempleado);


--
-- TOC entry 2280 (class 2606 OID 17928)
-- Name: encuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY encuenta
    ADD CONSTRAINT encuenta_pkey PRIMARY KEY (idencuenta);


--
-- TOC entry 2282 (class 2606 OID 17930)
-- Name: estadolaboral_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estadolaboral
    ADD CONSTRAINT estadolaboral_pkey PRIMARY KEY (idestadolaboral);


--
-- TOC entry 2284 (class 2606 OID 17932)
-- Name: estadorequerimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estadorequerimiento
    ADD CONSTRAINT estadorequerimiento_pkey PRIMARY KEY (idestadorequerimiento);


--
-- TOC entry 2286 (class 2606 OID 17934)
-- Name: institucion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY institucion
    ADD CONSTRAINT institucion_pkey PRIMARY KEY (idinstitucion);


--
-- TOC entry 2288 (class 2606 OID 17936)
-- Name: link_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY link
    ADD CONSTRAINT link_pkey PRIMARY KEY (idlink);


--
-- TOC entry 2290 (class 2606 OID 17938)
-- Name: log_solicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY log_solicitud
    ADD CONSTRAINT log_solicitud_pkey PRIMARY KEY (idlog_solicitud);


--
-- TOC entry 2292 (class 2606 OID 17940)
-- Name: pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (idpais);


--
-- TOC entry 2294 (class 2606 OID 17942)
-- Name: preferencias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY preferencias
    ADD CONSTRAINT preferencias_pkey PRIMARY KEY (idpreferencias);


--
-- TOC entry 2296 (class 2606 OID 17944)
-- Name: pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY (idpregunta);


--
-- TOC entry 2333 (class 2606 OID 22761)
-- Name: preguntafaq_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "respuestaFAQ"
    ADD CONSTRAINT preguntafaq_pkey PRIMARY KEY (idrespuestafaq);


--
-- TOC entry 2298 (class 2606 OID 17946)
-- Name: preguntafrecuente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY preguntafrecuente
    ADD CONSTRAINT preguntafrecuente_pkey PRIMARY KEY (idpregunafrecuente);


--
-- TOC entry 2301 (class 2606 OID 17948)
-- Name: respuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT respuesta_pkey PRIMARY KEY (idrespuesta);


--
-- TOC entry 2303 (class 2606 OID 17950)
-- Name: retencion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY retencion
    ADD CONSTRAINT retencion_pkey PRIMARY KEY (idretencion);


--
-- TOC entry 2305 (class 2606 OID 17952)
-- Name: rotulo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY rotulo
    ADD CONSTRAINT rotulo_pkey PRIMARY KEY (idrotulo);


--
-- TOC entry 2307 (class 2606 OID 17954)
-- Name: secciondocumental_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY secciondocumental
    ADD CONSTRAINT secciondocumental_pkey PRIMARY KEY (idsecciondocumental);


--
-- TOC entry 2309 (class 2606 OID 17956)
-- Name: servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY servicio
    ADD CONSTRAINT servicio_pkey PRIMARY KEY (idservicio);


--
-- TOC entry 2311 (class 2606 OID 17958)
-- Name: solicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT solicitud_pkey PRIMARY KEY (idsolicitud);


--
-- TOC entry 2313 (class 2606 OID 17960)
-- Name: temaarea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY temaarea
    ADD CONSTRAINT temaarea_pkey PRIMARY KEY (idtemaarea);


--
-- TOC entry 2315 (class 2606 OID 17962)
-- Name: tipodocumento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipodocumento
    ADD CONSTRAINT tipodocumento_pkey PRIMARY KEY (idtipodocumento);


--
-- TOC entry 2317 (class 2606 OID 17964)
-- Name: tipolugar_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipolugar
    ADD CONSTRAINT tipolugar_pkey PRIMARY KEY (idtipolugar);


--
-- TOC entry 2319 (class 2606 OID 17966)
-- Name: tiporequerimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiporequerimiento
    ADD CONSTRAINT tiporequerimiento_pkey PRIMARY KEY (idtiporequerimiento);


--
-- TOC entry 2321 (class 2606 OID 17968)
-- Name: tiporespuesta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiporespuesta
    ADD CONSTRAINT tiporespuesta_pkey PRIMARY KEY (idtiporespuesta);


--
-- TOC entry 2323 (class 2606 OID 17970)
-- Name: tiposolicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiposolicitud
    ADD CONSTRAINT tiposolicitud_pkey PRIMARY KEY (idtiposolicitud);


--
-- TOC entry 2325 (class 2606 OID 17972)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (idusers);


--
-- TOC entry 2329 (class 2606 OID 17974)
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (idusuario);


--
-- TOC entry 2254 (class 1259 OID 20477)
-- Name: fki_asignado_empleado1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_asignado_empleado1 ON asignado USING btree (idempleado);


--
-- TOC entry 2299 (class 1259 OID 22782)
-- Name: fki_respuesta_idusuario1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_respuesta_idusuario1 ON respuesta USING btree (idusuario);


--
-- TOC entry 2330 (class 1259 OID 22770)
-- Name: fki_respuestafaq_pregunta1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_respuestafaq_pregunta1 ON "respuestaFAQ" USING btree (idpregunta);


--
-- TOC entry 2331 (class 1259 OID 22776)
-- Name: fki_respuestafaq_usuario1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_respuestafaq_usuario1 ON "respuestaFAQ" USING btree (idusuario);


--
-- TOC entry 2334 (class 2606 OID 17975)
-- Name: fk_archivo_iddisposicionfinal; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT fk_archivo_iddisposicionfinal FOREIGN KEY (iddisposicionfinal) REFERENCES disposicionfinal(iddisposicionfinal);


--
-- TOC entry 2335 (class 2606 OID 17980)
-- Name: fk_archivo_idrespuesta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT fk_archivo_idrespuesta FOREIGN KEY (idrespuesta) REFERENCES respuesta(idrespuesta);


--
-- TOC entry 2336 (class 2606 OID 17985)
-- Name: fk_archivo_idsolicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY archivo
    ADD CONSTRAINT fk_archivo_idsolicitud FOREIGN KEY (idsolicitud) REFERENCES solicitud(idsolicitud);


--
-- TOC entry 2337 (class 2606 OID 17990)
-- Name: fk_asignado_idcargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asignado
    ADD CONSTRAINT fk_asignado_idcargo FOREIGN KEY (idcargo) REFERENCES cargo(idcargo);


--
-- TOC entry 2339 (class 2606 OID 20472)
-- Name: fk_asignado_idempleado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asignado
    ADD CONSTRAINT fk_asignado_idempleado1 FOREIGN KEY (idempleado) REFERENCES empleado(idempleado);


--
-- TOC entry 2338 (class 2606 OID 17995)
-- Name: fk_asignado_idsolicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY asignado
    ADD CONSTRAINT fk_asignado_idsolicitud FOREIGN KEY (idsolicitud) REFERENCES solicitud(idsolicitud);


--
-- TOC entry 2340 (class 2606 OID 18000)
-- Name: fk_cargo_iddependencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargo
    ADD CONSTRAINT fk_cargo_iddependencia FOREIGN KEY (iddependencia) REFERENCES dependencia(iddependencia);


--
-- TOC entry 2341 (class 2606 OID 18005)
-- Name: fk_categoriadocumental_idsecciondocumental; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoriadocumental
    ADD CONSTRAINT fk_categoriadocumental_idsecciondocumental FOREIGN KEY (idsecciondocumental) REFERENCES secciondocumental(idsecciondocumental);


--
-- TOC entry 2342 (class 2606 OID 18010)
-- Name: fk_categoriatipodocumento_idcategoriadocumental; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoriatipodocumento
    ADD CONSTRAINT fk_categoriatipodocumento_idcategoriadocumental FOREIGN KEY (idcategoriadocumental) REFERENCES categoriadocumental(idcategoriadocumental);


--
-- TOC entry 2343 (class 2606 OID 18015)
-- Name: fk_categoriatipodocumento_idtipodocumento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categoriatipodocumento
    ADD CONSTRAINT fk_categoriatipodocumento_idtipodocumento FOREIGN KEY (idtipodocumento) REFERENCES tipodocumento(idtipodocumento);


--
-- TOC entry 2344 (class 2606 OID 18020)
-- Name: fk_ciudad_iddepartamento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ciudad
    ADD CONSTRAINT fk_ciudad_iddepartamento FOREIGN KEY (iddepartamento) REFERENCES departamento(iddepartamento);


--
-- TOC entry 2345 (class 2606 OID 18025)
-- Name: fk_departamento_idpais; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT fk_departamento_idpais FOREIGN KEY (idpais) REFERENCES pais(idpais);


--
-- TOC entry 2346 (class 2606 OID 18030)
-- Name: fk_dependencia_idinstitucion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dependencia
    ADD CONSTRAINT fk_dependencia_idinstitucion FOREIGN KEY (idinstitucion) REFERENCES institucion(idinstitucion);


--
-- TOC entry 2347 (class 2606 OID 18035)
-- Name: fk_direccion_idciudad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT fk_direccion_idciudad FOREIGN KEY (idciudad) REFERENCES ciudad(idciudad);


--
-- TOC entry 2348 (class 2606 OID 18040)
-- Name: fk_direccion_idtipolugar; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT fk_direccion_idtipolugar FOREIGN KEY (idtipolugar) REFERENCES tipolugar(idtipolugar);


--
-- TOC entry 2349 (class 2606 OID 18045)
-- Name: fk_direccion_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY direccion
    ADD CONSTRAINT fk_direccion_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2350 (class 2606 OID 18050)
-- Name: fk_documento_idcategoriadocumental; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT fk_documento_idcategoriadocumental FOREIGN KEY (idcategoriadocumental) REFERENCES categoriadocumental(idcategoriadocumental);


--
-- TOC entry 2351 (class 2606 OID 18055)
-- Name: fk_documento_iddisposicionfinal; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT fk_documento_iddisposicionfinal FOREIGN KEY (iddisposicionfinal) REFERENCES disposicionfinal(iddisposicionfinal);


--
-- TOC entry 2352 (class 2606 OID 18060)
-- Name: fk_documento_idretencion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT fk_documento_idretencion FOREIGN KEY (idretencion) REFERENCES retencion(idretencion);


--
-- TOC entry 2353 (class 2606 OID 18065)
-- Name: fk_documento_idtipodocumento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documento
    ADD CONSTRAINT fk_documento_idtipodocumento FOREIGN KEY (idtipodocumento) REFERENCES tipodocumento(idtipodocumento);


--
-- TOC entry 2354 (class 2606 OID 18070)
-- Name: fk_ejetematico_idservicio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ejetematico
    ADD CONSTRAINT fk_ejetematico_idservicio FOREIGN KEY (idservicio) REFERENCES servicio(idservicio);


--
-- TOC entry 2355 (class 2606 OID 18075)
-- Name: fk_empleado_idcargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT fk_empleado_idcargo FOREIGN KEY (idcargo) REFERENCES cargo(idcargo);


--
-- TOC entry 2356 (class 2606 OID 18080)
-- Name: fk_empleado_idestadolaboral; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT fk_empleado_idestadolaboral FOREIGN KEY (idestadolaboral) REFERENCES estadolaboral(idestadolaboral);


--
-- TOC entry 2357 (class 2606 OID 18085)
-- Name: fk_empleado_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleado
    ADD CONSTRAINT fk_empleado_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2358 (class 2606 OID 18090)
-- Name: fk_encuenta_iddependencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuenta
    ADD CONSTRAINT fk_encuenta_iddependencia FOREIGN KEY (iddependencia) REFERENCES dependencia(iddependencia);


--
-- TOC entry 2359 (class 2606 OID 18095)
-- Name: fk_encuenta_idtemaarea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY encuenta
    ADD CONSTRAINT fk_encuenta_idtemaarea FOREIGN KEY (idtemaarea) REFERENCES temaarea(idtemaarea);


--
-- TOC entry 2360 (class 2606 OID 18100)
-- Name: fk_institucion_idciudad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY institucion
    ADD CONSTRAINT fk_institucion_idciudad FOREIGN KEY (idciudad) REFERENCES ciudad(idciudad);


--
-- TOC entry 2361 (class 2606 OID 18105)
-- Name: fk_log_solicitud_idsolicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_solicitud
    ADD CONSTRAINT fk_log_solicitud_idsolicitud FOREIGN KEY (idsolicitud) REFERENCES solicitud(idsolicitud);


--
-- TOC entry 2362 (class 2606 OID 18110)
-- Name: fk_log_solicitud_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY log_solicitud
    ADD CONSTRAINT fk_log_solicitud_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2363 (class 2606 OID 18115)
-- Name: fk_preferencias_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preferencias
    ADD CONSTRAINT fk_preferencias_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2364 (class 2606 OID 18120)
-- Name: fk_pregunta_idencuenta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT fk_pregunta_idencuenta FOREIGN KEY (idencuenta) REFERENCES encuenta(idencuenta);


--
-- TOC entry 2365 (class 2606 OID 18125)
-- Name: fk_pregunta_idtiporespuesta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pregunta
    ADD CONSTRAINT fk_pregunta_idtiporespuesta FOREIGN KEY (idtiporespuesta) REFERENCES tiporespuesta(idtiporespuesta);


--
-- TOC entry 2366 (class 2606 OID 18130)
-- Name: fk_preguntafrecuente_idservicio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preguntafrecuente
    ADD CONSTRAINT fk_preguntafrecuente_idservicio FOREIGN KEY (idservicio) REFERENCES servicio(idservicio);


--
-- TOC entry 2367 (class 2606 OID 18135)
-- Name: fk_preguntafrecuente_idtemaarea; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY preguntafrecuente
    ADD CONSTRAINT fk_preguntafrecuente_idtemaarea FOREIGN KEY (idtemaarea) REFERENCES temaarea(idtemaarea);


--
-- TOC entry 2368 (class 2606 OID 18140)
-- Name: fk_respuesta_idcargo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT fk_respuesta_idcargo FOREIGN KEY (idcargo) REFERENCES cargo(idcargo);


--
-- TOC entry 2369 (class 2606 OID 18145)
-- Name: fk_respuesta_idsolicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT fk_respuesta_idsolicitud FOREIGN KEY (idsolicitud) REFERENCES solicitud(idsolicitud);


--
-- TOC entry 2370 (class 2606 OID 22777)
-- Name: fk_respuesta_idusuario1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY respuesta
    ADD CONSTRAINT fk_respuesta_idusuario1 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2380 (class 2606 OID 22765)
-- Name: fk_respuestafaq_pregunta1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaFAQ"
    ADD CONSTRAINT fk_respuestafaq_pregunta1 FOREIGN KEY (idpregunta) REFERENCES pregunta(idpregunta);


--
-- TOC entry 2381 (class 2606 OID 22771)
-- Name: fk_respuestafaq_usuario1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "respuestaFAQ"
    ADD CONSTRAINT fk_respuestafaq_usuario1 FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2371 (class 2606 OID 18150)
-- Name: fk_rotulo_idsolicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY rotulo
    ADD CONSTRAINT fk_rotulo_idsolicitud FOREIGN KEY (idsolicitud) REFERENCES solicitud(idsolicitud);


--
-- TOC entry 2372 (class 2606 OID 18155)
-- Name: fk_servicio_idcategoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY servicio
    ADD CONSTRAINT fk_servicio_idcategoria FOREIGN KEY (idcategoria) REFERENCES categoria(idcategoria);


--
-- TOC entry 2373 (class 2606 OID 18160)
-- Name: fk_servicio_iddependencia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY servicio
    ADD CONSTRAINT fk_servicio_iddependencia FOREIGN KEY (iddependencia) REFERENCES dependencia(iddependencia);


--
-- TOC entry 2374 (class 2606 OID 18165)
-- Name: fk_solicitud_idejetematico; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT fk_solicitud_idejetematico FOREIGN KEY (idejetematico) REFERENCES ejetematico(idejetematico);


--
-- TOC entry 2375 (class 2606 OID 18170)
-- Name: fk_solicitud_idestadorequerimiento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT fk_solicitud_idestadorequerimiento FOREIGN KEY (idestadorequerimiento) REFERENCES estadorequerimiento(idestadorequerimiento);


--
-- TOC entry 2376 (class 2606 OID 18175)
-- Name: fk_solicitud_idtiporequerimiento; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT fk_solicitud_idtiporequerimiento FOREIGN KEY (idtiporequerimiento) REFERENCES tiporequerimiento(idtiporequerimiento);


--
-- TOC entry 2377 (class 2606 OID 18180)
-- Name: fk_solicitud_idtiposolicutd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT fk_solicitud_idtiposolicutd FOREIGN KEY (idtiposolicutd) REFERENCES tiposolicitud(idtiposolicitud);


--
-- TOC entry 2378 (class 2606 OID 18185)
-- Name: fk_solicitud_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY solicitud
    ADD CONSTRAINT fk_solicitud_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2379 (class 2606 OID 18190)
-- Name: fk_users_idusuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_idusuario FOREIGN KEY (idusuario) REFERENCES usuario(idusuario);


--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-07-20 15:29:39 COT

--
-- PostgreSQL database dump complete
--

